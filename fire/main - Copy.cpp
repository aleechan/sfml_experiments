#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML\Graphics\Color.hpp>
#include <SFML\Graphics\Image.hpp>
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <sstream>


const int width = 800;
const int height = 400;
const signed int scale = 5;

// Red

sf::Color c1 = sf::Color(0,0,0);
sf::Color c2 = sf::Color(255,0,0);
sf::Color c3 = sf::Color(255,170,0);
sf::Color c4 = sf::Color(255,255,255);


/*Purple*/
/*
sf::Color c1 = sf::Color(0,0,0);
sf::Color c2 = sf::Color(255,0,255);
sf::Color c3 = sf::Color(255,170,255);
sf::Color c4 = sf::Color(255,255,255);
*/
/*Green
sf::Color c1 = sf::Color(0,0,0);
sf::Color c2 = sf::Color(0,255,0);
sf::Color c3 = sf::Color(0,255,0);
sf::Color c4 = sf::Color(255,255,255);
*/
//Monotone
/*
sf::Color c1 = sf::Color(0,0,0);
sf::Color c2 = sf::Color(255,255,255);
sf::Color c3 = sf::Color(0,0,0);
sf::Color c4 = sf::Color(0,0,0);
*/
//raindow
/*
sf::Color c0 = sf::Color(0,0,0);
sf::Color c1 = sf::Color(255,0,0);
sf::Color c2 = sf::Color(255,255,0);
sf::Color c3 = sf::Color(255,170,0);
sf::Color c4 = sf::Color(0,255,0);
sf::Color c5 = sf::Color(0,255,85);
sf::Color c6 = sf::Color(0,255,255);
sf::Color c7 = sf::Color(0,0,255);
sf::Color c8 = sf::Color(255,0,255);
sf::Color c9 = sf::Color(255,128,255);
sf::Color c10 = sf::Color(255,255,255);
*/
//raindow(reversed)
/*
sf::Color c10 = sf::Color(0,0,0);
sf::Color c9 = sf::Color(255,0,0);
sf::Color c8 = sf::Color(255,255,0);
sf::Color c7 = sf::Color(255,170,0);
sf::Color c6 = sf::Color(0,255,0);
sf::Color c5 = sf::Color(0,255,85);
sf::Color c4 = sf::Color(0,255,255);
sf::Color c3 = sf::Color(0,0,255);
sf::Color c2 = sf::Color(255,0,255);
sf::Color c1 = sf::Color(255,128,255);
sf::Color c0 = sf::Color(255,255,255);
*/
sf::Color colours[255];
int grid[(int)(width/scale)][(int)(height/scale)] = {0};

static bool ready = false;

int clamp (int i)
{
	if (i < 0)
	{return 0;}
	if (i > 255)
	{return 255;}
	return i;
}
void init_colours()
{
	
	//for (int i=0; i<20; i++)
	//{
	//	double f = ((double)i/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c0.r + (f * ( (int)c1.r-(int)c0.r) ) ) ), 
	//							clamp( (int) ((int)c0.g + (f * ( (int)c1.g-(int)c0.g) ) ) ),
	//							clamp( (int) ((int)c0.b + (f * ( (int)c1.b-(int)c0.b) ) ) ),255);
	//}
	//for (int i=20; i<40; i++)
	//{
	//	double f = ((double)(i-10)/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c1.r + (f * ( (int)c2.r-(int)c1.r) ) ) ), 
	//							clamp( (int) ((int)c1.g + (f * ( (int)c2.g-(int)c1.g) ) ) ),
	//							clamp( (int) ((int)c1.b + (f * ( (int)c2.b-(int)c1.b) ) ) ),255);
	//}
	//for (int i=40; i<60; i++)
	//{
	//	double f = ((double)(i-20)/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c2.r + (f * ( (int)c3.r-(int)c2.r) ) ) ), 
	//							clamp( (int) ((int)c2.g + (f * ( (int)c3.g-(int)c2.g) ) ) ),
	//							clamp( (int) ((int)c2.b + (f * ( (int)c3.b-(int)c2.b) ) ) ),255);
	//}
	//for (int i=60; i<80; i++)
	//{
	//	double f = ((double)(i-30)/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c3.r + (f * ( (int)c4.r-(int)c3.r) ) ) ), 
	//							clamp( (int) ((int)c3.g + (f * ( (int)c4.g-(int)c3.g) ) ) ),
	//							clamp( (int) ((int)c3.b + (f * ( (int)c4.b-(int)c3.b) ) ) ),255);
	//}
	//for (int i=80; i<100; i++)
	//{
	//	double f = ((double)(i-40)/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c4.r + (f * ( (int)c5.r-(int)c4.r) ) ) ), 
	//							clamp( (int) ((int)c4.g + (f * ( (int)c5.g-(int)c4.g) ) ) ),
	//							clamp( (int) ((int)c4.b + (f * ( (int)c5.b-(int)c4.b) ) ) ),255);
	//}
	//for (int i=100; i<120; i++)
	//{
	//	double f = ((double)(i-50)/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c5.r + (f * ( (int)c6.r-(int)c5.r) ) ) ), 
	//							clamp( (int) ((int)c5.g + (f * ( (int)c6.g-(int)c5.g) ) ) ),
	//							clamp( (int) ((int)c5.b + (f * ( (int)c6.b-(int)c5.b) ) ) ),255);
	//}
	//for (int i=120; i<140; i++)
	//{
	//	double f = ((double)(i-60)/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c6.r + (f * ( (int)c7.r-(int)c6.r) ) ) ), 
	//							clamp( (int) ((int)c6.g + (f * ( (int)c7.g-(int)c6.g) ) ) ),
	//							clamp( (int) ((int)c6.b + (f * ( (int)c7.b-(int)c6.b) ) ) ),255);
	//}
	//for (int i=140; i<160; i++)
	//{
	//	double f = ((double)(i-70)/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c7.r + (f * ( (int)c8.r-(int)c7.r) ) ) ), 
	//							clamp( (int) ((int)c7.g + (f * ( (int)c8.g-(int)c7.g) ) ) ),
	//							clamp( (int) ((int)c7.b + (f * ( (int)c8.b-(int)c7.b) ) ) ),255);
	//}
	//for (int i=160; i<180; i++)
	//{
	//	double f = ((double)(i-70)/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c8.r + (f * ( (int)c9.r-(int)c8.r) ) ) ), 
	//							clamp( (int) ((int)c8.g + (f * ( (int)c9.g-(int)c8.g) ) ) ),
	//							clamp( (int) ((int)c8.b + (f * ( (int)c9.b-(int)c8.b) ) ) ),255);
	//}
	//for (int i=180; i<200; i++)
	//{
	//	double f = ((double)(i-70)/10);
	//	colours[i] = sf::Color( clamp( (int) ((int)c9.r + (f * ( (int)c10.r-(int)c9.r) ) ) ), 
	//							clamp( (int) ((int)c9.g + (f * ( (int)c10.g-(int)c9.g) ) ) ),
	//							clamp( (int) ((int)c9.b + (f * ( (int)c10.b-(int)c9.b) ) ) ),255);
	//}
	
	
	for (int i=0; i<85; i++)
	{
		double f = ((double)i/85);
		colours[i] = sf::Color( clamp( (int) ((int)c1.r + (f * ( (int)c2.r-(int)c1.r) ) ) ), 
								clamp( (int) ((int)c1.g + (f * ( (int)c2.g-(int)c1.g) ) ) ),
								clamp( (int) ((int)c1.b + (f * ( (int)c2.b-(int)c1.b) ) ) ),255);
	}
	for (int i=85; i<170; i++)
	{
		double f = ((double)(i-85)/85);
		colours[i] = sf::Color( clamp( (int) ((int)c2.r + (f * ( (int)c3.r-(int)c2.r) ) ) ),
								clamp( (int) ((int)c2.g + (f * ( (int)c3.g-(int)c2.g) ) ) ),
								clamp( (int) ((int)c2.b + (f * ( (int)c3.b-(int)c2.b) ) ) ),255);
	}
	for (int i=170; i<255; i++)
	{
		double f = ((double)(i-170)/85);
		colours[i] = sf::Color( clamp( (int) ((int)c3.r + (f * ( (int)c4.r-(int)c3.r) ) ) ), 
								clamp( (int) ((int)c3.g + (f * ( (int)c4.g-(int)c3.g) ) ) ),
								clamp( (int) ((int)c3.b + (f * ( (int)c4.b-(int)c3.b) ) ) ),255);
	}



	/*
	for (int i=0; i<100; i++)
	{
		double f = ((double)i/100);
		colours[i] = sf::Color( clamp( (int) ((int)c1.r + (f * (int)c2.r-(int)c1.r) ) ), 
								clamp( (int) ((int)c1.g + (f * (int)c2.g-(int)c1.g) ) ),
								clamp( (int) ((int)c1.b + (f * (int)c2.b-(int)c1.b) ) ),255);
	}*/
}

int irandom(int v)
{
	return (int)(v*rand()/RAND_MAX);
}

void init_base()
{
	for (int i=0; i<(int)(width/scale); i++)
	{
		int a = irandom(3);
		if (a==1)
		{ grid[i][(int)(height/scale)]=200;}
		else
		{ grid[i][(int)(height/scale)]=0;}
		//grid[i][(int)(height/scale)]=irandom(99);
	}
}

void rand_base()
{
	for (int i=0; i<(int)(width/scale); i++)
	{
		int a = irandom(7);
		if (a==1)
		{ grid[i][(int)(height/scale)]=200;}
		if (a==0)
		{ grid[i][(int)(height/scale)]=0;}

	}
}	


void effire()
{
	for(int i=1; i<(int)(width/scale)-1; i++)
	{
		for(int j=(int)(height/scale)-1; j>=0; j--)
		{
			grid[i][j] = (int)( ( grid[i-1][j+1] + grid[i][j+1] + grid[i+1][j+1] )/3 -1);
		}
	}
}
void effire2()
{
	for(int j=(int)(height/scale)-1; j>=0; j--)
	{
		for(int i=1; i<(int)(width/scale)-1; i++)
		{
			grid[i][j] = (int)( ( grid[i-1][j+1] + grid[i][j+1] + grid[i+1][j+1] )/3 );
		}
	}
}

int main()
{
    // Create the main rendering window
    sf::RenderWindow App(sf::VideoMode (width, height, 32), "Fire");
    sf::Texture      image;
	sf::Sprite       sprite;
	sf::Uint8        *pixels  = new sf::Uint8[width * height * 4];

	image.create(width,height);
	image.setSmooth(false);
	sprite.setTexture(image); 
	//App.setFramerateLimit(15);
	
	sf::Clock clock;
    float lastTime = 0;

	init_colours();
	init_base();
    // Start game loop
   while (App.isOpen())
     {
         // Process events
         sf::Event event;
         while (App.pollEvent(event))
         {
             // Close window : exit
             if (event.type == sf::Event::Closed)
                 App.close();
         }

			//App.clear();
			init_base();
			effire();
			for(int i=0; i<(int)(width/scale); i++)
			{
				for(int j=0; j<(int)(height/scale); j++)
				{
					int temp = (int) grid[i][j];
					if (j == 0 || j==(int)(height/scale))
					{
						temp=0;
					}
					if (temp > 200)
					{temp = 200;}
					if (temp<0)
					{temp=0;}
					
					if (scale==1)
					{ 
						pixels[ (i+j*width)*4]         = colours[temp].r; 
						pixels[ (i+j*width)*4 + 1]     = colours[temp].g;
						pixels[ (i+j*width)*4 + 2]     = colours[temp].b; 
						pixels[ (i+j*width)*4 + 3]     = 255;
					}
					else if (scale>1)
					{
						for (int a=0;a<scale;a++)
						{
							for (int b=0;b<scale;b++)
							{
								pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4]         = colours[temp].r; 
								pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4 + 1]     = colours[temp].g;
								pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4 + 2]     = colours[temp].b; 
								pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4 + 3]     = 255;
							}
						}
					}
				}
			}
			image.update(pixels);
			App.draw(sprite); 
			//App.draw(sprite);
			// Display window contents on screen
			App.display();
		float currentTime = clock.restart().asSeconds();
        float fps = 1.f / currentTime;
        lastTime = currentTime;
		std::stringstream out;
		out << fps;
		App.setTitle(out.str());
    }

    return EXIT_SUCCESS;
}