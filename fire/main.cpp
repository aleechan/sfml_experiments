#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML\Graphics\Color.hpp>
#include <SFML\Graphics\Image.hpp>
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <sstream>


const int width = 800;
const int height = 800;
#include "shaders.h"
const int scaleW = 400;
const int scaleH = 400;

float max_val = 1000;

int grid[scaleW][scaleH] = {0};

static bool ready = false;

int clamp (int i)
{
	if (i < 0)
	{return 0;}
	if (i > max_val)
	{return max_val;}
	return i;
}

class colours
{
public:
	sf::RenderTexture grad;
	std::vector<sf::Color> cols;
	std::vector<float> range;
	void add_colour(sf::Color col1, float range0)
	{
		cols.push_back(col1);
		range.push_back(range0);
	}
	void render()
	{
		grad.create(range[range.size()-1],1);
		grad.clear(sf::Color::Transparent);
		sf::VertexArray line(sf::LinesStrip,cols.size());
		for(int i = 0; i < cols.size(); i++)
		{
			line[i].color = cols[i];
			line[i].position = sf::Vector2f(range[i],0);
			
		}
		grad.draw(line);
	}
	void clear()
	{
		cols.clear();
		range.clear();
	}

};

int irandom(int v)
{
	return (int)(v*rand()/RAND_MAX);
}

void init_base()
{
	for (int i=0; i<scaleW; i++)
	{
		int a = irandom(3);
		if (a==1)
		{ grid[i][scaleH]=max_val;}
		else
		{ grid[i][scaleH]=0;}
		//grid[i][(int)(height/scale)]=irandom(99);
	}
}

void rand_base()
{
	for (int i=0; i<scaleW; i++)
	{
		int a = irandom(7);
		if (a==1)
		{ grid[i][scaleH]=max_val;}
		if (a==0)
		{ grid[i][scaleH]=0;}

	}
}	


void effire()
{
	for(int i=1; i<scaleW-1; i++)
	{
		for(int j=scaleH-1; j>=0; j--)
		{
			grid[i][j] = (int)( ( grid[i-1][j+1] + grid[i][j+1] + grid[i+1][j+1] )/3 -1);
		}
	}
}
void effire2()
{
	for(int i=1; i<scaleW-1; i++)
	{
		for(int j=scaleH-1; j>=0; j--)
		{
			grid[i][j] = (int)( ( grid[i-1][j+1] + grid[i][j+1] + grid[i+1][j+1] )/3 );
		}
	}
}

int main()
{
    // Create the main rendering window
    sf::RenderWindow App(sf::VideoMode (width, height, 32), "Fire");
    sf::Texture      image;
	sf::Sprite       sprite;
	sf::Uint8        *pixels  = new sf::Uint8[scaleW * scaleH * 4];

	image.create(scaleW,scaleH);
	image.setSmooth(false);
	sprite.setTexture(image); 
	sprite.setScale((float)width/scaleW,(float)height/scaleH);
	//App.setFramerateLimit(30);
	
	sf::RenderTexture temp;
	temp.create(width,height);

	sf::Transform scale;
	//scale.scale(sf::Vector2f(width/scaleW,height/scaleH),sf::Vector2f(width/2,height/2));

	colours grad;
	grad.clear();
	
	grad.add_colour(sf::Color(  0,  0,  0),0);
	grad.add_colour(sf::Color(255,  0,  0),100);
	grad.add_colour(sf::Color(255,255,  0),200);
	grad.add_colour(sf::Color(255,170,  0),300);
	grad.add_colour(sf::Color(  0,255,  0),400);
	grad.add_colour(sf::Color(  0,255, 85),500);
	grad.add_colour(sf::Color(  0,255,255),600);
	grad.add_colour(sf::Color(  0,  0,255),700);
	grad.add_colour(sf::Color(255,  0,255),800);
	grad.add_colour(sf::Color(255,128,255),900);
	grad.add_colour(sf::Color(255,255,255),1000);

	//grad.add_colour(sf::Color(0,0,0),0);
	//grad.add_colour(sf::Color(255,0,255),85);
	//grad.add_colour(sf::Color(255,170,255),170);
	//grad.add_colour(sf::Color(255,255,255),255);

	//grad.add_colour(sf::Color(0,0,0),0);
	//grad.add_colour(sf::Color( 55, 55, 55),500);
	//grad.add_colour(sf::Color(155,155,155),1000);

	//grad.add_colour(sf::Color(0,0,0),0);
	//grad.add_colour(sf::Color(255,0,0),85);
	//grad.add_colour(sf::Color(255,255,170),170);
	//grad.add_colour(sf::Color(255,255,255),255);
	grad.render();

	sf::Shader fire;
	fire.loadFromFile("fire2.txt",sf::Shader::Fragment);
	fire.setParameter("pal", grad.grad.getTexture());
	//fire.setParameter("pix", (float)max_val/255);

	gaus_blur blur;
	blur.init();

	sf::Clock clock;
    float lastTime = 0;

	init_base();
    // Start game loop
    while (App.isOpen())
     {
         // Process events
         sf::Event event;
         while (App.pollEvent(event))
         {
             // Close window : exit
             if (event.type == sf::Event::Closed)
                 App.close();
         }
		App.clear();
		blur.clear();
		init_base();
		effire();
		for(int i=0; i<scaleW; i++)
		{
			for(int j=0; j<scaleH; j++)
			{
				float temp = grid[i][j];
				if (j == 0 || j==scaleH) {temp=0;}
				if (temp > max_val)      {temp = max_val;}
				if (temp<0)              {temp=0;}
				pixels[ (i+j*scaleW)*4+1] = floor(temp/255);
				pixels[ (i+j*scaleW)*4  ] = temp - pixels[ (i+j*scaleW)*4+1] ; 
				
			}
		}
		temp.clear();
		image.update(pixels);
		temp.draw(sprite); 
		temp.display();

		fire.setParameter("tex",temp.getTexture());
		
		blur.original.draw(sf::Sprite(temp.getTexture()),&fire);
		App.draw(blur.render());
		//App.draw(sf::Sprite(temp.getTexture()),&fire);
		App.draw(sf::Sprite(grad.grad.getTexture()));

		App.display();
		float currentTime = clock.restart().asSeconds();
        float fps = 1.f / currentTime;
        lastTime = currentTime;
		std::stringstream out;
		out << fps;
		App.setTitle(out.str());
    }

    return EXIT_SUCCESS;
}