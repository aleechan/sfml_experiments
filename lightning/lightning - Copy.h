#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>

#ifndef __LIGHTNING_H_INCLUDED__
#define __LIGHTNING_H_INCLUDED__
#endif

extern const double PI = 3.14159265;
extern const double degtorad = 0.0174532925;
extern const double radtodeg = 57.2957795;

sf::RenderTexture temptex;
sf::Shader shader;

sf::Shader blurV,blurH,tint;
sf::RenderTexture temp1, temp2, temp3;

float phase;

double point_direction(double x1, double y1, double x2, double y2)
{
	// returns the angle of a line in radians
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

int irandom(int v)//generate random int
{
	if (v>10)
	{return  (int)(v*rand()/RAND_MAX);}
	else
	{return rand()%v;}
}

class line
{
public:
	sf::Vector2f p0;
	sf::Vector2f p1;
	double wid;

	void init(float x0, float y0, float x1, float y1, double wid2)
	{
		p0.x = x0;
		p0.y = y0;
		p1.x = x1;
		p1.y = y1;
		wid = wid2;
	}
};

void load_shader_blur()
{
	 temptex.create(800,600);
	 if(!shader.loadFromFile("blur.frag",sf::Shader::Fragment)){std::cout << "Error";}
	 shader.setParameter("texture", sf::Shader::CurrentTexture);
	 shader.setParameter("blur_radius", 0.008f);
	 phase = 0;
}

void load_shader_blur2()
{
	 temp1.create(800,600);
	 temp2.create(800,600);
	 temp3.create(800,600);

	 blurV.loadFromFile("blurV.txt",sf::Shader::Fragment);
	 blurV.setParameter("blurSize",0.00125f );

	 blurH.loadFromFile("blurH.txt",sf::Shader::Fragment);
	 blurH.setParameter("blurSize",0.00125f );

	 tint.loadFromFile("intensify.txt",sf::Shader::Fragment);
}
void bolt (double x0,double y0, double x1, double y1, double offset, double numgens,sf::Color colour,sf::RenderWindow &app)
{
	double maxwid =3+irandom(2);
	std::vector<line> lines;
	line temp;
	sf::Vector2f p0,p1,p2;
	temp.init(x0,y0,x1,y1,maxwid);
	lines.push_back(temp);
	for(int i = 0;i<numgens;i++)
	{
		//std::cout << lines.size() << "\n";
		int size = lines.size();
		for(int j = 0;j<size;j++)
		{
			p0 = lines[j].p0;
			p2 = lines[j].p1;
			p1.x = (p2.x-p0.x)/2+p0.x;
			p1.y = (p2.y-p0.y)/2+p0.y;
			int displace = offset-irandom(offset*2);
			double dir = point_direction(p0.x,p0.y,p2.x,p2.y)+PI/2;
			p1.x += displace*cos(dir);
			p1.y += displace*sin(dir);
			//temp.init(p1.x,p1.y,p2.x,p2.y);
			line temp2;
			temp2.p0 = p1;
			temp2.p1 = p2;
			lines.push_back(temp2);
			lines[j].init(p0.x,p0.y,p1.x,p1.y,maxwid);
			if(irandom(1)==0)
			{
				p2 = p1+(p1-p0);
				p0 = p1;
				temp2.init(p0.x,p0.y,p2.x,p2.y,maxwid);
				lines.push_back(temp2);
			}
		}
		offset/=2;
	}
	sf::VertexArray bolt(sf::Quads,lines.size()*8);
	sf::VertexArray boltglow(sf::Quads,lines.size()*8);
	double wid = 0;
	for(int i = 0; i<lines.size();i++)
	{
		wid = lines[i].wid;
		double  dir = point_direction(lines[i].p0.x,lines[i].p0.y,lines[i].p1.x,lines[i].p1.y)+PI/2;
		bolt[i*8  ].position = sf::Vector2f(lines[i].p0.x+wid*cos(dir),lines[i].p0.y+wid*sin(dir));
		bolt[i*8  ].color    = sf::Color(0,0,0,0);
		bolt[i*8+1].position = sf::Vector2f(lines[i].p1.x+wid*cos(dir),lines[i].p1.y+wid*sin(dir));
		bolt[i*8+1].color    = sf::Color(0,0,0,0);
		bolt[i*8+2].position = sf::Vector2f(lines[i].p1.x,lines[i].p1.y);
		bolt[i*8+2].color    = colour;
		bolt[i*8+3].position = sf::Vector2f(lines[i].p0.x,lines[i].p0.y);
		bolt[i*8+3].color    = colour;

		bolt[i*8+4].position = sf::Vector2f(lines[i].p0.x-wid*cos(dir),lines[i].p0.y-wid*sin(dir));
		bolt[i*8+4].color    = sf::Color(0,0,0,0);
		bolt[i*8+5].position = sf::Vector2f(lines[i].p1.x-wid*cos(dir),lines[i].p1.y-wid*sin(dir));
		bolt[i*8+5].color    = sf::Color(0,0,0,0);
		bolt[i*8+6].position = sf::Vector2f(lines[i].p1.x,lines[i].p1.y);
		bolt[i*8+6].color    = colour;
		bolt[i*8+7].position = sf::Vector2f(lines[i].p0.x,lines[i].p0.y);
		bolt[i*8+7].color    = colour;

		wid*=3.5;
		boltglow[i*8  ].position = sf::Vector2f(lines[i].p0.x+wid*cos(dir),lines[i].p0.y+wid*sin(dir));
		boltglow[i*8  ].color    = sf::Color(0,0,0,0);
		boltglow[i*8+1].position = sf::Vector2f(lines[i].p1.x+wid*cos(dir),lines[i].p1.y+wid*sin(dir));
		boltglow[i*8+1].color    = sf::Color(0,0,0,0);
		boltglow[i*8+2].position = sf::Vector2f(lines[i].p1.x,lines[i].p1.y);
		boltglow[i*8+2].color    = colour;
		boltglow[i*8+3].position = sf::Vector2f(lines[i].p0.x,lines[i].p0.y);
		boltglow[i*8+3].color    = colour;

		boltglow[i*8+4].position = sf::Vector2f(lines[i].p0.x-wid*cos(dir),lines[i].p0.y-wid*sin(dir));
		boltglow[i*8+4].color    = sf::Color(0,0,0,0);
		boltglow[i*8+5].position = sf::Vector2f(lines[i].p1.x-wid*cos(dir),lines[i].p1.y-wid*sin(dir));
		boltglow[i*8+5].color    = sf::Color(0,0,0,0);
		boltglow[i*8+6].position = sf::Vector2f(lines[i].p1.x,lines[i].p1.y);
		boltglow[i*8+6].color    = colour;
		boltglow[i*8+7].position = sf::Vector2f(lines[i].p0.x,lines[i].p0.y);
		boltglow[i*8+7].color    = colour;
	}

	//temptex.clear(sf::Color::Transparent);
	//temptex.draw(boltglow);
	//	
	//temptex.display();
	//sf::Sprite sprite(temptex.getTexture());
	//app.draw(sprite,&shader);

	//app.draw(bolt);

	temp1.clear(sf::Color::Transparent);
	temp2.clear(sf::Color::Transparent);
	temp3.clear(sf::Color::Transparent);

	phase+=0.005;
	//tint.setParameter("screenTexture",temp1.getTexture());

	temp1.draw(boltglow);
	temp1.display();
	blurV.setParameter("screenTexture",temp1.getTexture());
	
	temp2.draw(sf::Sprite(temp1.getTexture()),&blurV);
	temp2.display();
	blurH.setParameter("screenColorBuffer",temp2.getTexture());
	temp3.draw(sf::Sprite(temp2.getTexture()),&blurH);
	temp3.display();

	app.draw(sf::Sprite(temp3.getTexture()));
	app.draw(bolt);
}