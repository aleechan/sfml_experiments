#include <string>

double point_distance(double x1, double y1, double x2, double y2)
{
	// returns the distance between two points
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (sqrt(a));
}
double sqr_distance(double x1, double y1, double x2, double y2)
{
	// returns the square of the distance between two points
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (a);
}

double point_direction(double x1, double y1, double x2, double y2)
{
	// returns the angle of a line in radians
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

int irandom(int v)//generate random int
{
	if (v>10)
	{return  (int)(v*rand()/RAND_MAX);}
	else
	{return rand()%v;}
}

int round(double num)//round number
{
	double dec;
	dec = modf(num,&num);
	if (dec >=0.5)
	{return (num+1);}
	else
	{return num;}
}

int expt(int num, int exp)//returns power of a number
{
	int tnum = num;
	for (int i=0; i<exp;i++)
	{
		num*=tnum;
	}
	return num;
}

std::string inttostring(int num)//change int to a string
{
	std::stringstream ss;
	ss << num;
	return ss.str();
}