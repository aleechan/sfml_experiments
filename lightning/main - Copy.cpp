#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML\System\Thread.hpp>
#include <vector>
#include <list>
#include <iostream>
#include <sstream>
#include <string>

const double PI = 3.14159265;

int irandom(int v)
{
	if (v>10)
	{return  (int)(v*rand()/RAND_MAX);}
	else if (v == 0)
	{return 0;}
	else
	{return rand()%v;}
}

double point_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (sqrt(a));
}

double point_direction(double x1, double y1, double x2, double y2)
{
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

class line
{
public:
	sf::Vector2f p0;
	sf::Vector2f p1;
	double wid;

	void init(float x0, float y0, float x1, float y1, double wid2)
	{
		p0.x = x0;
		p0.y = y0;
		p1.x = x1;
		p1.y = y1;
		wid = wid2;
	}
};

void bolt (double x0,double y0, double x1, double y1, double offset, double numgens,sf::RenderWindow &app)
{
	std::vector<line> lines;
	line temp;
	sf::Vector2f p0,p1,p2;
	temp.init(x0,y0,x1,y1,offset*0.25);
	lines.push_back(temp);
	for(int i = 0;i<numgens;i++)
	{
		int size = lines.size();
		for(int j = 0;j<size;j++)
		{
			p0 = lines[j].p0;
			p2 = lines[j].p1;
			p1.x = (p2.x-p0.x)/2+p0.x;
			p1.y = (p2.y-p0.y)/2+p0.y;
			int displace = offset-irandom(offset*2);
			double dir = point_direction(p0.x,p0.y,p2.x,p2.y)+PI/2;
			p1.x += displace*cos(dir);
			p1.y += displace*sin(dir);
			line temp2;
			temp2.p0 = p1;
			temp2.p1 = p2;
			lines.push_back(temp2);
			lines[j].init(p0.x,p0.y,p1.x,p1.y,offset*0.25);
			if(irandom(2)==0)
			{
				p2.x = p1.x+(p1.x-p0.x)*(0.7+0.01*irandom(30));
				p2.y = p1.y+(p1.y-p0.y)*(0.7+0.01*irandom(30));
				p0 = p1;
				temp2.init(p0.x,p0.y,p2.x,p2.y,offset*0.25);
				lines.push_back(temp2);
			}
		}
		offset/=2;
	}
	sf::VertexArray bolt(sf::Quads,lines.size()*4);
	for(int i = 0; i<lines.size();i++)
	{
		double  dir = point_direction(lines[i].p0.x,lines[i].p0.y,lines[i].p1.x,lines[i].p1.y)+PI/2;
		int wid = 2;
		bolt[i*4  ].position = sf::Vector2f(lines[i].p0.x+wid*cos(dir),lines[i].p0.y+wid*sin(dir));
		bolt[i*4  ].color    = sf::Color(255,25,25,155+irandom(100));
		bolt[i*4+1].position = sf::Vector2f(lines[i].p0.x-wid*cos(dir),lines[i].p0.y-wid*sin(dir));
		bolt[i*4+1].color    = sf::Color(255,25,25,155+irandom(100));
		bolt[i*4+2].position = sf::Vector2f(lines[i].p1.x+wid*cos(dir),lines[i].p1.y+wid*sin(dir));
		bolt[i*4+2].color    = sf::Color(255,25,25,155+irandom(100));
		bolt[i*4+3].position = sf::Vector2f(lines[i].p1.x-wid*cos(dir),lines[i].p1.y-wid*sin(dir));
		bolt[i*4+3].color    = sf::Color(255,25,25,155+irandom(100));
	}
	app.draw(bolt);
}
void bolt2 (double x0,double y0, double x1, double y1, double offset, double numgens,sf::RenderWindow &app)
{
	double maxwid =3+irandom(4);
	std::vector<line> lines;
	line temp;
	sf::Vector2f p0,p1,p2;
	temp.init(x0,y0,x1,y1,maxwid);
	lines.push_back(temp);
	for(int i = 0;i<numgens;i++)
	{
		//std::cout << lines.size() << "\n";
		int size = lines.size();
		for(int j = 0;j<size;j++)
		{
			p0 = lines[j].p0;
			p2 = lines[j].p1;
			p1.x = (p2.x-p0.x)/2+p0.x;
			p1.y = (p2.y-p0.y)/2+p0.y;
			int displace = offset-irandom(offset*2);
			double dir = point_direction(p0.x,p0.y,p2.x,p2.y)+PI/2;
			p1.x += displace*cos(dir);
			p1.y += displace*sin(dir);
			//temp.init(p1.x,p1.y,p2.x,p2.y);
			line temp2;
			temp2.p0 = p1;
			temp2.p1 = p2;
			lines.push_back(temp2);
			lines[j].init(p0.x,p0.y,p1.x,p1.y,maxwid);
			if(irandom(3)==0)
			{
				p2 = p1+(p1-p0);
				p0 = p1;
				temp2.init(p0.x,p0.y,p2.x,p2.y,maxwid);
				lines.push_back(temp2);
			}
		}
		offset/=2;
	}
	//Draw with lines
	//sf::VertexArray bolt(sf::Lines,lines.size()*2);
	//for(int i = 0; i<lines.size();i++)
	//{
	//	bolt[i*2].position = lines[i].p0;
	//	bolt[i*2].color.White;
	//	bolt[i*2+1].position = lines[i].p1;
	//	bolt[i*2+1].color.White;
	//}
	//Draw with quads ver 1
	//sf::VertexArray bolt(sf::Quads,lines.size()*4);
	//for(int i = 0; i<lines.size();i++)
	//{
	//	double  dir = point_direction(lines[i].p0.x,lines[i].p0.y,lines[i].p1.x,lines[i].p1.y)+PI/2;
	//	bolt[i*4  ].position = sf::Vector2f(lines[i].p0.x+1*cos(dir),lines[i].p0.y+1*sin(dir));
	//	bolt[i*4  ].color    = sf::Color(255,25,25);
	//	bolt[i*4+1].position = sf::Vector2f(lines[i].p0.x-1*cos(dir),lines[i].p0.y-1*sin(dir));
	//	bolt[i*4+1].color    = sf::Color(255,25,25);
	//	bolt[i*4+2].position = sf::Vector2f(lines[i].p1.x+1*cos(dir),lines[i].p1.y+1*sin(dir));
	//	bolt[i*4+2].color    = sf::Color(255,25,25);
	//	bolt[i*4+3].position = sf::Vector2f(lines[i].p1.x-1*cos(dir),lines[i].p1.y-1*sin(dir));
	//	bolt[i*4+3].color    = sf::Color(255,25,25);
	//}
	//Draw with quads ver 2
	/*sf::VertexArray bolt(sf::Quads,lines.size()*16);
	for(int i = 0; i<lines.size();i++)
	{
		double  dir = point_direction(lines[i].p0.x,lines[i].p0.y,lines[i].p1.x,lines[i].p1.y)+PI/2;
		int wid =5+irandom(5);
		sf::Vector2f mid;
		mid.x = lines[i].p0.x+(lines[i].p1.x-lines[i].p0.x);
		mid.y = lines[i].p0.y+(lines[i].p1.y-lines[i].p0.y);

		bolt[i*16  ].position = sf::Vector2f(lines[i].p0.x+wid*cos(dir),lines[i].p0.y+wid*sin(dir));
		bolt[i*16  ].color    = sf::Color(0,0,0,0);
		bolt[i*16+1].position = sf::Vector2f(mid.x+wid*cos(dir),mid.y+wid*sin(dir));
		bolt[i*16+1].color    = sf::Color(0,0,0,0);
		bolt[i*16+2].position = mid;
		bolt[i*16+2].color    = sf::Color(255,25,25);
		bolt[i*16+3].position = lines[i].p0;
		bolt[i*16+3].color    = sf::Color(255,25,25);
		
		bolt[i*16+4].position = sf::Vector2f(lines[i].p0.x-wid*cos(dir),lines[i].p0.y-wid*sin(dir));
		bolt[i*16+4].color    = sf::Color(0,0,0,0);
		bolt[i*16+5].position = sf::Vector2f(mid.x-wid*cos(dir),mid.y-wid*sin(dir));
		bolt[i*16+5].color    = sf::Color(0,0,0,0);
		bolt[i*16+6].position = mid;
		bolt[i*16+6].color    = sf::Color(255,25,25);
		bolt[i*16+7].position = lines[i].p0;
		bolt[i*16+7].color    = sf::Color(255,25,25);

		bolt[i*16+8].position = sf::Vector2f(lines[i].p1.x-wid*cos(dir),lines[i].p1.y-wid*sin(dir));
		bolt[i*16+8].color    = sf::Color(0,0,0,0);
		bolt[i*16+9].position = sf::Vector2f(mid.x-wid*cos(dir),mid.y-wid*sin(dir));
		bolt[i*16+9].color    = sf::Color(0,0,0,0);
		bolt[i*16+10].position = mid;
		bolt[i*16+10].color    = sf::Color(255,25,25);
		bolt[i*16+11].position = lines[i].p1;
		bolt[i*16+11].color    = sf::Color(255,25,25);

		bolt[i*16+12].position = sf::Vector2f(lines[i].p1.x+wid*cos(dir),lines[i].p1.y+wid*sin(dir));
		bolt[i*16+12].color    = sf::Color(0,0,0,0);
		bolt[i*16+13].position = sf::Vector2f(mid.x+wid*cos(dir),mid.y+wid*sin(dir));
		bolt[i*16+13].color    = sf::Color(0,0,0,0);
		bolt[i*16+14].position = mid;
		bolt[i*16+14].color    = sf::Color(255,25,25);
		bolt[i*16+15].position = lines[i].p1;
		bolt[i*16+15].color    = sf::Color(255,25,25);
	}*/
	//Draw with quads ver 3
	sf::VertexArray bolt(sf::Quads,lines.size()*8);
	double wid = 0;
	for(int i = 0; i<lines.size();i++)
	{
		wid = lines[i].wid;
		double  dir = point_direction(lines[i].p0.x,lines[i].p0.y,lines[i].p1.x,lines[i].p1.y)+PI/2;
		bolt[i*8  ].position = sf::Vector2f(lines[i].p0.x+wid*cos(dir),lines[i].p0.y+wid*sin(dir));
		bolt[i*8  ].color    = sf::Color(0,0,0,0);
		bolt[i*8+1].position = sf::Vector2f(lines[i].p1.x+wid*cos(dir),lines[i].p1.y+wid*sin(dir));
		bolt[i*8+1].color    = sf::Color(0,0,0,0);
		bolt[i*8+2].position = sf::Vector2f(lines[i].p1.x,lines[i].p1.y);
		bolt[i*8+2].color    = sf::Color(255,0,0);
		bolt[i*8+3].position = sf::Vector2f(lines[i].p0.x,lines[i].p0.y);
		bolt[i*8+3].color    = sf::Color(255,0,0);

		bolt[i*8+4].position = sf::Vector2f(lines[i].p0.x-wid*cos(dir),lines[i].p0.y-wid*sin(dir));
		bolt[i*8+4].color    = sf::Color(0,0,0,0);
		bolt[i*8+5].position = sf::Vector2f(lines[i].p1.x-wid*cos(dir),lines[i].p1.y-wid*sin(dir));
		bolt[i*8+5].color    = sf::Color(0,0,0,0);
		bolt[i*8+6].position = sf::Vector2f(lines[i].p1.x,lines[i].p1.y);
		bolt[i*8+6].color    = sf::Color(255,0,0);
		bolt[i*8+7].position = sf::Vector2f(lines[i].p0.x,lines[i].p0.y);
		bolt[i*8+7].color    = sf::Color(255,0,0);
	}
	app.draw(bolt);
}


 int main()
 {
     sf::RenderWindow window(sf::VideoMode(800, 600), "SFML window");
	 window.setFramerateLimit(5);	
	 int seg = 3;
	
	//create clock for frame rate counter
	sf::Clock clock;
    float lastTime = 0;
     while (window.isOpen())
     {
         sf::Event event;
         while (window.pollEvent(event))
         {
             if (event.type == sf::Event::Closed)
                 window.close();
         }

         window.clear();
		 if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		 {seg++;}
		 if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		 {
			 seg--;
			 if(seg<1)
			 {seg=1;}
		 }
		 //bolt(0,300,sf::Mouse::getPosition(window).x,sf::Mouse::getPosition(window).y,100,10,window);
		 int startx = irandom(800);
		 //bolt2(startx,0,startx-50+irandom(100),600,200,6,window);
		 //bolt2(startx,0,startx-50+irandom(100),600,200,6,window);
		 bolt2(startx,0,startx-50+irandom(100),600,200,seg,window);

		 //calculate frame rate and display
		float currentTime = clock.restart().asSeconds();
        float fps = 1.f / currentTime;
        lastTime = currentTime;
		std::stringstream out;
		out << "Just Another Random Tower Defence     FPS: " << (int)fps;
		window.setTitle(out.str());

         window.display();
     }
 
     return EXIT_SUCCESS;
 }