#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML\System\Thread.hpp>
#include <vector>
#include <iostream>
#include <sstream>
#include <string>
//#include "my_math.h"
#include "lightning.h"

 int main()
 {
     sf::RenderWindow window(sf::VideoMode(800, 600), "SFML window");
	 //window.setFramerateLimit(5);	
	 int seg = 3;
	 int dir = 0;
	 srand ( time(NULL) );
	 //create clock for frame rate counter
	 sf::Clock clock;
     float lastTime = 0;

	 load_shader_blur2();

     while (window.isOpen())
     {
         sf::Event event;
         while (window.pollEvent(event))
         {
             if (event.type == sf::Event::Closed)
                 window.close();
         }

         window.clear();
		 if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		 {seg++;}
		 if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		 {
			 seg--;
			 if(seg<1)
			 {seg=1;}
		 }
		 sf::Vector2f temp;
		 temp.x = 0;
		 temp.y = 0;
		 //for(int i = 0; i <20; i++)
		 //{
			// temp = sf::Vector2f(200+250*cos( (dir+i*18)*degtorad )+dir,300+250*sin( (dir+i*18)*degtorad) );
			// bolt(200+dir,300,temp.x,temp.y,50,4,sf::Color(irandom(255),irandom(255),irandom(255),255),window);
		 //}
		 //dir++;
		 //if(dir>360) dir=0;
		 //bolt(0,300,sf::Mouse::getPosition(window).x,sf::Mouse::getPosition(window).y,100,10,window);
		 int startx = irandom(800);
		 for(int i = 0; i <5; i++)
		 {
			 startx = irandom(800);
			 bolt(startx,0,startx-50+irandom(100),600,200,5,sf::Color(irandom(255),irandom(255),irandom(255),255),window);
		 }
		 //bolt(startx,0,startx-50+irandom(100),600,200,6,window);

		 //calculate frame rate and display
		float currentTime = clock.restart().asSeconds();
        float fps = 1.f / currentTime;
        lastTime = currentTime;
		std::stringstream out;
		out << "Lightning Bolt Test    FPS: " << (int)fps;
		window.setTitle(out.str());

         window.display();
     }
 
     return EXIT_SUCCESS;
 }