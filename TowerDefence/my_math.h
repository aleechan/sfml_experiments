#include <iostream>
#include <windows.h>

#ifndef _MY_MATH
#define _MY_MATH
const double degtorad = 0.0174532925;
const double radtodeg = 57.2957795;
const double PI = 3.141592654;
int irandom(int v)
{
	return (int)( (v*rand()/RAND_MAX) +0.5);
}

double point_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (sqrt(a));
}

double sqr_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (a);
}

double point_direction(double x1, double y1, double x2, double y2)
{
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

#endif