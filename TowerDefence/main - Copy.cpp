
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <fstream>
#include <windows.h>
#include <cstdlib>
#include <math.h>
#include <zmouse.h>
#include <sstream>
#include <string>
#include "path.h"
#include "textureStorage.h"

// converson values for working wiht angles
const double degtorad = 0.0174532925;
const double radtodeg = 57.2957795;
const double PI = 3.14159265;

//Gravitational Constant
const double G = 10000;

//Maximun Particles
const int partCap = 10000;
//Maximun Emitters
const int emitCap = 1000;

// Size of window in pixels
const int width = 800;
const int height = 480;

//number of enemies
const int enemynum = 100;
//Max number of turrets
const int turretcap = 100;

bool waveActive = false;
int money = 100;
int life = 20;

//Base stats of Turret 0
const int t0cost = 100;
const int t0dmg = 2;
const int t0range = 150;
const int t0rate = 10;
//Upgrade mod for Turret 0
const int t0upcost = 50;
const int t0updmg = 1;
const int t0uprange = 15;
const int t0uprate = -1;

//Base stats of Turret 1
const double t1cost = 200;
const double t1dmg = 10;
const double t1range = 250;
const double t1rate = 100;
//Upgrade mod for Turret 1
const double t1upcost = 100;
const double t1updmg = 10;
const double t1uprange = 50;
const double t1uprate = -4;

//Base stats of Turret 2
const double t2cost = 300;
const double t2dmg = 0.1;
const double t2range = 75;
const double t2rate = 0;
//Upgrade mod for Turret 2
const double t2upcost = 50;
const double t2updmg = 0.1;
const double t2uprange = 5;
const double t2uprate = 0;

//Base stats of Turret 3
const double t3cost = 400;
const double t3dmg = 1;
const double t3range = 100;
const double t3rate = 3;
//Upgrade mod for Turret 3
const double t3upcost = 100;
const double t3updmg = 0.5;
const double t3uprange = 15;
const double t3uprate = 0;

//Base stats of Turret 4
const double t4cost = 500;
const double t4dmg = 10;
const double t4range = 300;
const double t4rate = 4;
//Upgrade mod for Turret 4
const double t4upcost = 200;
const double t4updmg = 5;
const double t4uprange = 15;
const double t4uprate = 0;

// Container for corrners of the path
const int numnodes = 12;
sf::Vector2f pathnode[numnodes];
int nodedir[numnodes-1] = {0,1,2,3,2,1,2,3,0,1,2};

double point_distance(double x1, double y1, double x2, double y2)
{
	// returns the distance between two points
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (sqrt(a));
}
double sqr_distance(double x1, double y1, double x2, double y2)
{
	// returns the square of the distance between two points
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (a);
}

double point_direction(double x1, double y1, double x2, double y2)
{
	// returns the angle of a line in radians
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

int irandom(int v)//generate random int
{
	if (v>10)
	{return  (int)(v*rand()/RAND_MAX);}
	else
	{return rand()%v;}
}

int round(double num)//round number
{
	double dec;
	dec = modf(num,&num);
	if (dec >=0.5)
	{return (num+1);}
	else
	{return num;}
}

int expt(int num, int exp)//returns power of a number
{
	int tnum = num;
	for (int i=0; i<exp;i++)
	{
		num*=tnum;
	}
	return num;
}

std::string inttostring(int num)//change int to a string
{
	std::stringstream ss;
	ss << num;
	return ss.str();
}

//Patricle System
class particle
{
public:
	double x, y, velx, vely, ax, ay, gx, gy;
	int count;
	sf::Color colour;
	bool active, grav, grav2;
	void init(double x2, double y2, double velx2, double vely2, double acelx, double acely, double life, sf::Color c, int rad) 
	// sets position, speed, acceleration, lifetime, and colour
	{
		active = true;
		grav = false;
		grav2 = false;
		x = x2;
		y = y2;
		velx = velx2;
		vely = vely2;
		ax = acelx;
		ay = acely;
		colour = c;
		count = (int)life;
	}
	void init_sprite()
	{
		// old function, now used to set all particles to inactive
		active = false;
	}
	void run()
	{
		//moves particles
		x+=velx;		
		y+=vely;
		velx+=ax;
		vely+=ay;
		if ( (x<0) || (y<0) || (x>width) || (y>height) || count <0)
		{
			//deactivates particles when outside screen or lifetime expires
			active = false;
		}
		count--;

	}
};

int inactive(particle *shot2) //returns first inactive particle in array
{
	for (int i = 0; i<partCap; i++)
	{
		if (shot2[i].active == false)
		{
			return i;
		}
	}
	return 0;
};



class emitter //particle emitter
{
public:
	double x, y, velx, vely, preX, preY, dist, gx, gy;
	int life, numP, type, rad;
	bool active, grav;
	sf::Color colour;
	// life = lifetime
	// munP = number of particles per frame
	// type = pattern
	/*
	Types Explained:
	0 = stationary particle trail
	1 = cone of particles
	2 = Explosion
	3 = Implosion
	*/
	void init(double x2, double y2, double velx2, double vely2, double life2, sf::Color c, int radius, int pat, int numP2)
	// sets position, speed, lifetime, colour, radius(not always used), type, and number of particles released per frame
	{
		active = true;
		grav = false;
		x = x2;
		y = y2;
		velx = velx2;
		vely = vely2;
		colour = c;
		life = (int)life2;
		rad = radius;
		type = pat;
		numP = numP2;
	}	
	void initg (double x2, double y2)
	{
		//set position to gravitat toward
		grav = true;
		gx = x2;
		gy = y2;
	}
	void init_state()
	{
		//sets emmitter to inactive
		active = false;
	}
	void run(particle *parts, emitter *emits)
	{
		//move emittter
		preX = x;
		preY = y;
		if (grav == true)
		{ 
			//gravitation code
			double ac = -0.01*(10-point_distance(x,y,gx,gy));
			double dist = sqr_distance(x,y,gx,gy);
			velx+=ac*(gx-x)/dist;
			vely+=ac*(gy-y)/dist;
		}
		x+=velx;		
		y+=vely;
		if (type == 0)
		{
			//create stationary particle
			int a = inactive(parts);
			parts[a].init(x,y,0,0,0,0,life,colour,rad);
		}
		if (type == 1)
		{
			//create cone of particles
			dist = point_distance(0,0,velx,vely);
			double tvely = vely;
			double tvelx = velx;
			if (vely == 0) 
			{tvely = 2-0.1*irandom(50);}
			if (velx == 0) 
			{tvelx = 2-0.1*irandom(50);}
			dist = point_distance(0,0,tvelx,tvely);
			double cosd = (-tvelx)/dist;
			double sind = (-tvely)/dist;
			for(int i = 0; i<numP; i++)
			{
				int a = inactive(parts);
				parts[a].init(x,y,-tvelx*cosd*0.1*irandom(5),-tvely*sind*0.1*irandom(5),0,0,5+irandom(5),colour,1);
			}
		}
		if (type ==2)
		{
			//create explosion
			for(int i = 0; i<numP; i++)
			{
				int a = inactive(parts);
				parts[a].init(x,y,6-irandom(12),6-irandom(12),0,0,10+irandom(10),colour,1);
			}
		}
		if (type == 3)
		{
			//create implosion
			for(int i = 0; i<numP*3; i++)
			{
				int t = irandom(360);
				double rx = rad*cos(degtorad*t);
				double ry = rad*sin(degtorad*t);
				int a = inactive(parts);
				parts[a].init(x+rx,y+ry,-rx/life,-ry/life,0,0,life,colour,1);
			}
		}
		if ( (x<0) || (y<0) || (x>width) || (y>height) || life <0)
		{
			//set to inactive if outside of window or lifetime expires
			active = false;
		}
		life--;

	}
};

int inactive(emitter *shot2) //returns first inactive emitter
{
	for (int i = 0; i<emitCap; i++)
	{
		if (shot2[i].active == false)
		{
			return i;
		}
	}
	return 0;
}


//void draw_lightning(double x0, double y0, double x1, double y1, sf::RenderWindow &app)
//// draws is a lightning bolt between two points
//{
//	std::vector<sf::Vector2f> points;
//	points.push_back(sf::Vector2f(x0,y0));
//	x1 = round(x1);
//	y1 = round(y1);
//	double dir;
//	do
//	{
//		dir = radtodeg*point_direction(x0,y0,x1,y1)-90+irandom(180);
//		x0 += cos(degtorad*dir);
//		y0 += sin(degtorad*dir);
//		points.push_back(sf::Vector2f(x0,y0));
//	}while(!(round(x0)==x1) && !(round(y0)==y1));
//	sf::VertexArray bolt(sf::LinesStrip,points.size());
//	for(int i=0;i<points.size();i++)
//	{
//		bolt[i].position = points[i];
//		bolt[i].color.r = 255;
//		bolt[i].color.g = 255;
//		bolt[i].color.b = 255;
//	}
//	app.draw(bolt);
//}
class line
{
public:
	sf::Vector2f p0;
	sf::Vector2f p1;
	void init(float x0, float y0, float x1, float y1)
	{
		p0.x = x0;
		p0.y = y0;
		p1.x = x1;
		p1.y = y1;
	}
};
void bolt (double x0,double y0, double x1, double y1, double offset, double numgens,sf::RenderWindow &app)
{
	std::vector<line> lines;
	line temp;
	sf::Vector2f p0,p1,p2;
	temp.init(x0,y0,x1,y1);
	lines.push_back(temp);
	for(int i = 0;i<numgens;i++)
	{
		int size = lines.size();
		for(int j = 0;j<size;j++)
		{
			p0 = lines[j].p0;
			p2 = lines[j].p1;
			p1.x = (p2.x-p0.x)/2+p0.x;
			p1.y = (p2.y-p0.y)/2+p0.y;
			int displace = offset-irandom(offset*2);
			double dir = point_direction(p0.x,p0.y,p2.x,p2.y)+PI/2;
			p1.x += displace*cos(dir);
			p1.y += displace*sin(dir);
			line temp2;
			temp2.p0 = p1;
			temp2.p1 = p2;
			lines.push_back(temp2);
			lines[j].init(p0.x,p0.y,p1.x,p1.y);
			if(irandom(3)==0)
			{
				p2 = p1+(p1-p0);
				p0 = p1;
				temp2.init(p0.x,p0.y,p2.x,p2.y);
				lines.push_back(temp2);
			}
		}
		offset/=2;
	}
	sf::VertexArray bolt(sf::Quads,lines.size()*4);
	for(int i = 0; i<lines.size();i++)
	{
		double  dir = point_direction(lines[i].p0.x,lines[i].p0.y,lines[i].p1.x,lines[i].p1.y)+PI/2;
		bolt[i*4  ].position = sf::Vector2f(lines[i].p0.x+1*cos(dir),lines[i].p0.y+1*sin(dir));
		bolt[i*4  ].color    = sf::Color(255,25,25);
		bolt[i*4+1].position = sf::Vector2f(lines[i].p0.x-1*cos(dir),lines[i].p0.y-1*sin(dir));
		bolt[i*4+1].color    = sf::Color(255,25,25);
		bolt[i*4+2].position = sf::Vector2f(lines[i].p1.x+1*cos(dir),lines[i].p1.y+1*sin(dir));
		bolt[i*4+2].color    = sf::Color(255,25,25);
		bolt[i*4+3].position = sf::Vector2f(lines[i].p1.x-1*cos(dir),lines[i].p1.y-1*sin(dir));
		bolt[i*4+3].color    = sf::Color(255,25,25);
	}
	app.draw(bolt);
}
class bullet 
{
public:
	double x, y, velx, vely, dmg, dir;
	bool active, grav;
	int count2;
	double *gx,*gy;
	sf::Sprite bullet2;
	textureStorage *tex;

	void init(double x2, double y2, double velx2, double vely2, double dmg2)
	//sets position, speed and damage
	{
		active = true;
		x = x2;
		y = y2;
		velx = velx2;
		vely = vely2;
		dmg = dmg2;
		count2 = 0;
		dir = radtodeg*point_direction(0,0,velx,vely);
		bullet2.setRotation(dir);
		grav = false;
	}
	void init_grav(double *x2, double *y2)
	//sets point to gravitate toward
	{
		gx = x2;
		gy = y2;
		grav = true;
	}
	void init_sprite()
	{
		//sets the sprite for the bullet
		bullet2.setTexture(tex->dot);
		bullet2.setOrigin(4,4);
		active = false;
	}
	void run(emitter *emits)
	{
		if (grav == true)
		{ 
			//calculates gravitation by spring and adjust speed
			double ac = -0.7*(10-point_distance(x,y,*gx,*gy));
			double dist = sqr_distance(x,y,*gx,*gy);
			velx+=ac*(*gx-x)/dist;
			vely+=ac*(*gy-y)/dist;
			int a = inactive(emits);
			emits[a].init(x,y,velx,vely,1,sf::Color::White,1,1,1);
		}
		//move bullete
		x+=velx;		
		y+=vely;
		bullet2.setPosition(float(x),float(y));
		if ( (x<0) || (y<0) || (x>width) || (y>height) )
		{
			//sets bullet in inactive if outside of the window
			active = false;
		}

	}
};


int inactiveShot(bullet *shot2) //returns the first inactive bullet
{
	for (int i = 0; i<1000; i++)
	{
		if (shot2[i].active == false)
		{
			return i;
		}
	}
	return 0;
}

class enemy
{
public:
	double x, y, velx, vely, radius, hp;
	int count, dir, speed, type, value;
	bool active;
	sf::Sprite spr_self;
	textureStorage *tex;
	/*
	dir is direction:
	0 = right
	1 = up
	2 = left
	3 = down

	speed must equal a factor of 16 (1,2,4,8,16)

	Type:
	0 = spider
	1 = fireball
	2 = ghost
	3 = computer
	4 = angry statue
	*/
	void init(double health, double speed2, int type2, int value2)
	//sets health, speed, type, and money value. as well as seting posiion to the first path node.
	{
		active = true;
		x = pathnode[0].x;
		y = pathnode[0].y;
		speed = speed2;
		velx = speed;
		vely = 0;
		hp = health;
		type = type2;
		value = value2;
		if (type == 0) spr_self.setTexture(tex->spr_spider_right);
		if (type == 1) spr_self.setTexture(tex->spr_fireball_right);
		if (type == 2) spr_self.setTexture(tex->spr_ghost);
		if (type == 3) spr_self.setTexture(tex->spr_computer);
		if (type == 4) spr_self.setTexture(tex->spr_statue);
		spr_self.setOrigin(16,16);
		radius = 16;
		count = 0;
	}
	void run(bullet *shot)
	{
		//move enemy
		x+=velx;
		y+=vely;
		spr_self.setPosition(float(x),float(y)); // move sprite
		if ((int)x%16==0 && (int)y%16==0)//if cordinates are divisable by 16 then chich if it is in a corner
		{
			for (int i = 0; i <= numnodes; i++)
			{
				if (x == pathnode[i].x && y == pathnode[i].y)
				{
					if(i!=numnodes)
					{
						//change if on a corner change direction and sprite
						dir = nodedir[i];
						if (dir==0)
						{
							velx=speed;
							vely=0;
							if (type == 0) spr_self.setTexture(tex->spr_spider_right);
							if (type == 1) spr_self.setTexture(tex->spr_fireball_right);
							if (type == 2) spr_self.setTexture(tex->spr_ghost);
							if (type == 3) spr_self.setTexture(tex->spr_computer);
							if (type == 4) spr_self.setTexture(tex->spr_statue);
						}
						if (dir==1)
						{
							velx=0;
							vely=-speed;
							if (type == 0) spr_self.setTexture(tex->spr_spider_up);
							if (type == 1) spr_self.setTexture(tex->spr_fireball_up);
							if (type == 2) spr_self.setTexture(tex->spr_ghost);
							if (type == 3) spr_self.setTexture(tex->spr_computer);
							if (type == 4) spr_self.setTexture(tex->spr_statue);
						}
						if (dir==2)
						{
							velx=-speed;
							vely=0;
							if (type == 0) spr_self.setTexture(tex->spr_spider_left);
							if (type == 1) spr_self.setTexture(tex->spr_fireball_left);
							if (type == 2) spr_self.setTexture(tex->spr_ghost);
							if (type == 3) spr_self.setTexture(tex->spr_computer);
							if (type == 4) spr_self.setTexture(tex->spr_statue);
						}
						if (dir==3)
						{
							velx=0;
							vely=speed;
							if (type == 0) spr_self.setTexture(tex->spr_spider_down);
							if (type == 1) spr_self.setTexture(tex->spr_fireball_down);
							if (type == 2) spr_self.setTexture(tex->spr_ghost);
							if (type == 3) spr_self.setTexture(tex->spr_computer);
							if (type == 4) spr_self.setTexture(tex->spr_statue);
						}
					}
					break;
				}
			}
			if (x == pathnode[11].x && y == pathnode[11].y)
			{
				//if it is on the last node, move to first node and remove one life
				x = pathnode[0].x-16;
				y = pathnode[0].y;
				velx=speed;
				vely=0;
				if (type == 0) spr_self.setTexture(tex->spr_spider_right);
				if (type == 1) spr_self.setTexture(tex->spr_fireball_right);
				if (type == 2) spr_self.setTexture(tex->spr_ghost);
				if (type == 3) spr_self.setTexture(tex->spr_computer);
				if (type == 4) spr_self.setTexture(tex->spr_statue);
				life--;
			}
		}
		count++;
		if (count >7){count = 0;}//count frames of animation
		spr_self.setTextureRect(sf::IntRect(count*32,0,32,32));//clip textrue
		for(int i = 0; i<1000; i++)//check for collision with bullet
		{
			if (shot[i].active == true)
			{
				double dist =  (x-shot[i].x)*(x-shot[i].x)+(shot[i].y-y)*(shot[i].y-y);
				if (dist<radius*radius)
				{
					//if collision then subtract damage from health and deactivate bullet
					hp-=shot[i].dmg;
					shot[i].active=false;
				}
			}
		}		
		if (hp<=0)
		{
			//if health is less than zero add value to money and deactivate self
			money+=value;
			active=false;
		}	
	}
};

int inactiveShot(enemy *shot2)//return first inactive enemy
{
	for (int i = 0; i<enemynum; i++)
	{
		if (shot2[i].active == false)
		{
			return i;
		}
	}
	return 0;
}

class cturret
{
	int count, count2;
public:
	double frate,dmg,range, pulseR;
	double angle,x,y;
	int pulsechange, sprwidth, type, level, frames, value;
	textureStorage *tex;
	/*
	Types:
	0 = basic
	1 = long ranged
	2 = lightning 
	3 = rapid fire
	4 = swarm
	*/
	bool state, active;
	/*state:
	0 = aim;
	1 = fireing;
	*/
	int target;
	sf::Sprite spr_self;
	sf::Sprite spr_mark;
	sf::CircleShape radius;
	sf::VertexArray pulse;
	void init(double x2, double y2, double rate, double dmg2, double range2, int type2)
	//sets position, fireing delay, damage, range, and type
	{
		active = true;
		x = x2;
		y = y2;
		frate = rate;
		dmg = dmg2;
		count = (int)rate;
		count2 = 0;
		level = 1;
		range = range2;
		target = -1;
		state = false;
		//pulseR = (int)(range/2);
		pulseR = range;
		pulsechange = 5;
		type = type2;
		//sets sprite based on type
		if (type == 0)
		{
			spr_self.setTexture(tex->texTurret[0]);
			sprwidth = 32;
			spr_self.setTextureRect(sf::IntRect(0,0,sprwidth,20));
			spr_self.setOrigin(9,10);
			frames = 6;
			value = t0cost;
		}
		if (type == 1)
		{
			spr_self.setTexture(tex->texTurret[1]);
			sprwidth = 64;
			spr_self.setTextureRect(sf::IntRect(0,0,sprwidth,20));
			spr_self.setOrigin(9,10);
			frames = 6;
			value = t1cost;
		}
		if(type == 2)
		{
			spr_self.setTexture(tex->texTurret[2]);
			spr_self.setOrigin(16,16);
			spr_self.setTextureRect(sf::IntRect(0,0,32,32));
			value = t2cost;
		}
		if (type ==3)
		{
			spr_self.setTexture(tex->texTurret[3]);
			sprwidth = 36;
			spr_self.setTextureRect(sf::IntRect(0,0,sprwidth,20));
			spr_self.setOrigin(9,10);
			frames = 3;
			value = t3cost;
		}
		if (type ==4)
		{
			spr_self.setTexture(tex->texTurret[4]);
			sprwidth = 32;
			spr_self.setTextureRect(sf::IntRect(0,0,sprwidth,32));
			spr_self.setOrigin(16,16);
			frames = 0;
			value = t4cost;
		}
		//sets sprite for targeting marker
		spr_self.setPosition(x,y);
		spr_mark.setTexture(tex->marker);
		spr_mark.setOrigin(16,16);
		//sets radius circle shape
		radius.setRadius(range);
		radius.setPosition(x,y);
		radius.setOutlineColor(sf::Color(255,255,255,150));
		radius.setOutlineThickness(1);
		radius.setFillColor(sf::Color::Transparent);
		radius.setOrigin(range,range);
		//
		//pulse.resize(362);
		//pulse.setPrimitiveType(sf::TrianglesFan);
		//pulse[0].color = sf::Color(255,0,0,100);
		//pulse[0].position = sf::Vector2f(x,y);
		//for(int i = 1; i<=361; i++)
		//{
		//	pulse[i].color = sf::Color(255,0,0,10);
		//	pulse[i].position.x = x+pulseR*cos(i*degtorad);
		//	pulse[i].position.y = y+pulseR*sin(i*degtorad);
		//}
	}
	void run(enemy *skulls, bullet *round, sf::RenderWindow &app)
	{
		//if (pulseR> range)
		//{pulseR = (int)(range/4);}
		//pulseR+=pulsechange;
		//for(int i = 1; i<=361; i++)
		//{
		//	pulse[i].color = sf::Color(255,0,0,10);
		//	pulse[i].position.x = x+pulseR*cos(i*degtorad);
		//	pulse[i].position.y = y+pulseR*sin(i*degtorad);
		//}
		if (type==2)// if the turret is a lightning turret
		{
			//draw a lightning bolt to all enemies inside range and damage them as well
			for (int i = 0; i<enemynum; i++)
			{
				if (skulls[i].active==true && skulls[i].x > 0)
				{
					if(sqr_distance(x,y,skulls[i].x,skulls[i].y) <= range*range)
					{
						//draw_lightning(x,y,skulls[i].x,skulls[i].y,app);
						bolt(x,y,skulls[i].x,skulls[i].y,25,5,app);
						skulls[i].hp-=dmg;
					}
				}
			}
		}
		else// for all other turret types
		{
			if (target==-1)// if no enemy targeted
			{
				//find the nearest target
				double dist= sqr_distance(x,y,skulls[0].x,skulls[0].y);
				if (skulls[0].active == false)
				{dist = 1000000000;};
				target=0;
				for (int i = 1; i<enemynum; i++)
				{
					if (skulls[i].active==true)
					{
						double dist2= sqr_distance(x,y,skulls[i].x,skulls[i].y);
						if ( dist2<dist)
						{
							dist = dist2;
							target = i;
						}
					}
				}
			}
			if (skulls[target].active==false || point_distance(x,y,skulls[target].x,skulls[target].y)>(range) || skulls[target].x < 0)
			//if the target is out of range set target to none
			{
				target=-1;
			}
			spr_mark.setPosition(skulls[target].x,skulls[target].y);//put marker over targeted enemy
			double dir = point_direction(x,y,skulls[target].x,skulls[target].y);
			if (target>=0)// if there is a target
			{
				if (frate < count)//if enough frames have passed
				{
					//fire shot at target
					int a = inactiveShot(round);
					if (a>=0)
					{
						if (type == 4)//if homing turret
						{
							//fire homing bullet
							double dist = sqrt( (x-skulls[target].x)*(x-skulls[target].x)+(skulls[target].y-y)*(skulls[target].y-y));
							double hmov = skulls[target].velx*dist/20;
							double vmov = skulls[target].vely*dist/20;
							round[a].init(x,y,5-irandom(10),5-irandom(10),dmg);
							round[a].init_grav(&skulls[target].x,&skulls[target].y);
						}
						else
						{
							//fire normal bullet adn account for movement of the target
							double dist = sqrt( (x-skulls[target].x)*(x-skulls[target].x)+(skulls[target].y-y)*(skulls[target].y-y));
							double hmov = skulls[target].velx*dist/20;
							double vmov = skulls[target].vely*dist/20;
							double dir2 = point_direction(x,y,skulls[target].x+hmov,skulls[target].y+vmov);
							round[a].init(x,y,20*cos(dir2),20*sin(dir2),dmg);
						}
						count = 0;//reset frame counter
						state = true;//start fireing animation
						count2 = 0;//reset animation counter
					}
				}
			}
			if (state == true && !(type==4))
			{
				//clip texture to animate turret
				count2++;
				spr_self.setTextureRect(sf::IntRect(count2*sprwidth,0,sprwidth,20));
				if (count2>frames)
				{
					count2 = 0;
					state = false;
					spr_self.setTextureRect(sf::IntRect(0,0,sprwidth,20));
				}
			}
			count++;
			spr_self.setRotation(radtodeg*dir);//set rotation of the sprite
		}
	}
	void upgrade(double dmg2, double frate2, double range2)
	//upgrade turret and add upgrade cost to value.
	{
		dmg   += dmg2;
		frate += frate2;
		range += range2;
		radius.setRadius(range);
		radius.setOrigin(range,range);
		level++;
		if(type==0) {value+=level*t0upcost;}
		if(type==1) {value+=level*t1upcost;}
		if(type==2) {value+=level*t2upcost;}
		if(type==3) {value+=level*t3upcost;}
		if(type==4) {value+=level*t4upcost;}
	}
};

int inactiveShot(cturret *shot2) //return first inactive turret
{
	for (int i = 0; i<turretcap; i++)
	{
		if (shot2[i].active == false)
		{
			return i;
		}
	}
	return 0;
}

bool pathcheck(int x, int y, cturret *guns)
//check if cordinates are a valid poision for a new turret
{
	bool clear = true;
	for (int i = 0; i< numnodes; i++)
	{
		if (nodedir[i] == 0)
		{
			if (abs(y-pathnode[i].y)<=16 &&  pathnode[i].x <= x && x <= pathnode[i+1].x)
			{
				clear = false;
			}
		}
		if (nodedir[i] == 1)
		{
			if (abs(x-pathnode[i].x)<=16 &&  pathnode[i].y >= y && y >= pathnode[i+1].y)
			{
				clear = false;
			}
		}
		if (nodedir[i] == 2)
		{
			if (abs(y-pathnode[i].y)<=16 &&  pathnode[i].x >= x && x >= pathnode[i+1].x)
			{
				clear = false;
			}
		}
		if (nodedir[i] == 3)
		{
			if (abs(x-pathnode[i].x)<=16 &&  pathnode[i].y <= y && y <= pathnode[i+1].y)
			{
				clear = false;
			}
		}
	}
	for(int i=0; i<turretcap; i++)
	{
		if(x == guns[i].x && y == guns[i].y)
		{
			clear = false;
		}
	}
	if (x<0 || x>640 || y<0 || y >480)
	{
		clear = false;
	}
	return clear;
}

bool checkMoney(double num)// chec kto see if player has enough money
{
	if ((int)num>money){return false;}
	else {return true;}
}

class menu
//class containing all code for main menu, help sceen, ingame side bar, and recording high scores
{
public:
	int wave, screen, helpnum;
	/*
	Screen:
	0 = main menu;
	1 = help;
	2 = game;
	*/
	int buyselect, turretselect;
	/*
	-1 = none
	0 = basic turret
	1 = long range turret
	2 = lightning
	3 = rapid fire
	4 = homing
	*/
	bool mouseL,arrowL, arrowR;//hold boolean for left mouse button, leff and right arrow keys

	textureStorage *tex;

	//declation for text display
	sf::Text txtmoney;
	sf::Text txtTurret;
	sf::Text txtnextwave;
	sf::Text txtturret0price;
	sf::Text txtturret1price;
	sf::Text txtturret2price;
	sf::Text txtturret3price;
	sf::Text txtturret4price;
	sf::Text txtturretstats;
	sf::Text txtupgrade;
	sf::Text txtsell;
	sf::Text txthighscore;

	sf::Font font;

	//declation of rectangles, used for buttons
	sf::RectangleShape turretbox;
	sf::RectangleShape turretbtn[5];
	sf::RectangleShape nextwavebtn;
	sf::RectangleShape upgradebtn;
	sf::RectangleShape sellbtn;
	sf::RectangleShape mainmenubtn[3];
	
	//declations of sprite containers
	sf::Sprite dispturret0;
	sf::Sprite dispturret1;
	sf::Sprite dispturret2;
	sf::Sprite dispturret3;
	sf::Sprite dispturret4;
	sf::Sprite mturret[5];
	sf::Sprite helppage[4];
	sf::Sprite menuimg;

	//will be used to change variables to strings
	std::ostringstream strStats;
	std::ostringstream strTStats;
	void init(textureStorage *tex1)
	{
		tex = tex1;

		//initlize varibles
		money = 1000;
		life = 20;
		buyselect = 0;
		turretselect = -1;
		screen = 0;
		helpnum = 0;

		strStats << "Money: ";
		strStats << money;

		font.loadFromFile("arial.ttf");

		//set all of the text's position, character size, colour, and text(in some cases)
		txtmoney.setCharacterSize(16);
		txtmoney.setPosition(648,8);
		txtmoney.setColor(sf::Color::White);
		txtmoney.setFont(font);
		txtmoney.setString(sf::String(strStats.str()));

		txtTurret.setCharacterSize(16);
		txtTurret.setPosition(680,120);
		txtTurret.setFont(font);
		txtTurret.setString("Turrets");

		txtnextwave.setCharacterSize(16);
		txtnextwave.setPosition(660,64);
		txtnextwave.setColor(sf::Color::Black);
		txtnextwave.setFont(font);
		txtnextwave.setString("Next Wave");

		txtturret0price.setCharacterSize(16);
		txtturret0price.setPosition(760,148);
		txtturret0price.setColor(sf::Color::White);
		txtturret0price.setFont(font);
		txtturret0price.setString(inttostring(t0cost));
		
		txtturret1price.setCharacterSize(16);
		txtturret1price.setPosition(760,182);
		txtturret1price.setColor(sf::Color::White);
		txtturret1price.setFont(font);
		txtturret1price.setString(inttostring(t1cost));
		
		txtturret2price.setCharacterSize(16);
		txtturret2price.setPosition(760,216);
		txtturret2price.setColor(sf::Color::White);
		txtturret2price.setFont(font);
		txtturret2price.setString(inttostring(t2cost));

		txtturret3price.setCharacterSize(16);
		txtturret3price.setPosition(760,250);
		txtturret3price.setColor(sf::Color::White);
		txtturret3price.setFont(font);
		txtturret3price.setString(inttostring(t3cost));

		txtturret4price.setCharacterSize(16);
		txtturret4price.setPosition(760,284);
		txtturret4price.setColor(sf::Color::White);
		txtturret4price.setFont(font);
		txtturret4price.setString(inttostring(t4cost));

		txtturretstats.setCharacterSize(16);
		txtturretstats.setPosition(650,320);
		txtturretstats.setFont(font);
		txtturretstats.setColor(sf::Color::White);

		txtupgrade.setCharacterSize(16);
		txtupgrade.setPosition(690,423);
		txtupgrade.setColor(sf::Color::Black);
		txtupgrade.setFont(font);
		txtupgrade.setString("Upgrade");

		txtsell.setCharacterSize(16);
		txtsell.setPosition(700,450);
		txtsell.setColor(sf::Color::Black);
		txtsell.setFont(font);
		txtsell.setString("Sell");

		txthighscore.setCharacterSize(16);
		txthighscore.setPosition(32,320);
		txthighscore.setColor(sf::Color::White);
		txthighscore.setFont(font);
		std::ifstream fscore("highscore.txt");
		if(fscore.is_open())
		{
			//load high score from text file
			std::string num;
			std::getline(fscore,num);
			txthighscore.setString("Most Waves Survived: " + num);
			fscore.close();
		}
		else{std::cout << "Error missing file 'highscore.txt'\n";}

		//sets colour, size and position of rectangles
		turretbox.setFillColor(sf::Color::Transparent);
		turretbox.setOutlineColor(sf::Color::White);
		turretbox.setOutlineThickness(1);
		turretbox.setSize(sf::Vector2f(140,165));
		turretbox.setPosition(650,140);

		for(int i=0;i<5;i++)
		{
			turretbtn[i].setFillColor(sf::Color(155,155,155,255));
			turretbtn[i].setSize(sf::Vector2f(138,32));
			turretbtn[i].setPosition(651,140+i*33);
		}

		nextwavebtn.setSize(sf::Vector2f(140,32));
		nextwavebtn.setFillColor(sf::Color::Green);
		nextwavebtn.setPosition(650,64);

		upgradebtn.setSize(sf::Vector2f(140,24));
		upgradebtn.setFillColor(sf::Color::Green);
		upgradebtn.setPosition(650,420);

		sellbtn.setSize(sf::Vector2f(140,24));
		sellbtn.setFillColor(sf::Color::Red);
		sellbtn.setPosition(650,446);

		//sets the texture, and position of app sprites
		dispturret0.setTexture(tex->texTurret[0]);
		dispturret0.setTextureRect(sf::IntRect(0,0,32,20));
		dispturret0.setPosition(652,148);

		dispturret1.setTexture(tex->texTurret[1]);
		dispturret1.setTextureRect(sf::IntRect(0,0,64,20));
		dispturret1.setPosition(652,182);
		
		dispturret2.setTexture(tex->texTurret[2]);
		dispturret2.setPosition(652,206);

		dispturret3.setTexture(tex->texTurret[3]);
		dispturret3.setTextureRect(sf::IntRect(0,0,36,20));
		dispturret3.setPosition(652,244);

		dispturret4.setTexture(tex->texTurret[4]);
		dispturret4.setPosition(652,272);

		mturret[0].setTexture(tex->texTurret[0]);
		mturret[0].setTextureRect(sf::IntRect(0,0,32,20));
		mturret[0].setPosition(652,222);
		mturret[0].setOrigin(9,10);

		mturret[1].setTexture(tex->texTurret[1]);
		mturret[1].setTextureRect(sf::IntRect(0,0,64,20));
		mturret[1].setPosition(652,182);
		mturret[1].setOrigin(9,10);
				
		mturret[2].setTexture(tex->texTurret[2]);
		mturret[2].setTextureRect(sf::IntRect(0,0,32,32));
		mturret[2].setPosition(652,182);
		mturret[2].setOrigin(16,16);
		
		mturret[3].setTexture(tex->texTurret[3]);
		mturret[3].setTextureRect(sf::IntRect(0,0,36,20));
		mturret[3].setPosition(652,182);
		mturret[3].setOrigin(9,10);

		mturret[4].setTexture(tex->texTurret[4]);
		mturret[4].setTextureRect(sf::IntRect(0,0,32,32));
		mturret[4].setPosition(652,182);
		mturret[4].setOrigin(16,16);

		for(int i=0;i<4;i++)//load help pages
		{helppage[i].setTexture(tex->help[i]);}

		menuimg.setTexture(tex->menutxt);
		for(int i=0; i<3; i++)
		{
			mainmenubtn[i].setFillColor(sf::Color(0,255,0,255));
			mainmenubtn[i].setSize(sf::Vector2f(150,32));
			mainmenubtn[i].setPosition(325,190+i*90);
		}
	}

	void next(enemy *a)// create next wave
	{
		waveActive = true;
		//preset waves
		if (wave == 0)
		{
			for(int i = 0; i<10; i++)
			{
				a[i].init(10,2,0,1);
				a[i].x = -32*i;
			}
			wave++;
		}
		else if (wave == 1)
		{
			for(int i = 0; i<20; i++)
			{
				a[i].init(10,2,0,wave);
				a[i].x = -32*i;
			}
			wave++;
		}
		else if (wave == 2)
		{
			for(int i = 0; i<10; i++)
			{
				a[i].init(5,8,2,wave);
				a[i].x = -64*i;
			}
			wave++;
		}
		else if (wave == 3)
		{
			for(int i = 0; i<10; i++)
			{
				a[i].init(25,1,1,wave);
				a[i].x = -16*i;
			}
			wave++;
		}
		else//when pased preset waves, randomly generate waves
		{
			int speed = irandom(3)+1;//random speed
			int hp;
			//health is based on speed of enemy and wave number
			if (speed == 1){hp = wave+( ( (int)wave/2  )*( (int)wave/2  ) );}
			if (speed == 2){hp = wave+( ( (int)wave/3  )*( (int)wave/3  ) );}
			if (speed == 3){hp = wave+( ( (int)wave/5  )*( (int)wave/4  ) );}
			if (speed == 4){hp = wave+( ( (int)wave/10 )*( (int)wave/10 ) );}
			if (speed == 5){hp = wave+( ( (int)wave/20 )*( (int)wave/20 ) );}
			int wavesize = wave+abs(3-speed)*10;//the number of enemyies genrated is based o nthe speed
			int type = irandom(5);//select enemy sprite
			for(int i = 0; i<wavesize;i++)
			{
				//spawn enemies
				a[i].init(hp,expt(2,speed),type,(int)(wave/2));
				a[i].x = -16*i;
			}
			wave++;
		}
	}
	void run(cturret *guns, sf::RenderWindow &app, enemy *skull)
	{
		if (screen == 0)// if on the main menu
		{
			double mx,my;//mouse position
			mx = sf::Mouse::getPosition(app).x;
			my = sf::Mouse::getPosition(app).y;

			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouseL!=sf::Mouse::isButtonPressed(sf::Mouse::Left))//if left click
			{
				if (mx>325 && mx <475 && my>190 && my<222)//if on start button go to game
				{
					screen = 2;
				}
				if (mx>325 && mx <475 && my>280 && my<312)//if on help button go to hel pages
				{
					screen = 1;
					helpnum=0;
				}
				if (mx>325 && mx <475 && my>370 && my<402)// if over exit button exit
				{
					app.close();
				}
			}
		}
		if (screen == 1)//if on help screen
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && arrowR == false)//next page
			{
				if(helpnum<4)
				{helpnum++;}
				if(helpnum>=4)
				{
					// if on last page return to main menu
					helpnum=0;
					screen=0;
				}
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) && arrowR == false)//previous page
			{
				if(helpnum<1)
				{helpnum--;}
			}
		}
		if (screen == 2)// in game screen
		{
			double mx,my;
			mx = sf::Mouse::getPosition(app).x;
			my = sf::Mouse::getPosition(app).y;
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouseL!=sf::Mouse::isButtonPressed(sf::Mouse::Left))//if left click
			{
				for(int i = 0; i<5; i++)//check turret puchase buttons
				{
					if (mx>650 && mx <788 && my>140+33*i && my<140+33*(i+1))
					{
						buyselect = i;
						break;
					}
				}
				if (mx>650 && mx <790 && my>64 && my<96)//send next wave if all enemy have been defeated
				{
					bool clear = true;
					 for(int i = 0; i<enemynum; i++)
					 {
						 if (skull[i].active == true)
						 {clear = false;}
					 }
					 if (clear == true)
					 {
						 next(skull);
					 }
				}
				bool col = false;
				for(int i=0; i<turretcap; i++)//check if a turret has been clicked on
				{
					if (abs(mx-guns[i].x) < 16 && abs(my-guns[i].y)< 16 && guns[i].active == true)
					{
						turretselect = i;
						col = true;
					}
				}
				if (mx>650 && mx <790 && my>420 && my<452 && turretselect>=0)
				//if a turret is selected, and mouse clicks on upgrade button, check money and upgrade turret
				{
					if (guns[turretselect].type == 0)
					{
						if(checkMoney((guns[turretselect].level-1)*t0upcost))
						{
							guns[turretselect].upgrade(t0updmg,t0uprate,t0uprange);
							money -= (guns[turretselect].level-1)*t0upcost;
						}
					}
					if (guns[turretselect].type == 1)
					{
						
						if(checkMoney((guns[turretselect].level-1)*t1upcost))
						{					
							guns[turretselect].upgrade(t1updmg,t1uprate,t1uprange);
							money -= (guns[turretselect].level-1)*t1upcost;
						}
					}
					if (guns[turretselect].type == 2)
					{
						if(checkMoney((guns[turretselect].level-1)*t2upcost))
						{			
							guns[turretselect].upgrade(t2updmg,t2uprate,t2uprange);
							money -= (guns[turretselect].level-1)*t2upcost;
						}
					}
					if (guns[turretselect].type == 3)
					{
						if(checkMoney((guns[turretselect].level-1)*t3upcost))
						{			
							guns[turretselect].upgrade(t3updmg,t3uprate,t3uprange);
							money -= (guns[turretselect].level-1)*t3upcost;
						}
					}
					if (guns[turretselect].type == 4)
					{
						
						if(checkMoney((guns[turretselect].level-1)*t4upcost))
						{			
							guns[turretselect].upgrade(t4updmg,t4uprate,t4uprange);
							money -= (guns[turretselect].level-1)*t4upcost;
						}
					}
					col = true;
				}
				if(mx>650 && mx <790 && my>446 && my<460)
				// if turret is selected and sell button is clicked sell turret for 75% or total value and deactivate turret
				{
					if (guns[turretselect].active == true)
					{
					strTStats << "Sell Price: ";
					if (guns[turretselect].type == 0){money += t0cost*0.75+(guns[turretselect].level-1)*0.75*t0upcost;}
					if (guns[turretselect].type == 1){money += t1cost*0.75+(guns[turretselect].level-1)*0.75*t1upcost;}
					if (guns[turretselect].type == 2){money += t2cost*0.75+(guns[turretselect].level-1)*0.75*t2upcost;}
					if (guns[turretselect].type == 3){money += t3cost*0.75+(guns[turretselect].level-1)*0.75*t3upcost;}
					money+=guns[turretselect].value;
					guns[turretselect].active = false;
					}
					col = true;
				}
				if (col == false)
				// if a turret is not clicked on deselect turret
				{
					turretselect = -1;
				}
			}
			if (mx>650 && mx <790 && my>420 && my<452 && turretselect>=0)
			// if the mouse if over the upgrade button and a turret is selected, write stats and upgraded stats
				{
					strTStats.str("");
					if (guns[turretselect].type == 0)
					{
						strTStats << "Basic Turret:" <<std::endl 
								  << "Damage: "       << guns[turretselect].dmg   << " + " << t0updmg       << std::endl
				 				  << "Fireing Delay: "<< guns[turretselect].frate << " - " << abs(t0uprate) << std::endl
								  << "Range: "        << guns[turretselect].range << " + " << t0uprange     << std::endl
								  << "Upgrade Cost: " << t0upcost*guns[turretselect].level << std::endl;
					}
					if (guns[turretselect].type == 1)
					{
						strTStats << "Long Ranged Turret:" <<std::endl 
								  << "Damage: "       << guns[turretselect].dmg   << " + " << t1updmg       << std::endl
				 				  << "Fireing Delay: "<< guns[turretselect].frate << " - " << abs(t1uprate) << std::endl
								  << "Range: "        << guns[turretselect].range << " + " << t1uprange     << std::endl
								  << "Upgrade Cost: " << t1upcost*guns[turretselect].level << std::endl;;
					}
					if (guns[turretselect].type == 2)
					{
						strTStats << "Lightning Turret:" <<std::endl 
								  << "Damage: "       << guns[turretselect].dmg   << " + " << t2updmg       << std::endl
				 				  << "Fireing Delay: "<< guns[turretselect].frate << " - " << abs(t2uprate) << std::endl
								  << "Range: "        << guns[turretselect].range << " + " << t2uprange     << std::endl
								  << "Upgrade Cost: " << t2upcost*guns[turretselect].level << std::endl;;
					}
					if (guns[turretselect].type == 3)
					{
						strTStats << "Rapid Fire Turret:" <<std::endl 
								  << "Damage: "       << guns[turretselect].dmg   << " + " << t3updmg       << std::endl
				 				  << "Fireing Delay: "<< guns[turretselect].frate << " - " << abs(t3uprate) << std::endl
								  << "Range: "        << guns[turretselect].range << " + " << t3uprange     << std::endl
								  << "Upgrade Cost: " << t3upcost*guns[turretselect].level << std::endl;;
					}
					if (guns[turretselect].type == 4)
					{
						strTStats << "Homing Turret:" <<std::endl 
								  << "Damage: "       << guns[turretselect].dmg   << " + " << t4updmg       << std::endl
				 				  << "Fireing Delay: "<< guns[turretselect].frate << " - " << abs(t4uprate) << std::endl
								  << "Range: "        << guns[turretselect].range << " + " << t4uprange     << std::endl
								  << "Upgrade Cost: " << t4upcost*guns[turretselect].level << std::endl;;
					}
				}
			else if (turretselect>=0)
			// if a turret is selected display stats
				{
					strTStats.str("");
					if (guns[turretselect].type == 0){strTStats << "Basic Turret:" <<std::endl;}
					if (guns[turretselect].type == 1){strTStats << "Long Ranged Turret:" <<std::endl;}
					if (guns[turretselect].type == 2){strTStats << "Lightning Turret:" <<std::endl;}
					if (guns[turretselect].type == 3){strTStats << "Rapid Fire Turret:" <<std::endl;}
					if (guns[turretselect].type == 4){strTStats << "Homing Turret:" <<std::endl;}
					strTStats << "Damage: "       << guns[turretselect].dmg   << std::endl
				 			  << "Fireing Delay: "<< guns[turretselect].frate << std::endl
							  << "Range: "        << guns[turretselect].range << std::endl;
					if(mx>650 && mx <790 && my>446 && my<470)
					//if mouse is over the sell button display the sell price
					{
						strTStats << "Sell Price: ";
						if (guns[turretselect].type == 0){strTStats << t0cost*0.75+(guns[turretselect].level-1)*0.75*t0upcost <<std::endl;}
						if (guns[turretselect].type == 1){strTStats << t1cost*0.75+(guns[turretselect].level-1)*0.75*t1upcost <<std::endl;}
						if (guns[turretselect].type == 2){strTStats << t2cost*0.75+(guns[turretselect].level-1)*0.75*t2upcost <<std::endl;}
						if (guns[turretselect].type == 3){strTStats << t3cost*0.75+(guns[turretselect].level-1)*0.75*t3upcost <<std::endl;}
						if (guns[turretselect].type == 3){strTStats << t4cost*0.75+(guns[turretselect].level-1)*0.75*t4upcost <<std::endl;}
					}
				}
			else
			// in all other cases do not draw stats
			{
				strTStats.str("");
			}
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			//when space bar is pressed
			 {
				 double tx,ty;
				 //round cordinates to neares 16
				 tx = 16*round((double)sf::Mouse::getPosition(app).x/16);
				 ty = 16*round((double)sf::Mouse::getPosition(app).y/16);
				 //mturret[buyselect].setOrigin(9,10);
				 mturret[buyselect].setPosition(tx,ty);
				 app.draw(mturret[buyselect]);//draw temporary turret
				 if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && mouseL!=sf::Mouse::isButtonPressed(sf::Mouse::Left) && pathcheck(tx,ty,guns))
				 // if left click check money and then place turret
				 {
					 int a = inactiveShot(guns);
					 if(guns[a].active == false) 
					 {
						 if (buyselect == 0)
						 {
							 if(checkMoney(t0cost))
							 {
								guns[a].init(tx,ty,t0rate,t0dmg,t0range,0);
								money-=t0cost;
							 }
						 }
						 if (buyselect == 1)
						 {
							 if(checkMoney(t1cost))
							 {
								 guns[a].init(tx,ty,t1rate,t1dmg,t1range,1);
								 money-=t1cost;
							 }
						 }
						 if (buyselect == 2)
						 {
							 if(checkMoney(t2cost))
							 {
								 guns[a].init(tx,ty,t2rate,t2dmg,t2range,2);
								 money-=t2cost;
							 }
						 }
						 if (buyselect == 3)
						 {
							 if(checkMoney(t3cost))
							 {
								 guns[a].init(tx,ty,t3rate,t3dmg,t3range,3);
								 money-=t3cost;
							 }
						 }
						 if (buyselect == 4)
						 {
							 if(checkMoney(t4cost))
							 {
								 guns[a].init(tx,ty,t4rate,t4dmg,t4range,4);
								 money-=t4cost;
							 }
						 }
					 }
				 }
			 }
			if(!sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
			//if space is not pressed move turret sprite off screen
			{
				for(int i=0;i<5;i++)
				{
					mturret[i].setPosition(-10,10);
				}
			}
			if(life<=0)
			//if not life is remaining
			{
				//load previous highscore
				screen = 0;
				std::ifstream fscore;
				fscore.open("highscore.txt");
				if(fscore.is_open())
				{
					std::string num;
					std::getline (fscore,num);
					int hscore = std::atoi(num.c_str() );
					if (hscore<wave)//if new score is greater than prvious change the file
					{
						txthighscore.setString("Most Waves Survived: " + inttostring(wave));
						fscore.close();
						std::ofstream fscore2("highscore.txt");
						fscore2 << inttostring(wave);
						fscore2.close();
					}
					fscore.close();
				}
				else
				{std::cout << "Error missing file 'highscore.txt'\n";}
				//reset game
				wave = 0;
				life = 20;
				money = 100;
				for (int i = 0; i<enemynum; i++)
				{
					skull[i].active =false;
				}
				for(int i = 0; i<turretcap; i++)
				{
					guns[i].active=false;
				}

			}
		}
		//chick if arrow keys and mouse ar pressed
		mouseL = sf::Mouse::isButtonPressed(sf::Mouse::Left);
		arrowL = sf::Keyboard::isKeyPressed(sf::Keyboard::Left);
		arrowR = sf::Keyboard::isKeyPressed(sf::Keyboard::Right);
	}

	void draw(sf::RenderWindow &app)
	//draw menu
	{
		if (screen == 0)//if on main menu draw it
		{
			app.draw(mainmenubtn[0]);
			app.draw(mainmenubtn[1]);
			app.draw(mainmenubtn[2]);
			app.draw(menuimg);
			app.draw(txthighscore);
		}
		if (screen == 1)// if on help screen draw help pages
		{
			app.draw(helppage[helpnum]);
		}
		if (screen == 2)//if in game draw side bar
		{
			strStats.str("");
			strStats << "Money: " << money << std::endl << "Life: " << life << std::endl << "Wave #" << wave;
			txtmoney.setString(sf::String(strStats.str()));
			app.draw(turretbox);
			app.draw(txtmoney);
			app.draw(nextwavebtn);
			app.draw(txtnextwave);
			app.draw(txtTurret);
			if(turretselect>=0)
			{
				txtturretstats.setString(strTStats.str());
				app.draw(txtturretstats);
			}
			for(int i = 0; i<5; i++)
			{
				app.draw(turretbtn[i]);
			}
			app.draw(upgradebtn);
			app.draw(txtupgrade);
			app.draw(sellbtn);
			app.draw(txtsell);
			app.draw(dispturret0);
			app.draw(dispturret1);
			app.draw(dispturret2);
			app.draw(dispturret3);
			app.draw(dispturret4);
			app.draw(txtturret0price);
			app.draw(txtturret1price);
			app.draw(txtturret2price);
			app.draw(txtturret3price);
			app.draw(txtturret4price);

		}
	}
};

int main()
{
    sf::RenderWindow App(sf::VideoMode (width, height, 32), "turret");//set size of render window
	App.setFramerateLimit(60);//set frame rate cap
	//set position of corners of path
	pathnode[0]  = sf::Vector2f(0,432);
	pathnode[1]  = sf::Vector2f(592,432);
	pathnode[2]  = sf::Vector2f(592,48);
	pathnode[3]  = sf::Vector2f(400,48);
	pathnode[4]  = sf::Vector2f(400,176);
	pathnode[5]  = sf::Vector2f(208,176);
	pathnode[6]  = sf::Vector2f(208,48);
	pathnode[7]  = sf::Vector2f(48,48);
	pathnode[8]  = sf::Vector2f(48,368);
	pathnode[9]  = sf::Vector2f(496,368);
	pathnode[10] = sf::Vector2f(496,272);
	pathnode[11] = sf::Vector2f(0,272);
	
	textureStorage tex;

	//initlize object arrays
	bullet *shots;
	shots = new bullet[1000];
	enemy *skull;
	skull = new enemy[enemynum];
	cturret *guns;
	guns = new cturret[turretcap];
	particle *particles;
	particles = new particle[partCap];
	emitter *test;
	test = new emitter[emitCap];
	menu control;

	

	//initlize sprites
	sf::Sprite temp;
	sf::Sprite sprite;
	sf::Sprite map;

	//create clock for frame rate counter
	sf::Clock clock;
    float lastTime = 0;

	//load images in files to textures in memory

	//initlize object variables
	int count = 0;

	for (int i = 0; i<enemynum; i++)
	{
		skull[i].tex = &tex;
		skull[i].active=false;
	}
	for (int i = 0; i<1000; i++)
	{
		shots[i].tex = &tex;
		shots[i].init_sprite();
	}
	for (int i = 0; i<turretcap; i++)
	{
		guns[i].tex = &tex;
		guns[i].active = false;
	}

	int partcount = 0;
	for (int i=0; i<partCap; i++)
	{
		particles[i].init_sprite();
	}
	for (int i=0; i<emitCap; i++)
	{
		test[i].init_state();
	}
	
	control.wave = 0;
	control.init(&tex);

	map.setTexture(tex.TexMap);

    // Start game loop
    while (App.isOpen())
     {
         // Process events
         sf::Event event;
         while (App.pollEvent(event))
         {
             // Close window : exit
             if (event.type == sf::Event::Closed)
                 App.close();
		 }
		 if (sf::Keyboard::isKeyPressed(sf::Keyboard::N))//send next wave
		 {
			 bool next = true;
			 for(int i = 0; i<enemynum; i++)
			 {
				 if (skull[i].active == true)
				 {next = false;}
			 }
			 if (next == true)
			 {
				 control.next(skull);
			 }
		 }

		 if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))//save screen to image file
		 {
			 sf::Image temp = App.capture();
			 temp.saveToFile("image.png");
		 }
		 if (waveActive == true)//check if wave is clear, if it is remove all bullets
		 {
			bool clear = true;
			for(int i = 0; i<enemynum; i++)
			{
				if (skull[i].active == true)
				{clear = false;}
			}
			if (clear == true)
			{
				waveActive = false;
				for(int i=0;i<1000;i++)
				{
					shots[i].active = false;
				}
				money += control.wave*50;
			}
		 }
		App.clear();//clear screen
		//if in game draw map
		if(control.screen == 2) App.draw(map);
		//run all objects and draw all active ones
		control.run(guns,App, skull);
		for (int i = 0; i<1000; i++)
		{
			if (shots[i].active == true)
			{
				shots[i].run(test);
				if(!shots[i].grav)
				{App.draw(shots[i].bullet2);}
			}
		}
		for(int i = 0; i<turretcap; i++)
		{
			if (guns[i].active == true)
			{
				guns[i].run(skull, shots, App);
			}
		}
		bool disprad = sf::Keyboard::isKeyPressed(sf::Keyboard::A);
		for(int i = 0; i<turretcap; i++)
		{
			if (guns[i].active == true)
			{
				App.draw(guns[i].spr_self);
				if(guns[i].target!=-1){App.draw(guns[i].spr_mark);}
				if(disprad){App.draw(guns[i].radius);}
			}
		}
		for (int i = 0; i<enemynum; i++)
		{
			if (skull[i].active == true)
			{
				skull[i].run(shots);
				App.draw(skull[i].spr_self);
			}
		}
		partcount = 0;
		for (int i=0; i<emitCap; i++)
		{
			if (test[i].active == true)
			{
				test[i].run(particles, test);
			}
		}
		for (int i=0; i<partCap; i++)
		{
			if (particles[i].active == true)
			{
				particles[i].run();
				partcount++;
			}
		}
		int count2 = 0;
		sf::VertexArray points(sf::Points,partcount);
		for (int i=0; i<partCap; i++)
		{
			if (particles[i].active == true)
			{
				points[count2].color = particles[i].colour;
				points[count2].position.x = particles[i].x;
				points[count2].position.y = particles[i].y;
				count2++;
			}
		}
		App.draw(points);//draws particles
		control.draw(App);

		//calculate frame rate and display
		float currentTime = clock.restart().asSeconds();
        float fps = 1.f / currentTime;
        lastTime = currentTime;
		std::stringstream out;
		out << "Just Another Random Tower Defence     FPS: " << (int)fps;
		App.setTitle(out.str());

		App.display();
    }
    return EXIT_SUCCESS;
}