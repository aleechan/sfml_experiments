#include <SFML\Graphics.hpp>
#pragma once
class textureStorage
{
public:

	sf::Texture texTurret[5];
	sf::Texture dot;
	sf::Texture spr_enemy;
	sf::Texture spr_spider_up;
	sf::Texture spr_spider_down;
	sf::Texture spr_spider_right;
	sf::Texture spr_spider_left;
	sf::Texture spr_fireball_up;
	sf::Texture spr_fireball_down;
	sf::Texture spr_fireball_right;
	sf::Texture spr_fireball_left;
	sf::Texture spr_ghost;
	sf::Texture spr_computer;
	sf::Texture spr_statue;
	sf::Texture marker;
	sf::Texture TexMap;
	sf::Texture help[4];
	sf::Texture menutxt;

	textureStorage()
	{
		texTurret[0].loadFromFile("sprturret0fire_strip.png");
		texTurret[1].loadFromFile("sprturret1fire_strip7.png");
		texTurret[2].loadFromFile("sprturret2.png");
		texTurret[3].loadFromFile("sprturret4fire_strip4.png");
		texTurret[4].loadFromFile("sprturret3.png");

		dot.loadFromFile("sprbullet.png");

		spr_enemy.loadFromFile("skeleton_walking_strip10.png");

		spr_spider_up.loadFromFile("spider_up_strip8.png");
		spr_spider_down.loadFromFile("spider_down_strip8.png");
		spr_spider_right.loadFromFile("spider_right_strip8.png");
		spr_spider_left.loadFromFile("spider_left_strip8.png");

		spr_fireball_up.loadFromFile("fireball_up_strip8.png");
		spr_fireball_down.loadFromFile("fireball_down_strip8.png");
		spr_fireball_right.loadFromFile("fireball_right_strip8.png");
		spr_fireball_left.loadFromFile("fireball_left_strip8.png");

		spr_ghost.loadFromFile("ghost_floating_strip8.png");

		spr_computer.loadFromFile("computer_strip8.png");

		spr_statue.loadFromFile("statue_strip8.png");

		help[0].loadFromFile("help_page_0.png");
		help[1].loadFromFile("help_page_1.png");
		help[2].loadFromFile("help_page_2.png");
		help[3].loadFromFile("help_page_3.png");

		menutxt.loadFromFile("menu.png");

		TexMap.loadFromFile("map.png");
	}

};

