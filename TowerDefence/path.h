#include <vector>
#include <SFML\System.hpp>

#pragma once
class path
{
	std::vector<sf::Vector2f> nodes;
	std::vector<int> nodedir;
public:
	path()
	{
		nodes.push_back( sf::Vector2f(0,432));
		nodes.push_back( sf::Vector2f(592,432));
		nodes.push_back( sf::Vector2f(592,48));
		nodes.push_back( sf::Vector2f(400,48));
		nodes.push_back( sf::Vector2f(400,176));
		nodes.push_back( sf::Vector2f(208,176));
		nodes.push_back( sf::Vector2f(208,48));
		nodes.push_back( sf::Vector2f(48,48));
		nodes.push_back( sf::Vector2f(48,368));
		nodes.push_back( sf::Vector2f(496,368));
		nodes.push_back( sf::Vector2f(496,272));
		nodes.push_back( sf::Vector2f(0,272));

		nodedir.push_back(0);
		nodedir.push_back(1);
		nodedir.push_back(2);
		nodedir.push_back(3);
		nodedir.push_back(2);
		nodedir.push_back(1);
		nodedir.push_back(2);
		nodedir.push_back(3);
		nodedir.push_back(0);
		nodedir.push_back(1);
		nodedir.push_back(2);
	}
	~path()
	{
		nodes.clear();
		nodedir.clear();
	}
	bool pathCheck(sf::Vector2f pos)
	{
		if (pos.x<0 || pos.x>640 || pos.y<0 || pos.y >480)
		{
			return false;
		}
		for(int i = 0; i < nodes.size()-1; i++)
		{
			if(nodedir[i] == 0)
			{
				if( abs(pos.y - nodes[i].y) <= 16 && nodes[i].x <= pos.x && pos.x <=nodes[i+1].x)
				{
					return false;
				}
			}
			else if(nodedir[i] == 1)
			{
				if( abs(pos.x - nodes[i].x) <= 16 && nodes[i].y <= pos.y && pos.y <=nodes[i+1].y)
				{
					return false;
				}
			}
			else if(nodedir[i] == 2)
			{
				if( abs(pos.y - nodes[i].y) <= 16 && nodes[i].x >= pos.x && pos.x >=nodes[i+1].x)
				{
					return false;
				}
			}
			else //if(nodedir[i] == 3)
			{
				if( abs(pos.x - nodes[i].x) <= 16 && nodes[i].y >= pos.y && pos.y >=nodes[i+1].y)
				{
					return false;
				}
			}
		}
		return true;
	}
};

