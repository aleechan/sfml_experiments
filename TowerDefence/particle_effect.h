#include <iostream>
#include <windows.h>
#include "my_math.h"
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>

#ifndef _PARTICLE_SYS
#define _PARTICLE_SYS

class particle
{
public:
	double x, y, velx, vely, ax, ay, gx, gy;
	int count;
	sf::Color colour;
	bool active, grav, grav2;
	void init(double x2, double y2, double velx2, double vely2, double acelx, double acely, double life, sf::Color c, int rad)
	{
		active = true;
		grav = false;
		grav2 = false;
		x = x2;
		y = y2;
		velx = velx2;
		vely = vely2;
		ax = acelx;
		ay = acely;
		colour = c;
		count = (int)life;
	}
	void init_state()
	{
		active = false;
	}
	void run()
	{
		x+=velx;		
		y+=vely;
		velx+=ax;
		vely+=ay;

		if ( (x<0) || (y<0) || (x>width) || (y>height) || count <0)
		{
			active = false;
		}
		count--;

	}
};

int inactive(particle *shot2, int partCap)
{
	for (int i = 0; i<partCap; i++)
	{
		if (shot2[i].active == false)
		{
			return i;
		}
	}
	return 0;
}

class emitter
{
public:
	double x, y, velx, vely, preX, preY, dist, gx, gy;
	int life, numP, type, rad, totalparts;
	bool active, grav, track;
	double *x1, *y1;
	sf::Color colour;
	// life = life time
	// munP = number of particles per frame
	// type = pattern
	void init(double x2, double y2, double velx2, double vely2, double life2, sf::Color c, int radius, int pat, int numP2, int totalparts0)
	{
		active = true;
		grav = false;
		track = false;
		x = x2;
		y = y2;
		velx = velx2;
		vely = vely2;
		colour = c;
		life = (int)life2;
		rad = radius;
		type = pat;
		numP = numP2;
		totalparts = totalparts0;
	}
	void set_tracking( double *x2, double *y2)
	{
		track = true;
		x1 = x2;
		y1 = y2;
	}
	void init_state()
	{
		active = false;
	}
	void run(particle *parts, emitter *emits)
	{
		if(type != 6)
		{
			if(track)
			{
				x = *x1;
				y = *y1;
			}
			preX = x;
			preY = y;
			x+=velx;		
			y+=vely;
		}
		if (type == 0)
		{
			int a = inactive(parts,totalparts);
			parts[a].init(x,y,0,0,0,0,life,colour,rad);
		}
		if (type == 1)
		{
			float angle = radtodeg*atan2(velx,vely);
			angle+=180;
			float sinmin = sin(degtorad*(angle-5));
			float cosmin = cos(degtorad*(angle-5));
			float sinmax = sin(degtorad*(angle+5));
			float cosmax = cos(degtorad*(angle+5));
			float dx = cosmax-cosmin;
			float dy = sinmax-sinmin;
			
			for(int i = 0; i<numP; i++)
			{
				int a = inactive(parts,totalparts);
				int b = 1+irandom(6); 
				parts[a].init(x,y,b*(sinmin+0.01*(irandom(100))*dy),b*(cosmin+0.01*(irandom(100))*dx),0,0,5+irandom(15),colour,1);
			}
		}
		if (type ==2)
		{
			for(int i = 0; i<numP; i++)
			{
				int a = inactive(parts,totalparts);
				parts[a].init(x,y,6-irandom(12),6-irandom(12),0,0,10+irandom(10),colour,1);
			}
		}
		if (type == 3)
		{
			for(int i = 0; i<numP; i++)
			{
				int t = irandom(360);
				double rx = rad*cos(degtorad*t);
				double ry = rad*sin(degtorad*t);
				int a = inactive(parts,totalparts);
				parts[a].init(x+rx,y+ry,-rx/life,-ry/life,0,0,life,colour,1);
			}
		}
		if (type == 4)
		{
			for(int i = 0; i<numP; i++)
			{
				int a = inactive(parts,totalparts);
				parts[a].init(x,y,0.1*(50-irandom(100)),0.1*(50-irandom(100)),0,0,10,colour,1);
			}
		}
		if (type == 5)
		{
			float angle = radtodeg*atan2(velx,vely);
			angle+=180;
			float sinmin = sin(degtorad*(angle-5));
			float cosmin = cos(degtorad*(angle-5));
			float sinmax = sin(degtorad*(angle+5));
			float cosmax = cos(degtorad*(angle+5));
			float dx = cosmax-cosmin;
			float dy = sinmax-sinmin;
			
			for(int i = 0; i<numP; i++)
			{
				int a = inactive(parts,totalparts);
				int b = 5+irandom(15); 
				parts[a].init(x,y,b*(sinmin+0.01*(irandom(100))*dy),b*(cosmin+0.01*(irandom(100))*dx),0,0,5+irandom(15),colour,1);
			}
		}
		if (type == 6)
		{
			float angle = radtodeg*atan2(velx,vely);
			float sinmin = sin(degtorad*(angle-10));
			float cosmin = cos(degtorad*(angle-10));
			float sinmax = sin(degtorad*(angle+10));
			float cosmax = cos(degtorad*(angle+10));
			float dx = cosmax-cosmin;
			float dy = sinmax-sinmin;
			
			for(int i = 0; i<numP; i++)
			{
				int a = inactive(parts,totalparts);
				int b = 5+irandom(15); 
				float c = 0.0001*irandom(10000);
				parts[a].init(x,y,b*(sinmin+c*dy),b*(cosmin+c*dx),0,0.5,50+irandom(15),colour,1);
			}

		}
		if ( /*(x<0) || (y<0) || (x>width) || (y>height) ||*/ life <0)
		{
			//if (type == 1)
			//{
			//	for(int i = 0; i<numP*5; i++)
			//	{
			//		int a = inactive(parts,totalparts);
			//		parts[a].init(x,y,6-irandom(12),-6-irandom(10),0,1+0.05*irandom(10),10+irandom(10),colour,1);
			//	}
			//}
			active = false;
		}
		life--;

	}
};

int inactive(emitter *shot2, int emitCap)
{

	for (int i = 0; i<emitCap; i++)
	{
		if (shot2[i].active == false)
		{
			return i;
		}
	}
	return 0;
}

class particle_system
{
public:
	particle *particles;
	emitter  *emitters;
	int partcap, emitcap;
	void init(int partcap0, int emitcap0)
	{
		partcap = partcap0;
		emitcap = emitcap0;
		particles = new particle[partcap];
		emitters = new emitter[emitcap];
		for (int i = 0; i < partcap; i++)
		{particles[i].init_state();}
		for (int i = 0; i < emitcap; i++)
		{emitters[i].init_state();}
	}
	int run_emitters()
	{
		int count =0;
		for (int i=0; i<emitcap; i++)
		{
			if (emitters[i].active == true)
			{
				emitters[i].run(particles, emitters);
				count++;
			}
		}
		return count;
	}
	int run_particles()
	{
		int count = 0;
		for (int i=0; i<partcap; i++)
		{
			if (particles[i].active == true)
			{
				particles[i].run();
				count++;
			}
		}
		return count;
	}
	int create_emitter(double x2, double y2, double velx2, double vely2,double life, sf::Color c, int radius, int pat, int numP2)
	{
		int a = inactive(emitters,emitcap);
		emitters[a].init(x2,y2,velx2,vely2,life,c,radius,pat,numP2,partcap);
		return a;
	}
	int draw_system(sf::RenderTarget &window)
	{
		int partcount = 0;
		for (int i = 0; i<partcap; i++)
		{
			if (particles[i].active == true)
			{partcount++;}
		}
		int count2 = 0;
		sf::VertexArray points(sf::Quads,partcount*4);
		for (int i=0; i<partcap; i++)
		{
			if (particles[i].active == true)
			{
				points[count2].color = particles[i].colour;
				points[count2].position.x = particles[i].x-2;
				points[count2].position.y = particles[i].y;
				count2++;

				points[count2].color = particles[i].colour;
				points[count2].position.x = particles[i].x;
				points[count2].position.y = particles[i].y-2;
				count2++;

				points[count2].color = particles[i].colour;
				points[count2].position.x = particles[i].x+2;
				points[count2].position.y = particles[i].y;
				count2++;

				points[count2].color = particles[i].colour;
				points[count2].position.x = particles[i].x;
				points[count2].position.y = particles[i].y+2;
				count2++;
			}
		}
		window.draw(points);
		return count2/4;
	}
	void draw_glow(sf::RenderTarget &window)
	{
		int partcount = 0;
		for (int i = 0; i<partcap; i++)
		{
			if (particles[i].active == true)
			{partcount++;}
		}
		int count2 = 0;
		sf::VertexArray points(sf::Quads,partcount*4);
		for (int i=0; i<partcap; i++)
		{
			if (particles[i].active == true)
			{
				points[count2].color = particles[i].colour;
				points[count2].position.x = particles[i].x-7;
				points[count2].position.y = particles[i].y;
				count2++;

				points[count2].color = particles[i].colour;
				points[count2].position.x = particles[i].x;
				points[count2].position.y = particles[i].y-7;
				count2++;

				points[count2].color = particles[i].colour;
				points[count2].position.x = particles[i].x+7;
				points[count2].position.y = particles[i].y;
				count2++;

				points[count2].color = particles[i].colour;
				points[count2].position.x = particles[i].x;
				points[count2].position.y = particles[i].y+7;
				count2++;
			}
		}
		window.draw(points);
	}
};

#endif