#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <gl\GLU.h>
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <math.h>
#include <sstream>
#include <fstream>

const double degtorad = 0.0174532925;
const double radtodeg = 57.2957795;
const double PI = 3.141592654;

const int width = 1280;
const int height = 720;

const float targetheight = -10.0f;
float k = 0.005;
float d = 0.008;
const int widthw  = 100;
const int heightw = 100;
const float xzscale = 1;
const float yscale  = 0.1;

int mousex = sf::Mouse::getPosition().x;
int mousey = sf::Mouse::getPosition().y;
bool mouselock = false;

int irandom(int v)
{
	return (int)( (v*rand()/RAND_MAX) +0.5);
}

double point_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (sqrt(a));
}

double sqr_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (a);
}

double point_direction(double x1, double y1, double x2, double y2)
{
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

int signof(float num)
{
	if(num>0){return  1;}
	if(num<0){return -1;}
	else     {return  0;}
}

void drawTexCube(float size)
{
	glBegin(GL_QUADS);

		glTexCoord2f(0, 0); glVertex3f(-size, -size, -size);
		glTexCoord2f(0, 1); glVertex3f(-size,  size, -size);
		glTexCoord2f(1, 1); glVertex3f( size,  size, -size);
		glTexCoord2f(1, 0); glVertex3f( size, -size, -size);

		glTexCoord2f(0, 0); glVertex3f(-size, -size, size);
		glTexCoord2f(0, 1); glVertex3f(-size,  size, size);
		glTexCoord2f(1, 1); glVertex3f( size,  size, size);
		glTexCoord2f(1, 0); glVertex3f( size, -size, size);

		glTexCoord2f(0, 0); glVertex3f(-size, -size, -size);
		glTexCoord2f(0, 1); glVertex3f(-size,  size, -size);
		glTexCoord2f(1, 1); glVertex3f(-size,  size,  size);
		glTexCoord2f(1, 0); glVertex3f(-size, -size,  size);

		glTexCoord2f(0, 0); glVertex3f(size, -size, -size);
		glTexCoord2f(0, 1); glVertex3f(size,  size, -size);
		glTexCoord2f(1, 1); glVertex3f(size,  size,  size);
		glTexCoord2f(1, 0); glVertex3f(size, -size,  size);

		glTexCoord2f(0, 1); glVertex3f(-size, -size,  size);
		glTexCoord2f(0, 0); glVertex3f(-size, -size, -size);
		glTexCoord2f(1, 0); glVertex3f( size, -size, -size);
		glTexCoord2f(1, 1); glVertex3f( size, -size,  size);

		glTexCoord2f(0, 1); glVertex3f(-size, size,  size);
		glTexCoord2f(0, 0); glVertex3f(-size, size, -size);
		glTexCoord2f(1, 0); glVertex3f( size, size, -size);
		glTexCoord2f(1, 1); glVertex3f( size, size,  size);

	glEnd();
}

int main()
{
	std::cout << "Initializing" << std::endl;
    // create the window
	sf::RenderWindow window(sf::VideoMode(width, height), "SFML OpenGL", sf::Style::Default, sf::ContextSettings(32));
    window.setVerticalSyncEnabled(true);
	window.setFramerateLimit(60);
	sf::Clock clock;
	sf::Clock clock2;
	float cangle = 90.0f;
	float cwid = (float)(800/600);
	float cx = 0.0f;
	float cy = -10.0f;
	float cz = 0.0f;
	float crx = 0.0f;
	float cry = 0.0f;
	float lastTime = 0;

	float sharkx = 10;
	float sharkdx = 1;
	float sharkDeg =0;

	sf::Texture bg;
	bg.loadFromFile("bg.png");

	sf::Vector2f water[widthw][heightw] = {sf::Vector2f(0,0)};
	// x = vely y = height;

	for(int i = 0; i<widthw; i++)
	{
		for (int j = 0; j< heightw; j++)
		{
			//water[i][j] = sf::Vector2f(0,20*sin(i*degtorad)+20*cos(j*radtodeg));
			water[i][j] = sf::Vector2f(0,0);
		}
	}

    // load resources, initialize the OpenGL states, ...
	
    // Enable Z-buffer read and write
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glClearDepth(1.f);

	// Setup a perspective projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(cangle, cwid, 1.f, 500.f);

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
 
    // run the main loop
    bool running = true;
    while (running)
    {
        // handle events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                // end the program
                running = false;
            }
            else if (event.type == sf::Event::Resized)
            {
                // adjust the viewport when the window is resized
                glViewport(0, 0, event.size.width, event.size.height);
            }
        }

        // Activate the window before using OpenGL commands.
        // This is useless here because we have only one window which is
        // always the active one, but don't forget it if you use multiple windows
        window.setActive();
        // clear the buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//for(int i = 1; i< 30; i++)
		//{
		//	for (int j = 1; j < 30; j++)
		//	{
		//					
		//		fluid[i][j].vely+=-k*(fluid[i][j].y-fluid[i+1][j-1].y)*0.025;
		//		fluid[i][j].vely+=-k*(fluid[i][j].y-fluid[i+1][j  ].y)*0.025;
		//		fluid[i][j].vely+=-k*(fluid[i][j].y-fluid[i+1][j+1].y)*0.025;
		//		fluid[i][j].vely+=-k*(fluid[i][j].y-fluid[i-1][j-1].y)*0.025;
		//		fluid[i][j].vely+=-k*(fluid[i][j].y-fluid[i-1][j  ].y)*0.025;
		//		fluid[i][j].vely+=-k*(fluid[i][j].y-fluid[i-1][j+1].y)*0.025;
		//		fluid[i][j].vely+=-k*(fluid[i][j].y-fluid[i  ][j-1].y)*0.025;
		//		fluid[i][j].vely+=-k*(fluid[i][j].y-fluid[i  ][j+1].y)*0.025;

		//	}
		//}

		//window.pushGLStates();
  //      window.draw(sf::Sprite(bg));
  //      window.popGLStates();
		float dx = 0;
		float dz = 0;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{	
			dx = -2*sin(crx*degtorad);
			dz =  2*cos(crx*degtorad);
			cz+= dz;
			cx+= dx;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			dx = -2*sin(crx*degtorad);
			dz =  2*cos(crx*degtorad);
			cz-= dz;
			cx-= dx;
			
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			dx = 2*cos((crx)*degtorad);
			dz = 2*cos((crx-90)*degtorad);
			cz-= dz;
			cx-= dx;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			dx = 2*cos((crx)*degtorad);
			dz = 2*cos((crx-90)*degtorad);
			cz+= dz;
			cx+= dx;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		{
			crx+=3.0f;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			crx-=3.0f;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			crx = 0.0f;
			cx  = 0.0f;
			cy  = -10.0f;;
			cz  = 0.0f;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
		{
			water[irandom(100)][irandom(100)].y =  -50+irandom(100);
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))
		{
			int x, y;
			x = 10+irandom(79);
			y = 10+irandom(79);
			for(int i = -5; i <=5;i++)
			{
				for( int j = -5; j<=5; j++)
				{
					water[x+i][y+j].y = -100+(4*i*i-50)+(4*j*j-50);
				}
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::H))
		{
			int x, y;
			x = 40;
			y = 40;
			for(int i = -5; i <=5;i++)
			{
				for( int j = -5; j<=5; j++)
				{
					water[x+i][y+j].y = -150+(4*i*i-50)+(4*j*j-50);
				}
			}
			x = 40;
			y = 60;
			for(int i = -5; i <=5;i++)
			{
				for( int j = -5; j<=5; j++)
				{
					water[x+i][y+j].y = -150+(4*i*i-50)+(4*j*j-50);
				}
			}
		}

		int x, y;
		x = 5+irandom(90);
		y = 5+irandom(90);
		for(int i = -5; i <=5;i++)
		{
			for( int j = -5; j<=5; j++)
			{
				//water[x+i][y+j].y += signof((float)(water[x][y].y))*(0.2*i*i-5)+(0.2*j*j-5);
				water[x+i][y+j].x += (0.032*i*i-0.8)+(0.032*j*j-0.8);
			}
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::X)){d+=0.01;}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z)){d-=0.01;}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::V)){k+=0.001;}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::C)){k-=0.001;}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Y))
		{
			if(mouselock == true )
			{
				window.setMouseCursorVisible(true);
				mouselock=false;
			}
			else if (mouselock == false)
			{
				window.setMouseCursorVisible(false);
				mouselock=true ;
			}
		}
		if (mouselock == true)
		{
			crx -= (400-sf::Mouse::getPosition(window).x)*0.2;
			cry -= (300-sf::Mouse::getPosition(window).y)*0.2;
			sf::Mouse::setPosition(sf::Vector2i(400,300),window);
		}


		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(cangle, cwid, 1.f, 1000.f);
		glRotatef(cry,1.0f,0.0f,0.0f);
		glRotatef(crx,0.0f,1.0f,0.0f);
		glTranslatef(cx, cy, cz);
		
		//sharkx+=sharkdx;
		//if (sharkx>95 || sharkx <5) {sharkdx*=-1;}
		//water[53][(int)sharkx-(int)dx*3].y +=3.0f;
		//water[52][(int)sharkx-(int)dx*2].y +=5.0f;
		//water[51][(int)sharkx-(int)dx].y +=7.0f;
		//water[50][(int)sharkx].y +=10.0f;
		//water[49][(int)sharkx-(int)dx].y +=7.0f;
		//water[48][(int)sharkx-(int)dx*2].y +=5.0f;
		//water[47][(int)sharkx-(int)dx*3].y +=3.0f;

		//sharkDeg+= 0.5;
		//if (sharkDeg==360){sharkDeg=0;}
		//sf::Vector2f temp = sf::Vector2f(40*cos(sharkDeg*degtorad)+50,40*sin(sharkDeg*degtorad)+50);
		//water[(int)temp.x  ][(int)temp.y-1].x +=5.0f;
		//water[(int)temp.x+1][(int)temp.y-1].x +=5.0f;
		//water[(int)temp.x+1][(int)temp.y  ].x +=5.0f;
		//water[(int)temp.x+1][(int)temp.y-1].x +=5.0f;
		//water[(int)temp.x  ][(int)temp.y  ].x +=10.0f;
		//water[(int)temp.x-1][(int)temp.y+1].x +=5.0f;
		//water[(int)temp.x-1][(int)temp.y  ].x +=5.0f;
		//water[(int)temp.x-1][(int)temp.y-1].x +=5.0f;
		//water[(int)temp.x  ][(int)temp.y+1].x +=5.0f;

		//float x =  sf::Mouse::getPosition(window).x * 200.f / window.getSize().x - 100.f;
        //float y = -sf::Mouse::getPosition(window).y * 200.f / window.getSize().y + 100.f;
		// Apply some transformations
        glMatrixMode(GL_MODELVIEW);
		for(int i = 1; i<widthw-1; i++)
		{
			for (int j = 1; j< heightw-1; j++)
			{
				float fract = 1.0;
				water[i][j].x+= -k*(water[i][j].y-water[i-1][j  ].y)*fract;
				water[i][j].x+= -k*(water[i][j].y-water[i+1][j  ].y)*fract;
				water[i][j].x+= -k*(water[i][j].y-water[i  ][j+1].y)*fract;
				water[i][j].x+= -k*(water[i][j].y-water[i  ][j-1].y)*fract;
				water[i][j].x+= -k*(water[i][j].y-water[i+1][j+1].y)*fract;
				water[i][j].x+= -k*(water[i][j].y-water[i+1][j-1].y)*fract;
				water[i][j].x+= -k*(water[i][j].y-water[i-1][j-1].y)*fract;
				water[i][j].x+= -k*(water[i][j].y-water[i-1][j+1].y)*fract;
			}
		}

		float dampen;
		for(int i = 0; i<widthw; i++)
		{
			for (int j = 0; j< heightw; j++)
			{
				water[i][j].y+=water[i][j].x;
				dampen = -d*water[i][j].x;
				if (abs(dampen) > abs(water[i][j].x) ) { dampen = -water[i][j].x;}
				water[i][j].x+= -k*(water[i][j].y-targetheight) + dampen;

			}
		}

		glBegin(GL_QUADS);
			glColor4f(0.0f,0.0f,1.0f,1.0f);
			glVertex3f(0.0f,-5.0f, 0.0f);
			glVertex3f(100.0f,-5.0f, 0.0f);
			glVertex3f(100.0f,-5.0f, 100.0f);
			glVertex3f(0.0f,-5.0f, 100.0f);
		glEnd();
		float h = 0;
		for(int i = 0; i< widthw-1; i++)
		{

			glBegin(GL_TRIANGLE_STRIP);
			for (int j = 0; j < heightw; j++)
			{

				//Blue Ver 2
				h = abs(water[i][j].y - targetheight);
				float a = 0.3;
				if     (h>25){glColor4f( 0.2-0.000448*(h-25)*(h-25) ,
											0.7*0.5-(0.000280784*(h-25)*(h-25)*0.7),
											1.0-(0.00112*(h-25)*(h-25))*0.7,
											a);}
				if     (h>50){glColor4f( 0.0f,
											0.0f,
											0.7f,
											a);}
				//if     (i%10==0 || j%10==0){glColor4f(0.0f,0.0f,0.0f,0.0f);}
				//if	   (i == 2) { glColor4f(0,0,0,1);}
				else         {glColor4f( 0.239215686-(0.000062745*h*h),
											0.7f-(0.0008*h*h)*0.7,
											0.847058823+(0.00244705*h*h)*0.7,
											a);}
				glVertex3f(i*xzscale,water[i][j].y*yscale,j*xzscale);
				h = abs(water[i+1][j].y - targetheight);
				if     (h>25){glColor4f( 0.2-0.000448*(h-25)*(h-25) ,
											0.7*0.5-(0.000280784*(h-25)*(h-25)*0.7),
											1.0-(0.00112*(h-25)*(h-25))*0.7,
											a);}
				if     (h>50){glColor4f( 0.0f,
											0.0f,
											0.7f,
											a);}
				//if     (i%10==0 || j%10==0){glColor4f(0.0f,0.0f,0.0f,0.0f);}
				else         {glColor4f( 0.239215686-(0.000062745*h*h),
											0.7f-(0.0008*h*h)*0.7,
											0.847058823+(0.00244705*h*h)*0.7,
											a);}
				glVertex3f(i*xzscale+xzscale,water[i+1][j].y*yscale,j*xzscale);
			}
			glEnd();
		}


        // end the current frame (internally swaps the front and back buffers)
		window.display();
		float currentTime = clock.restart().asSeconds();
		float fps = 1.f / currentTime;
		lastTime = currentTime;
		std::stringstream out;
		out << "FPS: " << (int)fps << " Camera Angle: " << cangle << " Camera Width: " << cwid << " Position(" << cx << "," << cy << "," << cz << ")"   << " K: " << k << " d: " << d;
		window.setTitle(out.str());
    }

    // release resources...

    return 0;
}

