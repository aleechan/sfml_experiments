#pragma once
#include "ParticleSystem\ParticleSystem.h"
#include "ParticleSystem\Emitter.h"
#include "ParticleSystem\Particle.h"
#include "ParticleSystem\BezierCurve.h"
#include "ParticleSystem\my_math.h"
#include "derivedEmitters.h"