#pragma once

#include "derivedParticles.h"

class staticEmitter : public Emitter
{
public:
	staticEmitter(sf::Vector2f pos, sf::Vector2f vel, sf::Vector2f acc, float life, sf::Color col) : Emitter(pos,vel,acc,life,col){}
	virtual void movement(float *timeElapsed)
	{
		vel += accel*(*timeElapsed);
		pos += vel*(*timeElapsed);
	}
	void deactivate()
	{
		active = false;
	}
	void setPosition(sf::Vector2f pos)
	{
		this->pos = pos;
	}
};

class staticSprayEmitter : public staticEmitter
{
	float xMin, yMin, dx, dy, partVel;
	ParticleSystem *sys;
public:

	staticSprayEmitter(sf::Vector2f pos, sf::Vector2f vel, sf::Vector2f acc, float life, float angle, float spread, float partVel, sf::Color col, ParticleSystem *sys,int partCount):staticEmitter(pos,vel,acc,life,col)
	{
		this->partCount = partCount;
		this->sys = sys;
		this->partVel = partVel;
		setAngle(angle,spread);
	}
	void setAngle(float angle, float spread)
	{
		xMin = cos(angle-spread);
		yMin = sin(angle-spread);
		dx = cos(angle+spread) - xMin;
		dy = sin(angle+spread) - yMin;
	}
	void setPartCount(int n)
	{
		this->partCount = n;
	}
	virtual void pattern(float *timeElapsed)
	{
		float a;
		for(int i = 0; i < partCount; i++)
		{
			a = frandom(1.f);
			//sys->addParticle(new BCQParticle(pos,partVel*sf::Vector2f(xMin+dx*a,yMin+dy*a),sf::Vector2f(0,0),0.1+frandom(0.5),sf::Color::Yellow,sf::Color(255,155,155),sf::Color::Red));
			sys->addParticle(new BCQParticle(pos,partVel*sf::Vector2f(xMin+dx*a,yMin+dy*a),sf::Vector2f(0,0),0.1+frandom(1.f),sf::Color::Cyan,sf::Color::Magenta, sf::Color::Blue));
			//sys->addParticle(new Particle(pos,partVel*sf::Vector2f(xMin+dx*a,yMin+dy*a),sf::Vector2f(0,0),0.1+frandom(0.5),col));
		}
	}
};

class sprayEmitter : public Emitter
{
	sf::Vector2f pos0;//previous position
	sf::Vector2f pv;//temporary varible for particle velocity
	ParticleSystem *sys;
public:
	sprayEmitter(sf::Vector2f pos, sf::Vector2f vel, sf::Vector2f acc, float life, sf::Color col, ParticleSystem *sys,int partCount):Emitter(pos,vel,acc,life,col)
	{
		this->partCount = partCount;
		this->sys = sys;
	}
	virtual void movement(float *timeElapsed)
	{
		pos0 = pos;
		life -= (*timeElapsed);
		vel += accel*(*timeElapsed);
		pos += vel*(*timeElapsed);

		if( 0 > life)
		{
			active = false;
		}
	}
	virtual void pattern(float *timeElapsed)
	{
		for(int i = 0; i < partCount; i++)
		{
			pv = pos - pos0;
			pv += sf::Vector2f(-pv.y,pv.x)*(0.5f-frandom(1))*5.f;
			sys->addParticle(new Particle(pos,pv,sf::Vector2f(0,0),(0.1f+frandom(0.9f)),col));
		}
	}
};