#pragma once

#include "ParticleEffects.h";

class BCQParticle : public Particle
{
	float lifeSpan;
	BezierCurveQuad *r,*g,*b;
public:
	BCQParticle(sf::Vector2f position, sf::Vector2f velocity, sf::Vector2f acceleration, float life, sf::Color c0, sf::Color c1, sf::Color c2) : Particle(position,velocity,acceleration,life,sf::Color::Black)
	{
		lifeSpan = life;
		r = new BezierCurveQuad(sf::Vector2f(0,c0.r),sf::Vector2f(0,c1.r),sf::Vector2f(0,c2.r));
		g = new BezierCurveQuad(sf::Vector2f(0,c0.g),sf::Vector2f(0,c1.g),sf::Vector2f(0,c2.g));
		b = new BezierCurveQuad(sf::Vector2f(0,c0.b),sf::Vector2f(0,c1.b),sf::Vector2f(0,c2.b));
		size = 6;
	}
	void run (float timeElapsed)
	{
		if(active)
		{
			float t = 1-life/lifeSpan;
			vel += accel*timeElapsed;
			pos += vel*timeElapsed;
			life -= timeElapsed;
			colour = sf::Color(r->getPoint(t).y,g->getPoint(t).y,b->getPoint(t).y);
			if(life < 0)
			{
				active = false;
			}
		}
	}
};