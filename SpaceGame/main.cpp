#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include "ParticleEffects.h"
#include "shaders.h"

sf::Vector2f mPos;



class playerShip
{
	sf::Texture tex;
	sf::Vector2f pos, vel, dir, ePos[2], gun;
	float angle, thrust;
	staticSprayEmitter* engineEmitters[2];
	ParticleSystem *sys;
public:
	sf::Sprite self;
	playerShip(sf::Vector2f pos, float dir, ParticleSystem *sys)
	{
		this->pos = pos;
		angle = dir;
		this->sys = sys;
		tex.loadFromFile("F5S4.png");
		self.setTexture(tex);
		self.setOrigin(68,40);
		self.setPosition(pos);
		ePos[0] = sf::Vector2f(-65,-9);
		ePos[1] = sf::Vector2f(-66, 9);
		gun = sf::Vector2f(60,0);
		this->dir = sf::Vector2f(cos(dir),sin(dir));
		engineEmitters[0] = new staticSprayEmitter(pos,sf::Vector2f(0,0),sf::Vector2f(0,0),100,dir,PI/12.f,150,sf::Color::Red,sys,5);
		engineEmitters[1] = new staticSprayEmitter(pos,sf::Vector2f(0,0),sf::Vector2f(0,0),100,dir,PI/12.f,150,sf::Color::Red,sys,5);
		sys->addEmitter(engineEmitters[0]);
		sys->addEmitter(engineEmitters[1]);
		thrust = 0.f;
	}
	void run(float timeElapsed)
	{
		vel += thrust*dir;
		pos += vel*timeElapsed;
		self.setPosition(pos);
		engineEmitters[0]->setPosition(pos + ePos[0]);
		engineEmitters[1]->setPosition(pos + ePos[1]);
		engineEmitters[0]->setAngle(angle+PI,PI/12.f);
		engineEmitters[1]->setAngle(angle+PI,PI/12.f);
	}
	void fire()
	{
		sys->addEmitter(new sprayEmitter(pos+gun,dir*300.f,sf::Vector2f(0,0),5,sf::Color::Cyan,sys,1));
	}
	void push()
	{
		thrust = 0.5f;
		engineEmitters[0]->setPartCount(10);
		engineEmitters[1]->setPartCount(10);
	}
	void stop()
	{
		thrust = 0.f;
		engineEmitters[0]->setPartCount(0);
		engineEmitters[1]->setPartCount(0);
	}
	void rotate(float theta)
	{
		float cosD = cos(theta),sinD = sin(theta);
		ePos[0] = sf::Vector2f(ePos[0].x*cosD - ePos[0].y*sinD, ePos[0].x*sinD + ePos[0].y*cosD);
		ePos[1] = sf::Vector2f(ePos[1].x*cosD - ePos[1].y*sinD, ePos[1].x*sinD + ePos[1].y*cosD);
		dir = sf::Vector2f(dir.x*cosD - dir.y*sinD, dir.x*sinD + dir.y*cosD);
		gun = sf::Vector2f(gun.x*cosD - gun.y*sinD, gun.x*sinD + gun.y*cosD);
		angle += theta;
		self.setRotation(angle*radtodeg);
		
	}
	void setPosition(sf::Vector2f p)
	{
		pos = p;
	}
	~playerShip()
	{
		engineEmitters[0]->deactivate();
		engineEmitters[1]->deactivate();
	}
};

class playerShipV2
{
	struct engine
	{
		sf::Vector2f pos, dir, force;
		staticSprayEmitter* emitter;
	};
	struct gun
	{
		sf::Vector2f pos, dir, *origin, *target;
		float delay, counter;
		bool tracking;
		void set(sf::Vector2f pos, sf::Vector2f dir, float delay)
		{
			this->delay = delay;
			counter = delay;
			this->pos = pos;
			this->dir = dir;
			tracking = false;
		}
		void run(float timeElapsed)
		{
			if(counter >0 )
			{
				counter -= timeElapsed;
			}
			if(tracking)
			{
				float angle = atan2(target->y - (pos.y+origin->y), target->x - (pos.x+origin->x));
				dir = sf::Vector2f(cos(angle),sin(angle));
			}
		}
		void rotate(float theta)
		{
			float cosD = cos(theta),sinD = sin(theta);
			dir = sf::Vector2f(dir.x*cosD - dir.y*sinD, dir.x*sinD + dir.y*cosD);
		}
		void fire(ParticleSystem *sys)
		{
			if(counter <= 0)
			{
				sys->addEmitter(new sprayEmitter(*origin + pos,300.f*dir,sf::Vector2f(0,0),2.0f,sf::Color::Green,sys,1));
				counter = delay;
			}
			
		}
	};
	sf::Texture tex;
	std::vector<sf::Vector2f*> fixedVectors;
	sf::Vector2f center, vel, dir;
	float angle, thrust, av;//av = angular velocity;
	ParticleSystem *sys;
	gun guns[3];
	engine engines[2];
public:
	sf::Sprite self;
	sf::CircleShape dots[6];
	playerShipV2(sf::Vector2f pos, float dir, ParticleSystem *sys)
	{
		this->center = pos;
		angle = dir;
		this->dir = sf::Vector2f(1,0);
		this->sys = sys;
		this->av = 0;
		
		tex.loadFromFile("ELxFZ.png");
		self.setTexture(tex);
		self.setOrigin(sf::Vector2f(tex.getSize().x/2,tex.getSize().y/2));
		self.setPosition(pos);

		guns[0].set(sf::Vector2f(0,-25),this->dir,1.0);
		guns[1].set(sf::Vector2f(0, 25),this->dir,1.0);
		guns[2].set(sf::Vector2f( 65,  0),this->dir,0.2);

		guns[0].origin = &center;
		guns[1].origin = &center;
		guns[2].origin = &center;

		guns[2].tracking = true;
		guns[2].target = &mPos;

		engines[0].pos = sf::Vector2f(-60, 9);
		engines[1].pos = sf::Vector2f(-60,-9);
		engines[0].dir = sf::Vector2f(-1,0);
		engines[1].dir = sf::Vector2f(-1,0);

		for(int i = 0; i < 6; i++)
		{
			dots[i].setFillColor(sf::Color::Cyan);
			dots[i].setRadius(5);
			dots[i].setOrigin(5,5);
		}

		fixedVectors.push_back(&this->dir);
		for(int i = 0; i < 3; i++)
		{
			fixedVectors.push_back(&guns[i].pos);
			fixedVectors.push_back(&guns[i].dir);
		}
		for(int i = 0; i < 2; i++)
		{
			fixedVectors.push_back(&engines[i].dir);
			fixedVectors.push_back(&engines[i].pos);
			engines[i].emitter = new staticSprayEmitter(engines[i].pos,sf::Vector2f(0,0),sf::Vector2f(0,0),100,PI,PI/12.f,150,sf::Color::Blue,sys,1);
			sys->addEmitter(engines[i].emitter);
		}
		decelerate();
		rotate(angle);
	}
	void run(float timeElapsed)
	{
		vel += thrust*dir*timeElapsed;
		vel -= 0.1f*vel*timeElapsed;
		center += vel*timeElapsed;
		self.setPosition(center);
		rotate(av*timeElapsed);
		av *= 0.99*(1-timeElapsed);
		for(int i = 0; i < 3; i++)
		{
			guns[i].run(timeElapsed);
			dots[i].setPosition(center+guns[i].pos);
		}
		//guns[2].fire(sys);
		for(int i = 0; i < 2; i++)
		{
			engines[i].emitter->setPosition(center + engines[i].pos);
			dots[i+2].setPosition(center + engines[i].pos);
			engines[i].emitter->setAngle(angle+PI,PI/12);
		}
		dots[5].setPosition(center);
	}
	void rotate(float theta)
	{
		sf::Transform transform;
		transform.rotate(theta*radtodeg);
		//double cosD = cos(theta),sinD = sin(theta);
		for(int i = 0; i < fixedVectors.size(); i++)
		{
			*fixedVectors[i] = transform.transformPoint(*fixedVectors[i]);
			//fixedVectors[i]->x = fixedVectors[i]->x*cosD - fixedVectors[i]->y*sinD;
			//fixedVectors[i]->y = fixedVectors[i]->x*sinD + fixedVectors[i]->y*cosD;
		}
		angle += theta;
		self.setRotation(angle*radtodeg);
	}
	void spin(float dTheta)
	{
		av+=dTheta;
	}
	void fire()
	{
		guns[0].fire(sys);
		guns[1].fire(sys);
		guns[2].fire(sys);
	}
	void accelerate()
	{
		thrust = 25.0f;
		for(int i = 0; i < 2; i++)
		{
			engines[i].emitter->setPartCount(5);
		}
	}
	void strafeLeft()
	{
		vel -= 1.5f*sf::Vector2f(-dir.y,dir.x);
	}
	void strafeRight()
	{
		vel += 1.5f*sf::Vector2f(-dir.y,dir.x);
	}
	void decelerate()
	{
		thrust = 0.f;
		for(int i = 0; i < 2; i++)
		{
			engines[i].emitter->setPartCount(0);
		}
	}
	void stablize()
	{
		vel *= 0.95f;
	}
	void setPosition(sf::Vector2f p)
	{
		center = p;
	}
};

int main()
{
    sf::RenderWindow window(sf::VideoMode(1280, 680), "");
	window.setFramerateLimit(60);
	//window.setVerticalSyncEnabled(true);
	ParticleSystem partSys;

	//playerShip ship(sf::Vector2f(1280/2,680/2),0.f,&partSys);
	playerShipV2 ship2(sf::Vector2f(1280/2,680/2),0.f,&partSys);

	gaus_blur blur;
	blur.init(1280,680);

	sf::Clock timer;
	sf::Time frameTime;
	float timeElapsed;
    while (window.isOpen())
    {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
                window.close();
        }
		frameTime = timer.restart();
		timeElapsed = frameTime.asSeconds();

		mPos = sf::Vector2f(sf::Mouse::getPosition(window).x,sf::Mouse::getPosition(window).y);

		if(sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{
			ship2.accelerate();
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			ship2.decelerate();
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			ship2.strafeLeft();
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			ship2.strafeRight();
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			ship2.spin(-0.1);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		{
			ship2.spin(0.1);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			ship2.setPosition(sf::Vector2f(1280/2,680/2));
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::X))
		{
			ship2.stablize();
		}
		partSys.run(&timeElapsed);
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			ship2.fire();
		}
		//ship2.rotate(0.03);
		
		//ship.run(timeElapsed);
		ship2.run(timeElapsed);
		window.clear(sf::Color(25,25,25));
		//partSys.render(window);
		blur.clear();
		partSys.render(blur.original);
		//window.draw(ship.self);
		window.draw(blur.render());
		window.draw(ship2.self);
		//for(int i = 0; i <6; i++)
		//{
		//	window.draw(ship2.dots[i]);
		//}
        window.display();
		std::stringstream out;
		out << "Space Game, FPS: " << (int)(1.f/frameTime.asSeconds());
		partSys.getStats(out);
		window.setTitle(out.str());
    }

    return 0;
}