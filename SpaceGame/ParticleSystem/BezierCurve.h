#ifndef BEZIERCURVE_H
#define BEZIERCURVE_H

#include <SFML\System.hpp>
#include <iostream>
#include <list>

class pascalstriangle
{
private:
	std::vector<std::vector<int>*> rows;
public:
	pascalstriangle(int initalRows)
	{
		if(initalRows < 2)
		{
			initalRows = 2;
		}
		rows.push_back(new std::vector<int>);
		rows.at(0)->push_back(1);
		rows.push_back(new std::vector<int>);
		rows.at(1)->push_back(1);
		rows.at(1)->push_back(1);
		for(int i = 2; i < initalRows; i++)
		{
			rows.push_back(new std::vector<int>);
			rows.at(i)->push_back(1);
			for(int j = 1; j < i; j++)
			{
				rows.at(i)->push_back(rows.at(i-1)->at(j) + rows.at(i-1)->at(j-1)); 
			}
			rows.at(i)->push_back(1);
		}
	}
	int getNumber(int row, int column)
	{
		if(row > 0 && row < rows.size())
		{
			if(column >= 0 && column <= row)
			{
				return rows.at(row)->at(column);
			}
		}
		return 1;
	}
	void print()
	{
		for(int i = 0; i < rows.size(); i++)
		{
			for(int j = 0; j < i+1; j++)
			{
				std::cout << rows.at(i)->at(j) << " ";
			}
			std::cout << std::endl;
		}
	}
};

class BezierCurveQuad
{
private:
	sf::Vector2f p[3];
public:
	BezierCurveQuad(sf::Vector2f p0, sf::Vector2f p1, sf::Vector2f p2)
	{
		p[0] = p0;
		p[1] = p1;
		p[2] = p2;
	}
	void setPoints(sf::Vector2f p0, sf::Vector2f p1, sf::Vector2f p2)
	{
		p[0] = p0;
		p[1] = p1;
		p[2] = p2;
	}
	sf::Vector2f getPoint(float t)
	{
		sf::Vector2f pos;
		pos.x = (1.f-t)*(1.f-t)*p[0].x + 2*t*(1.f-t)*p[1].x + t*t*p[2].x;
		pos.y = (1.f-t)*(1.f-t)*p[0].y + 2*t*(1.f-t)*p[1].y + t*t*p[2].y;
		return pos;
	}
};

class BezierCurve
{
private:
	std::list<sf::Vector2f> points;
	std::list<sf::Vector2f>::iterator current;
	pascalstriangle triangle;
public:
	BezierCurve():triangle(30){};
	void clearPoints()
	{
		points.clear();
	}
	void addFirst(sf::Vector2f point)
	{
		points.push_front(point);
	}
	void addLast(sf::Vector2f point)
	{
		points.push_back(point);
	}

	sf::Vector2f getPoint(float t)
	{
		current = points.begin();
		sf::Vector2f sum;
		float bn;
		for(int i = 0; i < points.size(); i++)
		{
			bn = triangle.getNumber(points.size()-1,i)*pow((1-t),points.size()-1-i)*pow(t,i);
			sum.x += bn*current->x;
			sum.y += bn*current->y;
			current++;
		}
		return sum;
	}
	~BezierCurve()
	{
		clearPoints();
	}
};

#endif