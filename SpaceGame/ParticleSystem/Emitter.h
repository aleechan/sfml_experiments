#pragma once
#include "Particle.h"

class Emitter
{
protected:
	int partCount;
	float life;
	sf::Vector2f vel, accel;
	sf::Color col;
	virtual void movement(float *timeElapsed)
	{
		life -= (*timeElapsed);
		vel += accel*(*timeElapsed);
		pos += vel*(*timeElapsed);

		if( 0 > life)
		{
			active = false;
		}
	}
	virtual void pattern (float *timedElapsed) = 0;
public:
	bool active;
	sf::Vector2f pos;
	Emitter(sf::Vector2f position, sf::Vector2f velocity, sf::Vector2f acceleration, float life, sf::Color colour)
	{
		pos = position;
		vel = velocity;
		accel = acceleration;
		this->life = life;
		this->col = colour;
		active = true;
	}
	void run(float *timeElapsed)
	{
		movement(timeElapsed);
		pattern(timeElapsed);
	}

	virtual ~Emitter(void){};
};

