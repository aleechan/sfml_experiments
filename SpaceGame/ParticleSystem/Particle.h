#pragma once
#include <SFML/Graphics.hpp>
#include <SFML\System.hpp>

class Particle
{
protected:
	float life;
	float size;
	sf::Color colour;
	sf::Vector2f pos,vel,accel;
public:
	bool active;
	Particle(sf::Vector2f position, sf::Vector2f velocity, sf::Vector2f acceleration, float life, sf::Color colour)
	{
		this->colour = colour;
		this->life = life;
		size = 3;
		pos = position;
		vel = velocity;
		accel = acceleration;
		active = true;
	}
	~Particle(){};
	sf::Vector2f getPos()
	{
		return pos;
	}
	void setSize(float size)
	{
		this->size = size;
	}
	float getSize()
	{
		return size;
	}
	sf::Color getColor()
	{
		return colour;
	}
	virtual void run(float timeElapsed)
	{
		if(active)
		{
			vel += accel*timeElapsed;
			pos += vel*timeElapsed;
			life -= timeElapsed;
			if(life < 0)
			{
				active = false;
			}
		}
	}
};

