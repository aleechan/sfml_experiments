#pragma once

#include "Particle.h"
#include "Emitter.h"
#include "BezierCurve.h"
#include <SFML\Graphics.hpp>
#include <vector>
#include <queue>
#include <string>
#include <sstream>
#include <iostream>
#include <thread>
#include <condition_variable>
#include <mutex>

class ParticleSystem
{
	sf::Clock timer;
	sf::VertexArray verts;
	std::vector<Particle*> particles;
	std::vector<Emitter*> emitters;
	std::queue<int> emptyParts, deadParts;
	std::queue<int> emptyEmits, deadEmits;
	std::queue<Particle*> newParts;
	std::queue<Emitter*> newEmits;
	int activeParts,activeEmits;
	float *timeElapsed;
	bool active, calculating, drawing, rendered;

	int getInactiveParticle()
	{
		int index = -1;
		if(!emptyParts.empty())
		{
			index = emptyParts.front();
			emptyParts.pop();
		}
		return index;
	}	
	int getInactiveEmitter()
	{
		int index = -1;
		if(!emptyEmits.empty())
		{
			index = emptyEmits.front();
			emptyEmits.pop();
		}
		if(index < -1)
		{
			index = -1;
		}
		return index;
	}
	
	std::thread thread;
	//sf::Time calcTimes[EThreadCount + 1];

public:
	ParticleSystem(void)
	{
		active = true;
		thread = std::thread(&ParticleSystem::threadFunction,this);
		calculating = false;
		drawing = false;
		rendered = false;
		verts.setPrimitiveType(sf::Triangles);
	}
	~ParticleSystem(void)
	{
		active = false;
		thread.join();
		for(int i = 0; i < particles.size(); i++)
		{
			delete particles[i];
		}
		for(int i = 0; i < emitters.size(); i++)
		{
			delete emitters[i];
		}
		particles.clear();
		emitters.clear();
	}
	void getStats(std::stringstream &out)
	{
		out << "Particles: " << activeParts << "/" << particles.size();
		out << "Emitters: " << activeEmits << "/" << emitters.size();
	}
	Emitter* addEmitter(Emitter *item)
	{
		if(calculating)
		{
			newEmits.push(item);
		}
		else
		{
			int index = getInactiveEmitter();
			if(index != -1)
			{
				emitters[index] = item;
			}
			else
			{
				emitters.push_back(item);
			}
		}
		return item;
	}
	void addParticle(Particle *item)
	{
		int index = getInactiveParticle();
		if(index != -1)
		{
			particles[index] = item;
		}
		else
		{
			particles.push_back(item);
		}
	}
	void run(float *timeElapsed)
	{
		calculating = true;
		this->timeElapsed = timeElapsed;
		//sf::Clock timer;
		activeParts = 0;
		activeEmits = 0;
	}	
	void threadFunction()
	{
		int index;
		while(active)
		{
			if(calculating)
			{
				for(int i = 0; i < emitters.size(); i++)
				{
					if(emitters[i] != nullptr)
					{
						if(emitters[i]->active)
						{
							emitters[i]->run(timeElapsed);
							activeEmits++;
						}
						else
						{
							delete emitters[i];
							emptyEmits.push(i);
							emitters[i] = nullptr;
						}
					}
				}
				drawing = true;
				verts.clear();
				verts.resize(particles.size()*3);
				int counter = 0;
				for(int i = 0; i < particles.size(); i++)
				{
					if(particles[i] != nullptr)
					{
						if(particles[i]->active)
						{
							particles[i]->run((*this->timeElapsed));
							verts[i*3  ].color = particles[i]->getColor();
							verts[i*3+1].color = particles[i]->getColor();
							verts[i*3+2].color = particles[i]->getColor();

							verts[i*3  ].position = particles[i]->getPos() + particles[i]->getSize()*sf::Vector2f( 0,-1);
							verts[i*3+1].position = particles[i]->getPos() + particles[i]->getSize()*sf::Vector2f(-0.707, 0.707);
							verts[i*3+2].position = particles[i]->getPos() + particles[i]->getSize()*sf::Vector2f( 0.707, 0.707);
							activeParts++;
						}
						else
						{
							delete particles[i];
							emptyParts.push(i);
							particles[i] = nullptr; 
						}
					}
				}
				rendered = true;
				drawing = false;

				calculating = false;
			}
			while(!newEmits.empty())
			{
				index = getInactiveEmitter();
				if(index != -1)
				{
					emitters[index] = newEmits.front();
				}
				else
				{
					emitters.push_back(newEmits.front());
				}
				newEmits.pop();
			}
		}
	}
	void render(sf::RenderTexture &tex)
	{
		while(drawing || !rendered)
		{
			std::this_thread::sleep_for(std::chrono::microseconds(100));
		}
		rendered = false;
		tex.draw(verts);
	}	

	void render(sf::RenderWindow &tex)
	{
		while(drawing || !rendered)
		{
			std::this_thread::sleep_for(std::chrono::microseconds(100));
		}
		tex.draw(verts);
		rendered = false;
	}
};

