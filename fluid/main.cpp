#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML\System\Thread.hpp>
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <math.h>
#include <zmouse.h>
#include <sstream>
#include <time.h>

const double degtorad = 0.0174532925;
const double radtodeg = 57.2957795;
const double PI = 3.141592654;

const int width = 800;
const int height = 600;

const float k  = 0.025f;
const float targetheight = 300.0f;

int mousex = sf::Mouse::getPosition().x;
int mousey = sf::Mouse::getPosition().y;

int irandom(int v)
{
	return (int)( (v*rand()/RAND_MAX) +0.5);
}

double point_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (sqrt(a));
}

double sqr_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (a);
}

double point_direction(double x1, double y1, double x2, double y2)
{
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

class water
{
public:
	float y,vely;
	void init (float x0, float y0)
	{
		y = y0;
		vely = 0;
	}

	void update()
	{
		float x = y-targetheight;
		//std::cout << x << std::endl;
		y += vely;
		vely += -k*x-0.055*vely;
	}
};

int main()
{
    sf::RenderWindow App(sf::VideoMode (width, height, 32), "Fluid");
	App.setFramerateLimit(60);
	App.setVerticalSyncEnabled(true);

	int clickx;
	bool lclick = sf::Mouse::isButtonPressed(sf::Mouse::Left);
	bool lclickpre;

	std::vector<water> fluid;
	water temp;

	for(int i = 0; i < width; i++)
	{
		temp.init(i,targetheight+50*cos(i*degtorad));
		fluid.push_back(temp);
	}




	//srand ( time(NULL) );

	sf::Clock clock;
    float lastTime = 0;

    // Start game loop
    while (App.isOpen())
     {
         // Process events
         sf::Event event;
         while (App.pollEvent(event))
         {
             // Close window : exit
             if (event.type == sf::Event::Closed)
             App.close();
        }

		mousex = sf::Mouse::getPosition(App).x;
		mousey = sf::Mouse::getPosition(App).y;
		lclickpre =lclick;
		lclick = sf::Mouse::isButtonPressed(sf::Mouse::Left);

		if (lclickpre == false && lclick == true)
		{
			clickx = mousex;
		}

		if (lclickpre == true && lclick == false)
		{
			fluid[clickx].y = height - mousey;
		}

		App.clear();

		for(int i = 4; i<fluid.size()-4; i++)
		{
			for (int j = -4; j < 4; j++)
			{
				fluid[i].vely+= -k*(fluid[i].y-fluid[i+j].y)*0.125;
			}

		}

		sf::VertexArray temp2(sf::Lines,fluid.size()*2+2);
		for (int i = 0; i<fluid.size(); i++)
		{
			fluid[i].update();
			temp2[i*2].position = sf::Vector2f(i,height);
			temp2[i*2+1].position = sf::Vector2f(i,height-fluid[i].y);
			temp2[i*2+1].color = sf::Color::Blue;
		}
		App.draw(temp2);

		float currentTime = clock.restart().asSeconds();
		float fps = 1.f / currentTime;
		lastTime = currentTime;
		std::stringstream out;
		out << "FPS: " << (int)fps;
		App.setTitle(out.str());

		App.display();
		

    }

    return EXIT_SUCCESS;
}
