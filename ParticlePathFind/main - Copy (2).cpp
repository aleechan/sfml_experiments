#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML\System\Thread.hpp>
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <math.h>
#include <zmouse.h>
#include <sstream>
#include <time.h>
const int width = 800;
const int height = 600;
#include "shaders.h"

/*
To Do
->Particles
->
*/

const double degtorad = 0.0174532925;
const double radtodeg = 57.2957795;
const double PI = 3.141592654;

const int g_wid = 40;
const int g_high = 30;
const int tile_w = (int)(width/g_wid);
const int tile_h = (int)(height/g_high);

int mousex = sf::Mouse::getPosition().x;
int mousey = sf::Mouse::getPosition().y;


int irandom(int v)
{
	return (int)( (v*rand()/RAND_MAX) +0.5);
}

double random()
{
	return ((double)rand()/RAND_MAX);
}

double point_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (sqrt(a));
}

double sqr_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (a);
}

double point_direction(double x1, double y1, double x2, double y2)
{
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

int signof(float x)
{
	if (x == 0) {return 0;}
	else if (x < 0) {return -1;}
	else if (x > 0) {return 1;}
}

int sgn(double x)
{
	return ( (x > 0) - (x < 0) );
}


float vel_mag(sf::Vector2f temp)
{
	return sqrt(temp.x*temp.x + temp.y*temp.y);
}

void normalize(sf::Vector2f &temp)
{
	float a = temp.x*temp.x + temp.y*temp.y;
	if(floor(a + 0.5) != 1)
	{
		a = sqrt(a);
		temp /= a;
	}
}

std::string itos(int num)
{
	std::stringstream ss;
	ss << num;
	return ss.str();
}

class node
{
public:
	int distance;
	sf::Vector2f vel;
	bool set;
	void init(int dist0)
	{
		set = true;
		distance = dist0;
	}
	void set_vel(sf::Vector2f vel0)
	{
		vel = vel0;
		if ((int)sqr_distance(0,0,vel.x,vel.y) != 1)
		{
			vel = vel* (float)(1/point_distance(0,0,vel.x,vel.y));
		}
	}
};

class map
{
public:
	sf::Vector2i end;
	node tiles[g_wid][g_high];
	int walls[g_wid][g_high];
	float density[g_wid][g_high];
	std::vector<sf::Text> dist_txt;
	sf::VertexArray blocks;
	sf::VertexArray grid;
	sf::VertexArray dirs;
	sf::RenderTexture tex;
	sf::Sprite UI;
	void init(sf::Vector2i end0)
	{
		end = end0;
		for(int i = 0 ; i <g_wid; i++)
		{
			for(int j = 0; j<g_high; j++)
			{
				tiles[i][j].set = false;
				walls[i][j] = 0;
			}
		}
		dirs.setPrimitiveType(sf::Lines);
		grid.setPrimitiveType(sf::Lines);
		grid.resize(g_wid*g_high*2);
		for(int i  = 0; i<g_wid; i++)
		{
			grid[i*2  ].position = sf::Vector2f(tile_w*i,0);
			grid[i*2+1].position = sf::Vector2f(tile_w*i,height);

			grid[i*2  ].color = sf::Color::White;
			grid[i*2+1].color = sf::Color::White;
		}

		for(int i  = 0; i<g_high; i++)
		{
			grid[g_wid*2 + i*2  ].position = sf::Vector2f(0,tile_w*i);
			grid[g_wid*2 + i*2+1].position = sf::Vector2f(width,tile_w*i);

			grid[g_wid*2 + i*2  ].color = sf::Color::White;
			grid[g_wid*2 + i*2+1].color = sf::Color::White;
		}
		tex.create(width,height);
		UI.setTexture(tex.getTexture());
	}
	void reset()
	{
		for(int i = 0 ; i <g_wid; i++)
		{
			for(int j = 0; j<g_high; j++)
			{
				tiles[i][j].set = false;
				walls[i][j] = 0;
				if(i == 0 || i == g_wid-1)
				{
					walls[i][j] = 1;
				}
				if(j == 0 || j == g_high-1)
				{
					walls[i][j] = 1;
				}
			}
		}
	}
	void reset_density()
	{
		for(int i = 0; i < g_wid; i++)
		{
			for(int j = 0; j < g_high; j++)
			{
				//density[i][j] = 0.f;
				density[i][j] *= 0.9;
			}
		}
	}
	void set_wall(int x, int y, int num)
	{
		walls[x][y] = num;
		refresh_walls();
	}
	bool set_node(int x, int y, int num)
	{
		if (x>=0 && x <g_wid && y>=0 && y<g_high)
		{
			if(tiles[x][y].set == false && walls[x][y] == 0)
			{
				tiles[x][y].init(num);
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	bool add_density(int x, int y, float num)
	{
		if (x>=0 && x <g_wid && y>=0 && y<g_high)
		{
			if(walls[x][y] == 0)
			{
				density[x][y] += num;
				return true;
			}
		}
		else
		{
			return false;
		}
	}
	int get_node(int x, int y, int def)
	{
		if (x>=0 && x <g_wid && y>=0 && y<g_high)
		{
			if(tiles[x][y].set == true && walls[x][y] == 0)
			{
				return tiles[x][y].distance;
			}
		}
		return def;
	}
	float get_density(int x, int y, float def)
	{
		if (x>=0 && x <g_wid && y>=0 && y<g_high)
		{
			if(walls[x][y] == 0)
			{
				return density[x][y];
			}
		}
		return def;
	}
	sf::Vector2i get_tile(int x, int y)
	{
		int tx=-1, ty=-1;
		for(int j = 0; j< g_wid; j++)
		{
			if (x >= j*tile_w && x < (j+1)*tile_w)
			{
				tx = j;
				break;
			}
		}
		for(int j = 0; j< g_high; j++)
		{
			if (y >= j*tile_h && y < (j+1)*tile_h)
			{
				ty = j;
				break;
			}
		}
		return sf::Vector2i(tx,ty);
	}
	sf::Vector2f get_vel(int x, int y)
	{

		if(walls[x][y] == 1)
		{
			return sf::Vector2f(0,0);
		}
		else
		{
			return tiles[x][y].vel;
		}
	}
	void refresh_walls()
	{
		blocks.clear();
		blocks.setPrimitiveType(sf::Quads);
		blocks.resize(g_wid*g_high*4);
		int a = 0;
		for(int i = 0 ; i <=g_wid; i++)
		{
			for(int j = 0; j<=g_high; j++)
			{
				if(walls[i][j] == 1)
				{
					sf::Vector2f pos2 = sf::Vector2f(i*tile_w,j*tile_h);

					blocks[a].position = pos2;
					blocks[a].color = sf::Color(128,128,128);
					a++;

					blocks[a].position = pos2+sf::Vector2f(tile_w,0);
					blocks[a].color = sf::Color(128,128,128);
					a++;

					blocks[a].position = pos2+sf::Vector2f(tile_w,tile_h);
					blocks[a].color = sf::Color(128,128,128);
					a++;
					
					blocks[a].position = pos2+sf::Vector2f(0,tile_h);
					blocks[a].color = sf::Color(128,128,128);
					a++;
				}
				if(sf::Vector2i(i,j) == end || tiles[i][j].distance == 0)
				{
					sf::Vector2f pos2 = sf::Vector2f(i*tile_w,j*tile_h);

					blocks[a].position = pos2;
					blocks[a].color = sf::Color::Red;
					a++;

					blocks[a].position = pos2+sf::Vector2f(tile_w,0);
					blocks[a].color = sf::Color::Red;
					a++;

					blocks[a].position = pos2+sf::Vector2f(tile_w,tile_h);
					blocks[a].color = sf::Color::Red;
					a++;
					
					blocks[a].position = pos2+sf::Vector2f(0,tile_h);
					blocks[a].color = sf::Color::Red;
					a++;
				}
			}
		}
		blocks.resize(a);
	}
	void refersh_flow_field()
	{
		float tx, ty;
		for(int i = 0 ; i <g_wid; i++)
		{
			for(int j = 0; j<g_high; j++)
			{
				if(tiles[i][j].set == true && walls[i][j] == 0)
				{
					tx = get_node(i-1,j,tiles[i][j].distance+1) - get_node(i+1,j,tiles[i][j].distance+1);
					ty = get_node(i,j-1,tiles[i][j].distance+1) - get_node(i,j+1,tiles[i][j].distance+1);
					if(tx == 0 && ty == 0)
					{
						if (walls[i+1][j] == 1)
						{
							ty = -0.1f;
						}
						else if (walls[i-1][j] == 1)
						{
							ty = 0.1f;
						}
						else if (walls[i][j+1] == 1)
						{
							tx = 0.1f;
						}
						else if (walls[i][j-1] == 1)
						{
							tx = 0.1f;
						}
					}
					tx += get_density(i-1,j,0) - get_density(i+1,j,0);
					ty += get_density(i,j-1,0) - get_density(i,j+1,0);
					tiles[i][j].set_vel(sf::Vector2f(tx,ty));
				}
			}
		}
		dirs.clear();
		dirs.resize(g_wid*g_high*2);
		int a = 0;
		for(int i = 0 ; i <g_wid; i++)
		{
			for(int j = 0; j<g_high; j++)
			{
				if(walls[i][j] == 0 && tiles[i][j].set == true)
				{
					dirs[a*2].color = sf::Color::Red;
					dirs[a*2].position = sf::Vector2f((i+0.5)*tile_w,(j+0.5)*tile_h);
					dirs[a*2+1].color = sf::Color::White;
					dirs[a*2+1].position =dirs[a*2].position + 15.f*tiles[i][j].vel;
					a++;
				}
			}
		}
	}
	void generate()
	{
		for(int i = 0 ; i <g_wid; i++)
		{
			for(int j = 0; j<g_high; j++)
			{
				tiles[i][j].set = false;
				tiles[i][j].set_vel(sf::Vector2f(0,0));
			}
		}
		tiles[end.x][end.y].init(0);
		tiles[end.x+1][end.y].init(0);
		tiles[end.x][end.y+1].init(0);
		tiles[end.x+1][end.y+1].init(0);

		bool empty = false;
		int round  = 0;
		while(!empty)
		{
			empty = true;
			for(int i = 0 ; i <g_wid; i++)
			{
				for(int j = 0; j<g_high; j++)
				{
					if(tiles[i][j].distance == round)
					{
						if (tiles[i][j].set == true)
						{
							if(set_node(i-1,j,round+1))
							{
								empty = false;
							}
							if(set_node(i,j-1,round+1))
							{
								empty = false;
							}
							if(set_node(i+1,j,round+1))
							{
								empty = false;
							}
							if(set_node(i,j+1,round+1))
							{
								empty = false;
							}
						}
					}
				}
			}
			std::cout << round << std::endl;
			round++;
		}
		refersh_flow_field();
		refresh_walls();
		//sf::Text temp_txt;
		//dist_txt.clear();
		//dirs.clear();
		//dirs.resize(g_wid*g_high*2);
		//int a = 0;
		//for(int i = 0 ; i <g_wid; i++)
		//{
		//	for(int j = 0; j<g_high; j++)
		//	{
		//		if(walls[i][j] == 0 && tiles[i][j].set == true)
		//		{
		//			dirs[a*2].color = sf::Color::Red;
		//			dirs[a*2].position = sf::Vector2f((i+0.5)*tile_w,(j+0.5)*tile_h);
		//			dirs[a*2+1].color = sf::Color::White;
		//			dirs[a*2+1].position =dirs[a*2].position + 15.f*tiles[i][j].vel;
		//			a++;
		//			//temp_txt.setPosition(i*tile_w,j*tile_h);
		//			//temp_txt.setCharacterSize(14);
		//			//temp_txt.setString(itos(tiles[i][j].distance));
		//			//dist_txt.push_back(temp_txt);
		//		}
		//	}
		//}
	}
	void draw(sf::RenderWindow &app)
	{
		app.draw(blocks);
		app.draw(dirs);
		//for(int i = 0; i<dist_txt.size(); i++)
		//{
		//	app.draw(dist_txt[i]);
		//}
	}
};

class particle
{
public:
	sf::Vector2f pos,vel;
	sf::Color colour;
	bool active;
	int life;
	void init(sf::Vector2f pos0, sf::Color col, int life0)
	{
		pos = pos0;
		vel = sf::Vector2f(0,0);
		colour = col;
		life = life0;
		active = true;
	}
	void init2(sf::Vector2f pos0, sf::Vector2f vel0, sf::Color col, int life0)
	{
		pos = pos0;
		vel = vel0;
		colour = col;
		life = life0;
		active = true;
	}
	void run()
	{
		//if (vel_mag(vel) > 7)
		//{
		//	normalize(vel);
		//	vel *= 7.f;
		//}
		pos += vel;
		if(life < 0)
		{
			active = false;
		}
		life--;
	}
};

int inactive(std::vector<particle> & parts)
{
	for(int i = 0; i <parts.size(); i++)
	{
		if(!parts[i].active)
		{
			return i;
		}
	}
	return -1;
}

int main()
{
    sf::RenderWindow App(sf::VideoMode (width, height, 32), "turret");
	App.setFramerateLimit(60);
	//App.setVerticalSyncEnabled(true);

	gaus_blur blur;
	blur.init();

	sf::VertexArray grid(sf::Lines,g_wid*g_high*2);
	for(int i  = 0; i<g_wid; i++)
	{
		grid[i*2  ].position = sf::Vector2f(tile_w*i,0);
		grid[i*2+1].position = sf::Vector2f(tile_w*i,height);

		grid[i*2  ].color = sf::Color::White;
		grid[i*2+1].color = sf::Color::White;
	}

	for(int i  = 0; i<g_high; i++)
	{
		grid[g_wid*2 + i*2  ].position = sf::Vector2f(0,tile_w*i);
		grid[g_wid*2 + i*2+1].position = sf::Vector2f(width,tile_w*i);

		grid[g_wid*2 + i*2  ].color = sf::Color::White;
		grid[g_wid*2 + i*2+1].color = sf::Color::White;
	}

	sf::Vector2i end, m;
	end = sf::Vector2i(10,10);

	map maze;
	maze.init(end);
	maze.reset_density();
	maze.reset();
	maze.generate();

	bool lclick = false;
	int mstate  = 0;
	mousex = sf::Mouse::getPosition(App).x;
	mousey = sf::Mouse::getPosition(App).y;
	
	std::vector<particle> dust;
	particle tempP;

	srand ( time(NULL) );

	sf::Clock clock;
	float lastTime = 0;
	while (App.isOpen())
    {
		sf::Event event;
		while (App.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			App.close();
		}
		App.clear();

		mousex = sf::Mouse::getPosition(App).x;
		mousey = sf::Mouse::getPosition(App).y;
		sf::Vector2i pos;
		for(int i = 0; i<g_wid; i++)
		{
			if (mousex>=i*tile_w && mousex<i*tile_w+tile_w)
			{
				pos.x = i;
			}
		}
		for(int i = 0; i<g_high; i++)
		{
			if(mousey>=i*tile_h && mousey<i*tile_h+tile_h)
			{
				pos.y = i;
			}
		}
		if (lclick == false && sf::Mouse::isButtonPressed(sf::Mouse::Left) == true)
		{
			std::cout << "(" << pos.x << "," << pos.y <<")";
			if     (maze.walls[pos.x][pos.y] == 1) {mstate = 0;}
			else if(maze.walls[pos.x][pos.y] == 0) {mstate = 1;}
			std::cout << mstate << std::endl;
		}
		lclick = sf::Mouse::isButtonPressed(sf::Mouse::Left);
		if(lclick)
		{
			maze.set_wall(pos.x,pos.y,mstate);
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		{
			int a;
			for(int i = 0; i < 20; i++)
			{
				a = inactive(dust);
				if(a != -1)
				{
					//dust[a].init(sf::Vector2f(mousex-15+irandom(30),mousey-15+irandom(30)),sf::Color(irandom(255),irandom(255),irandom(255)),400);
					//dust[a].init2(sf::Vector2f(mousex-15+irandom(30),mousey-15+irandom(30)),sf::Vector2f(15-irandom(30),15-irandom(30)),sf::Color(irandom(255),irandom(255),irandom(255)),400);
					dust[a].init2(sf::Vector2f(mousex-15+irandom(30),mousey-15+irandom(30)),sf::Vector2f(15-irandom(30),15-irandom(30)),sf::Color::Blue,400);
				}
				else
				{
					//tempP.init(sf::Vector2f(mousex-15+irandom(30),mousey-15+irandom(30)),sf::Color(irandom(255),irandom(255),irandom(255)),400);
					tempP.init2(sf::Vector2f(mousex-15+irandom(30),mousey-15+irandom(30)),sf::Vector2f(15-irandom(30),15-irandom(30)),sf::Color::Green,400);
					dust.push_back(tempP);
				}
			}
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::X))
		{
			int a;
			for(int i = 0; i <g_wid; i++)
			{
				for(int j = 0; j < g_high; j++)
				{
					if(maze.walls[i][j] != 1)
					{
						for(int k = 0; k < 1; k++)
						{
							a = inactive(dust);
							if(a != -1)
							{
								//dust[a].init2(sf::Vector2f((i+0.5)*tile_w,(j+0.5)*tile_h),sf::Vector2f(10-irandom(20),10-irandom(20)),sf::Color(irandom(255),irandom(255),irandom(255)),400);
								dust[a].init2(sf::Vector2f((i+0.5)*tile_w,(j+0.5)*tile_h),sf::Vector2f(10-irandom(20),10-irandom(20)),sf::Color::Blue,400);
							}
							else
							{
								//tempP.init2(sf::Vector2f((i+0.5)*tile_w,(j+0.5)*tile_h),sf::Vector2f(10-irandom(20),10-irandom(20)),sf::Color(irandom(255),irandom(255),irandom(255)),400);
								tempP.init2(sf::Vector2f((i+0.5)*tile_w,(j+0.5)*tile_h),sf::Vector2f(10-irandom(20),10-irandom(20)),sf::Color::Green,400);
								dust.push_back(tempP);
							}
						}
					}
				}
			}
		}
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right))
		{
			end = pos;
			maze.end = end;
			maze.refresh_walls();
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			maze.generate();
			dust.clear();
		}
		sf::VertexArray particles(sf::Quads,dust.size()*4);
		int activeParts = 0;

		maze.reset_density();
		for(int i = 0; i<dust.size(); i++)
		{
			if(dust[i].active)
			{
				sf::Vector2i pos = maze.get_tile(dust[i].pos.x,dust[i].pos.y);
				maze.add_density(pos.x,pos.y,0.05f);
				//maze.add_density(pos.x,pos.y,0.5f);
			}
		}
		maze.refersh_flow_field();
		for(int i = 0; i<dust.size(); i++)
		{
			if(dust[i].active)
			{
				if(dust[i].pos.x < 0 || dust[i].pos.x > width || dust[i].pos.y < 0 || dust[i].pos.y > height)
				{
					dust[i].active = false;
				}
				activeParts++;
				sf::Vector2f steering;
				sf::Vector2f tileVel;
				sf::Vector2i pos;
				steering = dust[i].vel;
				normalize(steering);
				//if ((int)sqr_distance(0,0,steering.x,steering.y) != 1 && sqr_distance(0,0,steering.x,steering.y) != 0)
				//{
				//	steering *= (float)(1/point_distance(0,0,steering.x,steering.y));
				//}
				pos = maze.get_tile(dust[i].pos.x + dust[i].vel.x,dust[i].pos.y + dust[i].vel.y);
				tileVel = maze.get_vel(pos.x,pos.y);
				
				normalize( tileVel);
				steering =  tileVel - steering;
				dust[i].vel += 3.f*steering;
				dust[i].run();
				pos = maze.get_tile(dust[i].pos.x,dust[i].pos.y);
				if(pos != sf::Vector2i(-1,-1))
				{
					if(maze.walls[pos.x][pos.y] == 1)
					{
						float dx,dy;
						dx = dust[i].pos.x - (pos.x+0.5)*tile_w;
						dy = dust[i].pos.y - (pos.y+0.5)*tile_h;
						if(dx == 0)
						{
							dx += 0.1;
						}
						if(dy == 0)
						{
							dy += 0.1;
						}
						if(abs(dx)<=abs(dy))
						{
							dust[i].pos.y = (pos.y+0.5)*tile_h + sgn(dy)*(tile_h/2+2);
							//dust[i].vel += 2.f*sgn(dx)*sf::Vector2f(1,0);
						}
						else
						{
							dust[i].pos.x = (pos.x+0.5)*tile_w + sgn(dx)*(tile_w/2+2);
							//dust[i].vel += 2.f*sgn(dy)*sf::Vector2f(0,1);
						}
						float a = 1.0 + random()*3;
						//dust[i].vel += a*sf::Vector2f(0.71*sgn(dx),0.71*sgn(dy));
						if(dx < dy)
						{
							dust[i].vel += a*sf::Vector2f(0.5*sgn(dx),0.866*sgn(dy));
						}
						else
						{
							dust[i].vel += a*sf::Vector2f(0.866*sgn(dx),0.5*sgn(dy));
						}
					}
				}
			
				particles[i*4  ].position = dust[i].pos+sf::Vector2f( 0,-2);
				particles[i*4+1].position = dust[i].pos+sf::Vector2f( 2, 0);
				particles[i*4+2].position = dust[i].pos+sf::Vector2f( 0, 2);
				particles[i*4+3].position = dust[i].pos+sf::Vector2f(-2 ,0);

				//particles[i*4  ].position = dust[i].pos+sf::Vector2f( 0,-5);
				//particles[i*4+1].position = dust[i].pos+sf::Vector2f( 5, 0);
				//particles[i*4+2].position = dust[i].pos+sf::Vector2f( 0, 5);
				//particles[i*4+3].position = dust[i].pos+sf::Vector2f(-5 ,0);

				//particles[i*4  ].position = dust[i].pos+sf::Vector2f( 0,-7);
				//particles[i*4+1].position = dust[i].pos+sf::Vector2f( 7, 0);
				//particles[i*4+2].position = dust[i].pos+sf::Vector2f( 0, 7);
				//particles[i*4+3].position = dust[i].pos+sf::Vector2f(-7, 0);

				particles[i*4  ].color = dust[i].colour;
				particles[i*4+1].color = dust[i].colour;
				particles[i*4+2].color = dust[i].colour;
				particles[i*4+3].color = dust[i].colour;
			}
		}
		maze.draw(App);
		App.draw(grid);
		App.draw(particles);

		//blur.clear();
		//blur.original.draw(particles);
		//App.draw(blur.render());
		//App.draw(particles);

		float currentTime = clock.restart().asSeconds();
		float fps = 1.f / currentTime;
		lastTime = currentTime;
		std::stringstream out;
		out << "FPS: " << (int)fps << " Particle Total: " << dust.size() << " Active Particles: " << activeParts;
		App.setTitle(out.str());
		App.display();
		

   }
   return EXIT_SUCCESS;
}
