#include <iostream>
#include <windows.h>
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>

#ifndef _SHADERS_SYS
#define _SHADERS_SYS

class gaus_blur
{
public:
	sf::Shader blurV,blurH;
	sf::RenderTexture original, buffer, final;
	void init()
	{
		blurV.loadFromFile("blurV.txt",sf::Shader::Fragment);
		blurV.setParameter("blurSize",0.00125f );

		blurH.loadFromFile("blurH.txt",sf::Shader::Fragment);
		blurH.setParameter("blurSize",0.00125f  );

		original.create(width,height);
		buffer.create(width,height);
		final.create(width,height);
	}

	void clear()
	{
		original.clear(sf::Color::Transparent);
		buffer.clear(sf::Color::Transparent);
		final.clear(sf::Color::Transparent);
	}

	sf::Sprite render()
	{
		original.display();
		blurV.setParameter("screenTexture",original.getTexture());
	
		buffer.draw(sf::Sprite(original.getTexture()),&blurV);
		buffer.display();
		blurH.setParameter("screenColorBuffer",buffer.getTexture());

		final.draw(sf::Sprite(buffer.getTexture()),&blurH);
		final.display();

		return (sf::Sprite(final.getTexture()));
	}

};

#endif