# README #

### What is this repository for? ###

* This is a repository for my various coding experiments i have done over the years.

### How do I get set up? ###

* Projects were created in Visual Studio 2015 Community
* Projects require the environment to be setup with SFML 2.3.2 (DLL)

### 2D Lighting ###

* This is a basic hard-shadow lighting system
* Left click to place blocks
* Right click to place lights

### Fire ###

* A basic 2D fire program

### Fluid ###

* A 2D fluid simulation based on springs

### Fluid 3D ###

* A fluid simulation based on springs

### Langton Ants ###

* An implementation of langton's ants

### Lightning ###

* A fractal lightning bolt generator

### Maze Game ###

* A 3D maze
* Move with WASD
* Turn with Q,E

### Particle Path Find ###

* An implementation of flow field path finding
* Right-Click to place the end point
* Left-Click to place/remove walls
* Space to place particles at the cursor location

### Smoke Test ###

* A proof of concept for my smoke shader
* Left-click to place smoke

### Space Game ###

* A spaceship with force based movement
* An use of my modular particle effect system

* W to accelerate forward
* S to stop accelerating
* A to move left
* D to move right
* Q to turn counter-clockwise
* E to turn clockwise

### Tower Defense ###

* An old TD project
* To place towers left click on a tower, then press space to show the turret, then click where you want  place the turret
