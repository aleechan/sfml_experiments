#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <event.h>
#include <math.h>
#include <zmouse.h>
#include <sstream>

const int width = 800;
const int height = 600;
const int scale = 1;
long int count =0;

int grid[(int)(width/scale)][(int)(height/scale)] = {0};

class ant
{
public:
	int x,y,dir;
	 /* direction
	 0 = right
	 1 = up
	 2 = left
	 3 = down
	 */
	void init(int x2, int y2, int dir2)
	{
		dir = dir2;
		x = x2;
		y = y2;
	}
	void run()
	{
		 if (dir == 0) {x+=1;}
		 if (dir == 1) {y-=1;}
		 if (dir == 2) {x-=1;}
		 if (dir == 3) {y+=1;}

		 if(x<0){x = (int)(width/scale);}
		 if(x>(int)(width/scale)){x = 0;}
		 if(y<0){x = (int)(height/scale);}
		 if(y>(int)(height/scale)){x = 0;}

		 if (dir == 0)
		 {
			 if (grid[x][y]==1)
			 {
				
				 dir = 1;
			 }
			 if (grid[x][y]==0)
			 {
				 dir = 3;
			 }
		 }
		 else if (dir == 1)
		 {
			 if (grid[x][y]==1)
			 {
				 
				 dir = 2;
			 }
			 if (grid[x][y]==0)
			 {
				 dir = 0;
			 }
		 }
		 else if (dir == 2)
		 {
			 if (grid[x][y]==1)
			 {
				 
				 dir = 3;
			 }
			  if (grid[x][y]==0)
			 {
				 dir = 1;
			 }
		 }
		 else if (dir == 3)
		 {
			 if (grid[x][y]==1)
			 {
				 
				 dir = 0;
			 }
			 if (grid[x][y]==0)
			 {
				 dir = 2;
			 }
		 }
		 if      (grid[x][y]==0) {grid[x][y]=1;}
		 else if (grid[x][y]==1) {grid[x][y]=0;}
	}
};

int main()
 {
     // Create the main window
     sf::RenderWindow window(sf::VideoMode(width, height), "SFML window");
	 sf::Texture image;
	 image.create(width,height);
	 sf::Sprite       sprite;
	 sf::Uint8        *pixels  = new sf::Uint8[width * height * 4];
	 sprite.setTexture(image); 
	 //window.setFramerateLimit(60);

	 sf::Clock clock;
     float lastTime = 0;

	 ant a[100];
	 for (int i=0; i<100; i++)
	 {
		 a[i].init(200+i%2,200+i,rand()%4);
	 }

     // Start the game loop
     while (window.isOpen())
     {
         // Process events
         sf::Event event;
         while (window.pollEvent(event))
         {
             // Close window : exit
             if (event.type == sf::Event::Closed)
                 window.close();
         }

		 //run
		 for (int i=0; i<100; i++)
		 {
		 a[i].run();
		 }

		 //std::cout << dir << std::endl; 
         window.clear();
		 if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		 {
		 for(int i=0; i<(int)(width/scale); i++)
			{
				for(int j=0; j<(int)(height/scale); j++)
				{
					if (scale==1)
					{ 
						 if (grid[i][j]==1)
						 {
							pixels[ (i+j*width)*4]         = 255; 
							pixels[ (i+j*width)*4 + 1]     = 255;
							pixels[ (i+j*width)*4 + 2]     = 255; 
							pixels[ (i+j*width)*4 + 3]     = 255;
						 }
						 if (grid[i][j]==0)
						 {
							pixels[ (i+j*width)*4]         = 0; 
							pixels[ (i+j*width)*4 + 1]     = 0;
							pixels[ (i+j*width)*4 + 2]     = 0; 
							pixels[ (i+j*width)*4 + 3]     = 255;
						 }
					}
					else if (scale>1)
					{
						for (int a=0;a<scale;a++)
						{
							for (int b=0;b<scale;b++)
							{
								if (grid[i][j]==1)
								 {
									pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4]         = 255; 
									pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4 + 1]     = 255;
									pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4 + 2]     = 255; 
									pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4 + 3]     = 255;
								 }
								 if (grid[i][j]==0)
								 {
									pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4]         = 0; 
									pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4 + 1]     = 0;
									pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4 + 2]     = 0; 
									pixels[ ( ( (i*scale)+a +( (j*scale)+b )*width ) ) * 4 + 3]     = 255;
								 }
							}
						}
					}
				}
				/*
				for (int i = 0; i<100; i++)
				{
					if (scale ==1)
					{
						pixels[ (a[i].x+a[i].y*width)*4]         = 255; 
						pixels[ (a[i].x+a[i].y*width)*4 + 1]     = 0;
						pixels[ (a[i].x+a[i].y*width)*4 + 2]     = 0; 
						pixels[ (a[i].x+a[i].y*width)*4 + 3]     = 255;
					}
					else if (scale >1)
					{
						for (int b=0;b<scale;b++)
						{
							for (int c=0;b<scale;b++)
							{
								pixels[ ( ( (a[i].x*scale)+b +( (a[i].y*scale)+c )*width ) ) * 4]         = 255; 
								pixels[ ( ( (a[i].x*scale)+b +( (a[i].y*scale)+c )*width ) ) * 4 + 1]     = 0;
								pixels[ ( ( (a[i].x*scale)+b +( (a[i].y*scale)+c )*width ) ) * 4 + 2]     = 0; 
								pixels[ ( ( (a[i].x*scale)+b +( (a[i].y*scale)+c )*width ) ) * 4 + 3]     = 255;
							}
						}
					}
				}*/
			}
		 image.update(pixels);
		 window.draw(sprite); 
         window.display();
		}
		count++;
		float currentTime = clock.restart().asSeconds();
        float fps = 1.f / currentTime;
        lastTime = currentTime;
		std::stringstream out;
		out << fps << " " << count;
		window.setTitle(out.str());
     }
 
     return EXIT_SUCCESS;
 }