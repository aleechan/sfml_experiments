#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <SFML\System\Thread.hpp>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

const double PI = 3.14159265;
const int scale = 25;
const int width = 800, height = 600;
const int widthG = width/scale, heightG = height/scale;
const sf::Color colours[4] = {sf::Color(0  ,0  ,0  ),
							  sf::Color(255,255,255),
							  sf::Color(0  ,255,255),
							  sf::Color(100,100,100),};
int grid [widthG][heightG] = {0};
int grid2[widthG][heightG] = {0};

int irandom(int v)
{
	if (v>10)
	{return  (int)(v*rand()/RAND_MAX);}
	else
	{return rand()%v;}
}
std::string inttostring(int num)//change int to a string
{
	std::stringstream ss;
	ss << num;
	return ss.str();
}


 int main()
 {
	 int tick = 0;
     sf::RenderWindow window(sf::VideoMode(width, height), "SFML window");
	 sf::VertexArray gridline(sf::Lines,280);
	 for(int i = 0; i < 80; i++)
	 {
		 gridline[i*2  ].position = sf::Vector2f(i*10,0);
		 gridline[i*2+1].position = sf::Vector2f(i*10,600);
		 gridline[i*2  ].color = sf::Color::White;
		 gridline[i*2+1].color = sf::Color::White;
	 }
	 for(int i = 80; i < 140; i++)
	 {
		 gridline[i*2  ].position = sf::Vector2f(0,i*10);
		 gridline[i*2+1].position = sf::Vector2f(800,i*10);
		 gridline[i*2  ].color = sf::Color::White;
		 gridline[i*2+1].color = sf::Color::White;
	 }

	 window.setFramerateLimit(60);
	 sf::Clock clock;
     float lastTime = 0;
	 int count=0;
     while (window.isOpen())
     {
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            window.close();
        }
		tick++;
        window.clear();
		sf::VertexArray blocks(sf::Quads,widthG*heightG*4);
		double mx = sf::Mouse::getPosition(window).x;
		double my = sf::Mouse::getPosition(window).y;	
		if(tick %3==0)
		{
		for(int i = 0; i< widthG; i++)
			{
				for(int j = 0; j<heightG; j++)
				{		
					if (mx>i*scale && mx<i*scale+scale && my>j*scale && my<j*scale+scale)
					{
						if(grid[i][j]==0 && sf::Mouse::isButtonPressed(sf::Mouse::Left))
						{grid2[i][j]=3;}
						if(sf::Mouse::isButtonPressed(sf::Mouse::Right))
						{grid2[i][j]=0;grid[i][j]=0;}
						if(grid[i][j]==3 && sf::Mouse::isButtonPressed(sf::Mouse::Middle))
						{grid2[i][j]=1;}
					}
					if(grid[i][j]==3)
					{
						count = 0;
						if(grid[i+1][j  ]==1){count++;}
						if(grid[i+1][j-1]==1){count++;}
						if(grid[i  ][j-1]==1){count++;}
						if(grid[i-1][j-1]==1){count++;}
						if(grid[i-1][j  ]==1){count++;}
						if(grid[i-1][j+1]==1){count++;}
						if(grid[i  ][j+1]==1){count++;}
						if(grid[i+1][j+1]==1){count++;}
						if( (count==1)||(count==2))
						{grid2[i][j]=1;}
					}				
					if(grid[i][j]==2){grid2[i][j]=3;}
					if(grid[i][j]==1){grid2[i][j]=2;}
				}
			}
			tick = 0;
		}
		for(int i = 0; i< widthG; i++)
		{
			for(int j = 0; j<heightG; j++)
			{
				grid[i][j] = grid2[i][j];
				blocks[(i+j*widthG)*4  ].color    = colours[grid[i][j]];
				blocks[(i+j*widthG)*4  ].position = sf::Vector2f(i*scale,j*scale);
				blocks[(i+j*widthG)*4+1].color    = colours[grid[i][j]];
				blocks[(i+j*widthG)*4+1].position = sf::Vector2f(i*scale+scale,j*scale);
				blocks[(i+j*widthG)*4+2].color    = colours[grid[i][j]];
				blocks[(i+j*widthG)*4+2].position = sf::Vector2f(i*scale+scale,j*scale+scale);
				blocks[(i+j*widthG)*4+3].color    = colours[grid[i][j]];
				blocks[(i+j*widthG)*4+3].position = sf::Vector2f(i*scale,j*scale+scale);

			}
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			std::ofstream data("data.txt");
			for(int j = 0; j<heightG; j++)
			{
				for(int i = 0; i< widthG; i++)
				{
					data << inttostring(grid[i][j])+std::string(",");
				}
				data << "."<< std::endl;
			}
			std::cout << "Level Saved\n";
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::C))
		{
			std::ofstream data("data.txt");
			for(int j = 0; j<heightG; j++)
			{
				for(int i = 0; i< widthG; i++)
				{
					grid[i][j]=0;
					grid2[i][j]=0;
				}
			}
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::L))
		{
			std::string data;
			std::ifstream file("data.txt");
			if(file.is_open())
			{
				while(!file.eof())
				{
					std::string data3;
					std::getline(file,data3);
					data += data3;
				}
				//char *data2  = new char[data.size()+1];
				//std::strcpy(data2,data.c_str());
				std::string temp;
				std::vector<int> level;
				for(int i =0; i<data.size()+1;i++)
				{
					if( !(data[i] == ',') && !(data[i] == '.')  )
					{
						temp+= data[i];
					}
					if(data[i] == ',')
					{
						level.push_back(std::atoi(temp.c_str()));
						temp.clear();
					}
				}
				for(int i = 0; i< widthG; i++)
				{
					for(int j = 0; j<heightG; j++)
					{
						grid2[i][j] = level[(i+j*widthG)];
					}
				}
				file.close();
			}
			std::cout << "Level Loaded\n";

		}
		window.draw(blocks);
		//window.draw(gridline);
		float currentTime = clock.restart().asSeconds();
        float fps = 1.f / currentTime;
        lastTime = currentTime;
		std::stringstream out;
		out << "FPS: " << (int)fps;
		window.setTitle(out.str());


         window.display();
     }
 
     return EXIT_SUCCESS;
 }