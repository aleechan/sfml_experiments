#pragma comment(lib, "opengl32.lib")
#pragma comment(lib, "glu32.lib")

#include <SFML/Graphics.hpp>
#include <SFML/OpenGL.hpp>
#include <gl\GLU.h>
#include <iostream>
#include <windows.h>
#include <cstdlib>
#include <math.h>
#include <sstream>
#include <fstream>

const double degtorad = 0.0174532925;
const double radtodeg = 57.2957795;
const double PI = 3.141592654;

const int width = 800;
const int height = 600;

std::vector<sf::Vector3f> blocks;
float size = 20.f;

int mousex = sf::Mouse::getPosition().x;
int mousey = sf::Mouse::getPosition().y;

int irandom(int v)
{
	return (int)( (v*rand()/RAND_MAX) +0.5);
}

double point_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (sqrt(a));
}

double sqr_distance(double x1, double y1, double x2, double y2)
{
	double a = (y2-y1)*(y2-y1)+(x2-x1)*(x2-x1);
	return (a);
}

double point_direction(double x1, double y1, double x2, double y2)
{
	double cx = x2-x1;
	double cy = y2-y1;
	double a = atan2(cy,cx);
	return a;
}

bool check_blocks(float x, float y, float z)
{
	bool col = false;
	for(int i = 0; i<blocks.size(); i++)
	{
		if (col == false)
		{
			//if(abs(blocks[i].x-x)<size && abs(blocks[i].y-y)<size && abs(blocks[i].z-z)<size)
			if(abs(blocks[i].x-x)<=size+2 && abs(blocks[i].z-z)<=size+2)
			{
				col = true;
			}
		}
	}
	return col;
}

class box
{
public:
	float x,y,z,size;
	float vely;
	void init(float x0, float y0, float z0, float size0)
	{
		vely = 0;
		x = x0;
		y = y0;
		z = z0;
		size = size0;
	}
	void run()
	{
		vely-=0.3f;
		if (y+vely-size<= -20)
		{
			y = -20.0f+size;
			vely*=-1.0f;
		}
		else
		{y+=vely;}
	}
};

void drawTexCube(float size)
{
	glBegin(GL_QUADS);

		glTexCoord2f(0, 0); glVertex3f(-size, -size, -size);
		glTexCoord2f(0, 1); glVertex3f(-size,  size, -size);
		glTexCoord2f(1, 1); glVertex3f( size,  size, -size);
		glTexCoord2f(1, 0); glVertex3f( size, -size, -size);

		glTexCoord2f(0, 0); glVertex3f(-size, -size, size);
		glTexCoord2f(0, 1); glVertex3f(-size,  size, size);
		glTexCoord2f(1, 1); glVertex3f( size,  size, size);
		glTexCoord2f(1, 0); glVertex3f( size, -size, size);

		glTexCoord2f(0, 0); glVertex3f(-size, -size, -size);
		glTexCoord2f(0, 1); glVertex3f(-size,  size, -size);
		glTexCoord2f(1, 1); glVertex3f(-size,  size,  size);
		glTexCoord2f(1, 0); glVertex3f(-size, -size,  size);

		glTexCoord2f(0, 0); glVertex3f(size, -size, -size);
		glTexCoord2f(0, 1); glVertex3f(size,  size, -size);
		glTexCoord2f(1, 1); glVertex3f(size,  size,  size);
		glTexCoord2f(1, 0); glVertex3f(size, -size,  size);

		glTexCoord2f(0, 1); glVertex3f(-size, -size,  size);
		glTexCoord2f(0, 0); glVertex3f(-size, -size, -size);
		glTexCoord2f(1, 0); glVertex3f( size, -size, -size);
		glTexCoord2f(1, 1); glVertex3f( size, -size,  size);

		glTexCoord2f(0, 1); glVertex3f(-size, size,  size);
		glTexCoord2f(0, 0); glVertex3f(-size, size, -size);
		glTexCoord2f(1, 0); glVertex3f( size, size, -size);
		glTexCoord2f(1, 1); glVertex3f( size, size,  size);

	glEnd();
}

class bullet
{
public:
	bool active;
	int count;
	float x,y,z,velx,vely,velz;
	void init(float x0, float y0, float z0, float velx0, float vely0, float velz0)
	{
		count = 0;
		active = true;
		x = x0;
		y = y0;
		z = z0;
		velx = velx0;
		vely = vely0;
		velz = velz0;
	}
	void run()
	{
		x+=velx;
		y+=vely;
		z+=velz;
		count++;
		if (count > 100)
		{active = false;}
	}
};

int inactive(bullet *list)
{
	for(int i = 0; i <100; i++)
	{
		if (list[i].active == false)
		{
			return i;
		}
	}
	return 0;
}

int main()
{
	std::cout << "Initializing" << std::endl;
    // create the window
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML OpenGL", sf::Style::Default, sf::ContextSettings(32));
    window.setVerticalSyncEnabled(true);
	//std::vector<sf::Vector3f> blocks;

	bullet shots[100];
	for(int i = 0 ; i<100; i++)
	{
		shots[i].active = false;
	}


	window.setFramerateLimit(60);
	sf::Clock clock;
	sf::Clock clock2;
	float cangle = 110.0f;
	float cwid = (float)(800/600);
	float cx = 0.0f;
	float cy = -10.0f;
	float cz = 0.0f;
	float crx = 0.0f;
    float lastTime = 0;
	std::cout << "loading Level...";
	std::string data;
	std::ifstream file("data.txt");
	if(file.is_open())
	{
		while(!file.eof())
		{
			std::string data3;
			std::getline(file,data3);
			data += data3;
		}
		//char *data2  = new char[data.size()+1];
		int z = 0;
		int x = 0;
		//std::strcpy(data2,data.c_str());
		std::string temp;
		for(int i =0; i<data.size()+1;i++)
		{
			if( !(data[i] == ',') && !(data[i] == '.')  )
			{
				temp+= data[i];
			}
			if(data[i] == ',')
			{
				if (temp == "3")blocks.push_back(sf::Vector3f(-120.0f+40.0f*x,10.0f,-100.0f+40.0f*z));
				x++;
				temp.clear();
			}
			if(data[i] == '.')
			{
				x=0;
				z++;
			}
		}
		file.close();
	}
	std::cout << "Finished" << std:: endl;
    // load resources, initialize the OpenGL states, ...
	
    // Enable Z-buffer read and write
    glEnable(GL_DEPTH_TEST);
    glDepthMask(GL_TRUE);
    glClearDepth(1.f);

	// Setup a perspective projection
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(cangle, cwid, 1.f, 500.f);
	
	std::cout << "Loading Textures...";

	GLuint texture = 0;
    {
        sf::Image image;
        if (!image.loadFromFile("metal_dark.png"))
            return EXIT_FAILURE;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_2D, texture);
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, image.getSize().x, image.getSize().y, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    }

	GLuint texture2 = 0;
    {
        sf::Image image;
        if (!image.loadFromFile("plants.png"))
            return EXIT_FAILURE;
        glGenTextures(1, &texture2);
        glBindTexture(GL_TEXTURE_2D, texture2);
        gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA, image.getSize().x, image.getSize().y, GL_RGBA, GL_UNSIGNED_BYTE, image.getPixelsPtr());
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    }
	glEnable(GL_TEXTURE_2D);
    glBindTexture(GL_TEXTURE_2D, texture);
    glColor4f(1.f, 1.f, 1.f, 1.f);

	sf::Texture bg;
	bg.loadFromFile("bg.png");

	std::cout << "Finished" << std::endl;

	box test;
	test.init(0.0,20.0f,0.0f,10.0f);

    // run the main loop
    bool running = true;
    while (running)
    {
        // handle events
        sf::Event event;
        while (window.pollEvent(event))
        {
            if (event.type == sf::Event::Closed)
            {
                // end the program
                running = false;
            }
            else if (event.type == sf::Event::Resized)
            {
                // adjust the viewport when the window is resized
                glViewport(0, 0, event.size.width, event.size.height);
            }
        }

        // Activate the window before using OpenGL commands.
        // This is useless here because we have only one window which is
        // always the active one, but don't forget it if you use multiple windows
        window.setActive();
        // clear the buffers
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		//window.pushGLStates();
        //window.draw(sf::Sprite(bg));
        //window.popGLStates();
		float dx = 0;
		float dz = 0;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
		{	
			dx = -2*sin(crx*degtorad);
			dz =  2*cos(crx*degtorad);
			if (!check_blocks(-(cx+dx),cy,-(cz+dz)))
			{
				cz+= dz;
				cx+= dx;
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
		{
			dx = -2*sin(crx*degtorad);
			dz =  2*cos(crx*degtorad);
			if (!check_blocks(-(cx-dx),cy,-(cz-dz)))
			{
				cz-= dz;
				cx-= dx;
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
		{
			dx = 2*cos((crx)*degtorad);
			dz = 2*cos((crx-90)*degtorad);
			if (!check_blocks(-(cx-dx),cy,-(cz-dz)))
			{
				cz-= dz;
				cx-= dx;
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
		{
			dx = 2*cos((crx)*degtorad);
			dz = 2*cos((crx-90)*degtorad);
			if (!check_blocks(-(cx+dx),cy,-(cz+dz)))
			{
				cz+= dz;
				cx+= dx;
			}
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
		{
			crx+=3.0f;
		}
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
		{
			crx-=3.0f;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			crx = 0.0f;
			cx  = 0.0f;
			cy  = -10.0f;;
			cz  = 0.0f;
		}

		//if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
		//{
		//	shots[inactive(shots)].init(-cx,-cy-1.0f,-cz,0.5*sin(crx*degtorad),0,-0.5*cos(crx*degtorad));
		//}

		//if (sf::Keyboard::isKeyPressed(sf::Keyboard::G))
		//{
		//	for(float i = 0 ; i <360; i+=3.6)
		//	{
		//		shots[inactive(shots)].init(-cx,-cy-2.0f,-cz,sin(i*degtorad),0,-cos(i*degtorad));
		//	}
		//}

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(cangle, cwid, 1.f, 1000.f);
		glRotatef(crx,0.0f,1.0f,0.0f);
		glTranslatef(cx, cy, cz);
		
		//float x =  sf::Mouse::getPosition(window).x * 200.f / window.getSize().x - 100.f;
        //float y = -sf::Mouse::getPosition(window).y * 200.f / window.getSize().y + 100.f;
		// Apply some transformations
        glMatrixMode(GL_MODELVIEW);
        //glTranslatef(x, y, -100.f);
		
		// draw...
		glLoadIdentity();
		glTranslatef(-cx, -20.0f, -cz);	
		
		glBegin(GL_QUADS);										// Draw A Quad
			
			glColor3f(1.0f,1.0f,1.0f);
			glVertex3f(-1000.0f, 0.0f, -1000.0f);					// Top Left
			glColor3f(1.0f,1.0f,1.0f);
			glVertex3f( 1000.0f, 0.0f, -1000.0f);					// Top Right
			glColor3f(1.0f,1.0f,1.0f);
			glVertex3f( 1000.0f, 0.0f,  1000.0f);					// Bottom Right
			glColor3f(1.0f,1.0f,1.0f);
			glVertex3f(-1000.0f, 0.0f,  1000.0f);					// Bottom Left
		
		glEnd();	
		//size = 20.0f;
		//for (int j = 0; j < 5; j++)
		//{
		//	for (int i = 0; i < 5; i++)
		//	{
		//		glLoadIdentity();
		//		glTranslatef(-50.0f+size*2*j, 0.0f, -0.0+size*2*i);
		//		//glRotatef(clock2.getElapsedTime().asSeconds() * 50.f, 1.f, 0.f, 0.f);
		//		//glRotatef(clock2.getElapsedTime().asSeconds() * 30.f, 0.f, 1.f, 0.f);
		//		//glRotatef(clock2.getElapsedTime().asSeconds() * 90.f, 0.f, 0.f, 1.f);
		//		glRotatef(180,1.0f,0.0f,0.0f);
		//		drawTexCube(size);
		//	}
		//}		
		for(int i = 0; i<100; i++)
		{
			if (shots[i].active == true)
			{
				glBindTexture(GL_TEXTURE_2D, texture2);
				shots[i].run();
				glLoadIdentity();
				glTranslatef(shots[i].x,shots[i].y,shots[i].z);
				drawTexCube(1.0f);
			}

		}
		for(int i = 0; i<blocks.size(); i++)
		{
			glBindTexture(GL_TEXTURE_2D, texture);
			glLoadIdentity();
			glTranslatef(blocks[i].x,blocks[i].y,blocks[i].z);
			//glRotatef(180,1.0f,0.0f,0.0f);
			drawTexCube(size);
		}

		test.run();
		glLoadIdentity();
		glTranslatef(test.x,test.y,test.z);
		drawTexCube(test.size);

        // end the current frame (internally swaps the front and back buffers)
		window.display();
		float currentTime = clock.restart().asSeconds();
		float fps = 1.f / currentTime;
		lastTime = currentTime;
		std::stringstream out;
		out << "FPS: " << (int)fps << " Camera Angle: " << cangle << " Camera Width: " << cwid << " Position: " << cx << "," << cy << "," << cz;
		window.setTitle(out.str());
    }

    // release resources...

    return 0;
}

