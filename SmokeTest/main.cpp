#include <SFML/Graphics.hpp>

int main()
{
	sf::RenderWindow window(sf::VideoMode(1280, 720), "SFML Template");
	sf::RenderTexture buffer;
	sf::Shader shader;
	sf::CircleShape dot(100);

	dot.setOrigin(100, 100);

	buffer.create(1280, 720);

	shader.loadFromFile("smoke.frag",sf::Shader::Fragment);
	shader.setParameter("res", sf::Vector2f(1280, 720));
	shader.setParameter("radius", 0.005f);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();
		dot.setPosition(sf::Vector2f(sf::Mouse::getPosition(window).x, sf::Mouse::getPosition(window).y));
		window.draw(dot);
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			dot.setFillColor(sf::Color(rand() % 255, rand() % 255, rand() % 255));
			buffer.draw(dot);
		}
		shader.setParameter("tex", buffer.getTexture());
		buffer.draw(sf::Sprite(buffer.getTexture()), &shader);
		buffer.display();
		window.draw(sf::Sprite(buffer.getTexture()));
		window.display();
	}

	return 0;
}