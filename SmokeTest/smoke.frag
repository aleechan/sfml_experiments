uniform sampler2D tex;
uniform vec2 res;
uniform float radius;
void main(void)
{
	vec2 pixel = gl_FragCoord.xy / res.xy;
	vec4 color = texture2D( tex, pixel);
	color += texture2D( tex, pixel + vec2(0, radius))*0.1;
	color += texture2D( tex, pixel + vec2(0,-radius))*2.3;
	color += texture2D( tex, pixel + vec2(radius, 0))*0.8;
	color += texture2D( tex, pixel + vec2(-radius,0))*0.8;
	color /= 5;
	color *= 0.97;
	gl_FragColor = color;
}